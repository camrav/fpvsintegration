﻿using System.Collections.Generic;

namespace FreePascalTasks
{
    internal enum MessageType
    { Error, Warning, NoError }

    internal delegate void LogDelegate(string subCategory, string errorCode, string helpKeyword, string file,
        int lineNumber, int columnNumber, int endLineNumber, int endColumnNumber, string message, params object[] messageArgs);

    internal class CompilerMessage
    {
        public MessageType Category;
        private string subcategory;

        public string Subcategory
        {
            get { return subcategory; }
            set
            {
                subcategory = value;
                if (errorTypes.ContainsKey(value))
                {
                    Category = errorTypes[value];
                }
                else
                {
                    Category = MessageType.NoError;
                }
            }
        }

        public string HelpKeyword = string.Empty;
        public string ErrorCode = string.Empty;
        public string File;
        public int Line;
        public int Column;
        public string Message;

        private static Dictionary<string, MessageType> errorTypes = new Dictionary<string, MessageType>();

        static CompilerMessage()
        {
            errorTypes.Add("Fatal", MessageType.Error);
            errorTypes.Add("Error", MessageType.Error);
            errorTypes.Add("Warning", MessageType.Warning);
            errorTypes.Add("Hint", MessageType.Warning);
            errorTypes.Add("Note", MessageType.Warning);
        }

        public void LogMessage(LogDelegate logFunc)
        {
            logFunc(Subcategory, ErrorCode, HelpKeyword, File, Line, Column, Line, Column, Message);
        }

        public override string ToString()
        {
            return string.Format("Category: {0}; SubCategory: {1}; HelpKeyword: {2}, ErrorCode: {3}, File: {4}, Line: {5}, Column: {6}, Message: {7}",
                Category, Subcategory, HelpKeyword, ErrorCode, File, Line, Column, Message);
        }
    }
}