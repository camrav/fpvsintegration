﻿using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;

namespace FreePascalTasks
{
    internal class MessageReporter
    {
        private TaskLoggingHelper Log;
        private ITaskItem[] SourceFiles;

        public MessageReporter(TaskLoggingHelper log, ITaskItem[] sourceFiles)
        {
            Log = log;
            SourceFiles = sourceFiles;
        }

        public void ProcessOutputLine(string line)
        {
            Log.LogMessage(MessageImportance.High, "Fpc: {0}", line);

            CompilerMessage message = ParseMessage(line);

            if (message.Category == MessageType.NoError)
                return;

            if (message.Category == MessageType.Error)
            {
                message.LogMessage(Log.LogError);
            }
            else if (message.Category == MessageType.Warning)
            {
                message.LogMessage(Log.LogWarning);
            }
        }

        private CompilerMessage ParseMessage(string line)
        {
            //Example of error message:
            //HelloWorldProject58.pas(3,4) Fatal: Syntax error, "BEGIN" expected but "identifier BEG" found

            int index = line.IndexOf(':');
            if (index < 0) return new CompilerMessage { Category = MessageType.NoError };

            string errorPart = line.Substring(0, index);
            string userFriendlyPart = line.Substring(index + 2); //line[index] == ':', line[index] == ' '

            CompilerMessage result = new CompilerMessage();

            index = errorPart.LastIndexOf(' ');

            if (index < 0) return new CompilerMessage { Category = MessageType.NoError }; //It is error, but it doesn't say anything about what happened.

            string infoPart = line.Substring(0, index);
            result.Subcategory = errorPart.Substring(index + 1);

            ParseInfo(infoPart, result);

            result.Message = userFriendlyPart; //TODO: parse syntaxError/Unknown char, etc.

            return result;
        }

        private void ParseInfo(string infoPart, CompilerMessage result)
        {
            //Example of info:
            //HelloWorldProject58.pas(3,4)
            //HelloWorldProject58.pas(3)

            int index = infoPart.LastIndexOf('(');
            if (index < 0)
            {
                result.Category = MessageType.NoError;
                return;
            }

            result.File = infoPart.Substring(0, index);
            result.File = SourceFiles[0].ToString();

            string numbers = infoPart.Substring(index + 1);
            string[] parts = numbers.Split(',', ')');

            result.Line = int.Parse(parts[0]);
            if (parts.Length >= 2)
                result.Column = int.Parse(parts[1]);
        }
    }
}