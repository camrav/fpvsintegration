﻿using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace FreePascalTasks
{
    public class FreePascalCompile : Task
    {
        [Required]
        public ITaskItem[] Source { get; set; }

        [Required]
        public string OutputPath { get; set; }

        /// <summary>
        /// Name of the result executable/library
        /// </summary>
        [Required]
        public string BinaryName { get; set; }

        [Required]
        public string OutputType { get; set; }

        public bool EnableGoto { get; set; } = false;

        public int OptimizationLevel { get; set; } = 0;

        public string AdditionalLibraryPaths { get; set; } = string.Empty;

        public ITaskItem[] Directories { get; set; } = new ITaskItem[0];

        public string CompileSymbols { get; set; } = string.Empty;

        public override bool Execute()
        {
            Log.LogMessage(MessageImportance.High, "Fpc: Compile task start");
            if (Source.Length == 0)
            {
                Log.LogWarning("No items to compile");
                return true;
            }

            string compiler = GetCompiler();
            if (compiler == null)
            {
                Log.LogError("Compiler not found. Please install Free Pascal.");
                return false;
            }

            string args = new CommandLineBuilder(this).GenerateCommandLineArguments(GetProgramTaskItem());
            Log.LogMessage(MessageImportance.High, "Used command: {0} {1}", compiler, args);

            using (Process process = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = compiler,
                    Arguments = args,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true,
                    UseShellExecute = false,
                }
            })
            {
                process.Start();
                process.WaitForExit();

                MessageReporter reporter = new MessageReporter(Log, Source);
                string line;
                while ((line = process.StandardOutput.ReadLine()) != null)
                {
                    reporter.ProcessOutputLine(line);
                }

                bool success = process.ExitCode == 0;
                Log.LogMessage(MessageImportance.High, "Fpc: Compilation end");
                return success;
            }
        }

        private string GetCompiler()
        {
            string compiler = Settings.Default.CompilerPath;
            if (!File.Exists(compiler))
            {
                Process process = new Process
                {
                    StartInfo = new ProcessStartInfo
                    {
                        FileName = "where",
                        Arguments = "fpc",
                        RedirectStandardOutput = true,
                        CreateNoWindow = true,
                        UseShellExecute = false
                    }
                };

                process.Start();
                process.WaitForExit();

                if (process.ExitCode != 0)
                    return null;

                string filePath = process.StandardOutput.ReadToEnd();

                Settings.Default.CompilerPath = filePath;
                Settings.Default.Save();
            }

            return compiler;
        }

        private ITaskItem GetProgramTaskItem()
        {
            ITaskItem result = Source.Where(item => item.GetMetadata("CodeType") == "Program").FirstOrDefault();
            if (result == null)
            {
                result = Source[0];
                Log.LogWarning("No source file marked with metadata <CodeType>Program</CodeType> in project file. Assuming {0} is program file.", result);
            }

            return result;
        }
    }
}