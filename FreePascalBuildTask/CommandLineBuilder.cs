﻿using Microsoft.Build.Framework;
using System;
using System.IO;
using CmdBuilder = Microsoft.Build.Utilities.CommandLineBuilder;

namespace FreePascalTasks
{
    internal class CommandLineBuilder
    {
        private FreePascalCompile task;

        public CommandLineBuilder(FreePascalCompile task)
        {
            this.task = task;
        }

        public string GenerateCommandLineArguments(ITaskItem sourceFile)
        {
            CmdBuilder builder = new CmdBuilder(true);

            AddGenerateDebugSymbols(builder);
            SetOptimizations(builder);
            AddLibraryPaths(builder);
            AddOutputPath(builder);
            AddEnableGoto(builder);
            AddUnitSearchPaths(builder);
            AddCompileSymbols(builder);
            AddSourceFile(builder, sourceFile);

            return builder.ToString();
        }

        private void AddEnableGoto(CmdBuilder builder)
        {
            if (!task.EnableGoto) return;

            builder.AppendSwitch("-Sg");
        }

        private void AddCompileSymbols(CmdBuilder builder)
        {
            string[] symbols = task.CompileSymbols.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

            foreach (var symbol in symbols)
            {
                builder.AppendSwitchIfNotNull("-d", symbol);
            }
        }

        private void AddUnitSearchPaths(CmdBuilder builder)
        {
            foreach (var directory in task.Directories)
            {
                builder.AppendSwitchIfNotNull("-Fu", directory);
            }
        }

        private void AddLibraryPaths(CmdBuilder builder)
        {
            if (string.IsNullOrEmpty(task.AdditionalLibraryPaths))
                return;

            builder.AppendSwitchIfNotNull("-Fl", task.AdditionalLibraryPaths);
        }

        private const int MinOptimLevel = 0;
        private const int MaxOptimLevel = 4;

        private void SetOptimizations(CmdBuilder builder)
        {
            if (task.OptimizationLevel < MinOptimLevel || task.OptimizationLevel > MaxOptimLevel)
            {
                task.Log.LogWarning("Optimization level ({0}) out of range. Valid range {1} - {2}", task.OptimizationLevel, MinOptimLevel, MaxOptimLevel);
                if (task.OptimizationLevel < 0)
                    task.OptimizationLevel = 0;
                else
                    task.OptimizationLevel = 4;
            }

            string strRepr = task.OptimizationLevel == 0 ? "-" : task.OptimizationLevel.ToString();
            builder.AppendSwitch("-O" + strRepr);
        }

        private void AddSourceFile(CmdBuilder builder, ITaskItem sourceFile)
        {
            builder.AppendFileNameIfNotNull(sourceFile);
        }

        private void AddOutputPath(CmdBuilder builder)
        {
            string path = EnsurePathEndsWithDirectoryDelimiter(task.OutputPath);
            EnsurePathExists(path);
            string outputName = path + Path.ChangeExtension(task.BinaryName, ".exe");
            builder.AppendSwitchIfNotNull("-o", outputName);
        }

        private void AddGenerateDebugSymbols(CmdBuilder builder)
        {
            builder.AppendSwitch("-gw");
        }

        private string EnsurePathEndsWithDirectoryDelimiter(string outputPath)
        {
            if (outputPath[outputPath.Length - 1] == Path.DirectorySeparatorChar)
                return outputPath;
            return outputPath + Path.DirectorySeparatorChar;
        }

        private void EnsurePathExists(string path)
        {
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
        }
    }
}