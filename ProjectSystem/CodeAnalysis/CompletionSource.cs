﻿using CodeAnalysis.Grammar;
using CodeAnalysis.Semantics;
using CodeAnalysis.SyntaxTree.OnDemandTree;
using FpLexer;
using Microsoft.VisualStudio.Language.Intellisense;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FpTextSpan = CodeAnalysis.TextSpan;

namespace ProjectSystem.CodeAnalysis
{
    internal class CompletionSource : ICompletionSource
    {
        private CompletionSourceProvider provider;
        private ITextBuffer textBuffer;
        private FpGrammar grammar = new FpGrammar();

        public CompletionSource(CompletionSourceProvider provider, ITextBuffer textBuffer)
        {
            this.provider = provider;
            this.textBuffer = textBuffer;
        }

        public void AugmentCompletionSession(ICompletionSession session, IList<CompletionSet> completionSets)
        {
            try
            {
                var span = FindTokenSpanAtPosition(session.GetTriggerPoint(textBuffer), session);
                FpSyntaxNode node = FindNodeAtPosition(span);
                if (node == null || node.Kind.IsComment() || node.Kind == SyntaxKind.StringConstant) return;

                List<Completion> completions = new List<Completion>();
                HashSet<SyntaxKind> uniqueKinds = new HashSet<SyntaxKind>(PossibleTerminals(node));
                foreach (SyntaxKind kind in uniqueKinds)
                {
                    AddCompletions(completions, node, kind);
                }

                completions.Sort((a, b) => StringComparer.OrdinalIgnoreCase.Compare(a.DisplayText, b.DisplayText));
                completionSets.Add(new CompletionSet(
                    "Tokens",
                    "Tokens",
                    span, completions, null));
            }
            catch (Exception)
            {
            }
        }

        private void AddCompletions(List<Completion> completions, FpSyntaxNode node, SyntaxKind kind)
        {
            if (kind.IsTrivia())
            {
                return;
            }

            if (kind.IsKeyword() || kind.IsPunctuation() || kind.IsModifier() || kind.IsOperator())
            {
                completions.Add(new Completion(grammar.GetStringAlias(kind)));
                return;
            }

            if (kind.IsIdentifier())
            {
                var tree = node.Tree as FpSyntaxTree;
                switch (kind)
                {
                    case SyntaxKind.VariableIdentifier:
                        AddVariableCompletions(completions, node);
                        return;

                    case SyntaxKind.ProcedureIdentifier:
                        AddProcedureCompletions(completions, node);
                        return;

                    case SyntaxKind.FunctionIdentifier:
                        AddFunctionCompletions(completions, node);
                        return;

                    case SyntaxKind.ConstantIdentifier:
                        AddConstantCompletion(completions, node);
                        return;

                    case SyntaxKind.TypeIdentifier:
                        AddTypeCompletions(completions, node);
                        return;

                    case SyntaxKind.FieldIdentifier:
                        AddFieldCompletions(completions, node);
                        return;

                    default:
                        return;
                }
            }
        }

        private void AddFieldCompletions(List<Completion> completions, FpSyntaxNode node)
        {
            RecordTypeSymbol recordSymbol = FindMemberType(node);

            if (recordSymbol == null)
                return;

            foreach (var member in recordSymbol.Members)
            {
                string description = $"{recordSymbol.Name}.{member.Name}: {member.Type.ToString()}";
                completions.Add(new Completion(member.Name, member.Name, description, null, null));
            }
        }

        private ITypeSymbol FindVariableType(FpSyntaxNode node)
        {
            if (node is FpSyntaxIdentifier)
            {
                VariableSymbol varSymbol = (node.Tree as FpSyntaxTree).SemanticModel.GetSymbol(node as FpSyntaxIdentifier) as VariableSymbol;
                return varSymbol?.Type;
            }
            else if (node is FpDereferencedPointerVariable)
            {
                var variable = (node as FpDereferencedPointerVariable).Variable;
                PointerTypeSymbol typeSymbol = FindVariableType(variable) as PointerTypeSymbol;
                return typeSymbol?.BaseType;
            }
            else if (node is FpRecordFieldVariable)
            {
                var variableNode = node as FpRecordFieldVariable;
                RecordTypeSymbol typeSymbol = FindVariableType(variableNode.Variable) as RecordTypeSymbol;
                var member = typeSymbol?.Children.Where(sym => string.Equals(sym.Name, variableNode.FieldName.Text, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
                return (member as VariableSymbol)?.Type;
            }
            else if (node is FpArrayVariable)
            {
                ArrayTypeSymbol typeSymbol = FindVariableType((node as FpArrayVariable).Variable) as ArrayTypeSymbol;
                return typeSymbol?.ElementType;
            }

            return null;
        }

        private RecordTypeSymbol FindMemberType(FpSyntaxNode node)
        {
            while (node != null && !(node is FpRecordFieldVariable))
                node = node.Parent as FpSyntaxNode;

            if (node == null)
                return null;

            var fieldVariable = node as FpRecordFieldVariable;

            return FindVariableType(fieldVariable.Variable) as RecordTypeSymbol;
        }

        private void AddFunctionCompletions(List<Completion> completions, FpSyntaxNode node)
        {
            foreach (FunctionSymbol function in GetSymbols(node, SymbolKind.Function))
            {
                string paramsDescription = GetParamsDescription(function);

                string description = $"Function {function.Name}{paramsDescription}: {function.ReturnType.ToString()}";
                //   string displayText = $"{function.Name}{paramsDescription}: {function.ReturnType.ToString()}";
                completions.Add(new Completion(function.Name, function.Name, description, null, null));
            }
        }

        private void AddProcedureCompletions(List<Completion> completions, FpSyntaxNode node)
        {
            foreach (ProcedureSymbol procedure in GetSymbols(node, SymbolKind.Procedure))
            {
                string paramsDescription = GetParamsDescription(procedure);
                string description = $"Procedure {procedure.Name}{paramsDescription}";
                //    string displayText = $"{procedure.Name}{paramsDescription}";
                completions.Add(new Completion(procedure.Name, procedure.Name, description, null, null));
            }
        }

        private string GetParamsDescription(SubprogramSymbol subprogram)
        {
            StringBuilder sb = new StringBuilder("(");

            bool first = true;
            foreach (ParameterSymbol param in subprogram.Children.Where(sym => sym.SymbolKind == SymbolKind.Parameter))
            {
                if (!first)
                    sb.Append("; ");
                string prefix = string.Empty;
                if (param.ParamType == ParameterType.Const)
                    prefix = "const "; //trailing space for formatting
                else if (param.ParamType == ParameterType.Var)
                    prefix = "var ";

                sb.AppendFormat("{0}{1}", prefix, GetVariableDescription(param));
                first = false;
            }

            sb.Append(")");
            return sb.ToString();
        }

        private void AddConstantCompletion(List<Completion> completions, FpSyntaxNode node)
        {
            foreach (ConstantSymbol constant in GetSymbols(node, SymbolKind.Constant))
            {
                string description = string.Format("Const {0} = {1}", constant.Name, constant.Value);
                completions.Add(new Completion(constant.Name, constant.Name, description, null, null));
            }

            foreach (EnumTypeSymbol enumSymbol in GetSymbols(node, SymbolKind.Type).Where(sym => (sym as TypeSymbol).Kind == TypeKind.Enum))
            {
                foreach (ConstantSymbol constant in enumSymbol.Children)
                {
                    string description = string.Format("{0} from enum {1}", constant.Name, enumSymbol.Name);
                    completions.Add(new Completion(constant.Name, constant.Name, description, null, null));
                }
            }
        }

        private void AddTypeCompletions(List<Completion> completions, FpSyntaxNode node)
        {
            foreach (TypeSymbol type in GetSymbols(node, SymbolKind.Type))
            {
                if (string.IsNullOrEmpty(type.Name)) continue; //no need to give intellisense to unnamed types
                string description = string.Format("Type {0}", type.ToString());
                completions.Add(new Completion(type.Name, type.Name, description, null, null));
            }
        }

        private void AddVariableCompletions(List<Completion> completions, FpSyntaxNode node)
        {
            foreach (VariableSymbol variable in GetSymbols(node, SymbolKind.Variable))
            {
                if (variable.Type == null)
                    continue; //unsupported types (i.e. sets)
                string description = "Var " + GetVariableDescription(variable);
                completions.Add(new Completion(variable.Name, variable.Name, description, null, null));
            }

            foreach (ParameterSymbol parameter in GetSymbols(node, SymbolKind.Parameter))
            {
                if (parameter.Type == null)
                    continue;

                string description = "Parameter " + GetVariableDescription(parameter);
            }
        }

        private string GetVariableDescription(VariableSymbol variable)
        {
            return string.Format("{0}: {1}", variable.Name, variable.Type?.ToString());
        }

        private IEnumerable<ISymbol> GetSymbols(FpSyntaxNode node, SymbolKind kind)
        {
            return (node.Tree as FpSyntaxTree).SemanticModel.GetAvailableSymbols(node).Where(sym => sym.SymbolKind == kind).Where(sym => !sym.Name.StartsWith("$"));
        }

        private IEnumerable<SyntaxKind> PossibleTerminals(FpSyntaxNode node)
        {
            IEnumerable<SyntaxKind> result = null;
            if (node is UnexpectedTrivia)
            {
                var unexpected = node as UnexpectedTrivia;
                result = unexpected.Expected;
            }

            FpSyntaxNode originalNode = node;

            int childNumber = 0;
            while (node.Parent != null)
            {
                if (childNumber != 0)
                {
                    break;
                }

                for (int i = 0; i < node.Parent.Children.Count; i++)
                {
                    if (node.Parent.Children[i] == node)
                    {
                        childNumber = i;
                        if (IsTriviaAfter(originalNode))
                            childNumber++;
                        break;
                    }
                }
                node = (FpSyntaxNode)node.Parent;
            }

            if (result == null)
                return PossibleTerminalsAt(node, childNumber);
            else
                return result.Concat(PossibleTerminalsAt(node, childNumber));
        }

        private bool IsTriviaAfter(FpSyntaxNode node)
        {
            if (node.Parent.HasTriviaAfter)
            {
                var triviaParent = node.Parent as FpSyntaxNode;
                return triviaParent.GetTriviaAfter().Contains(node);
            }
            return false;
        }

        private IEnumerable<SyntaxKind> PossibleTerminalsAt(FpSyntaxNode node, int childNumber)
        {
            HashSet<SyntaxKind> result = new HashSet<SyntaxKind>(grammar.GetPossibleKinds(node.Kind, childNumber, GetExistingTerminals(node, childNumber)));

            if (result.Contains(SyntaxKind.None) && node.Parent != null)
            {
                var parent = node.Parent as FpSyntaxNode;
                int childNr = GetChildNumber(parent, node);
                var extra = PossibleTerminalsAt(parent, childNr + 1);
                result.UnionWith(extra);
            }

            result.UnionWith(CheckRecursiveProductions(node, childNumber));

            while (childNumber > 0)
            {
                childNumber--;
                var extra = MissingOrPossibleTerminalsIn(node.Children[childNumber] as FpSyntaxNode);

                result.UnionWith(extra);

                if (node.Children[childNumber].Length != 0) break;
            }
            result.Remove(SyntaxKind.None);
            return result;
        }

        private int GetChildNumber(FpSyntaxNode parent, FpSyntaxNode child)
        {
            for (int i = 0; i < parent.Children.Count; i++)
            {
                if (parent.Children[i] == child)
                    return i;
            }
            return 0;
        }

        private SyntaxKind[] GetExistingTerminals(FpSyntaxNode node, int stopAt)
        {
            int count = Math.Max(0, stopAt);

            return node.Children.Take(count).Cast<FpSyntaxNode>().Select(child => child.Kind).ToArray();
        }

        private IEnumerable<SyntaxKind> CheckRecursiveProductions(FpSyntaxNode node, int childNumber)
        {
            if (childNumber == 0)
                return Enumerable.Empty<SyntaxKind>();

            var previousChild = node.Children[childNumber - 1] as FpSyntaxNode;

            if (!previousChild.Kind.IsPunctuation() || previousChild.Kind == SyntaxKind.DotToken)
                return Enumerable.Empty<SyntaxKind>();

            var follows = grammar.GetFollows(node.Kind).ToList();
            if (follows.Contains(previousChild.Kind))
            {
                return follows.Except(new[] { previousChild.Kind });
            }

            return Enumerable.Empty<SyntaxKind>();
        }

        private IEnumerable<SyntaxKind> MissingOrPossibleTerminalsIn(FpSyntaxNode node)
        {
            if (node.Length == 0)
                return grammar.GetPossibleKinds(node.Kind, 0, new SyntaxKind[0]);

            IEnumerable<SyntaxKind> result = new SyntaxKind[0];
            foreach (FpSyntaxNode child in node.Children.Reverse())
            {
                if (child.Length == 0)
                {
                    result = result.Concat(grammar.GetPossibleKinds(child.Kind, 0, new SyntaxKind[0]));
                }
                else
                {
                    return result.Concat(MissingOrPossibleTerminalsIn(child));
                }
            }

            return result;
        }

        private ITrackingSpan FindTokenSpanAtPosition(ITrackingPoint trackingPoint, ICompletionSession session)
        {
            SnapshotPoint currentPoint = (session.TextView.Caret.Position.BufferPosition) - 1;
            var previousChar = currentPoint.GetChar();
            if (!(char.IsLetterOrDigit(previousChar) || previousChar == '_'))
                currentPoint += 1;

            ITextStructureNavigator navigator = provider.NavigatorService.GetTextStructureNavigator(textBuffer);
            TextExtent extent = navigator.GetExtentOfWord(currentPoint);

            return extent.IsSignificant
                ? currentPoint.Snapshot.CreateTrackingSpan(extent.Span, SpanTrackingMode.EdgeInclusive)
                : currentPoint.Snapshot.CreateTrackingSpan(new Span(currentPoint.Position, 0), SpanTrackingMode.EdgeInclusive);
        }

        private FpSyntaxNode FindNodeAtPosition(ITrackingSpan span)
        {
            var tree = provider.Trees.GetRoot(textBuffer);
            var curSpan = span.GetSpan(textBuffer.CurrentSnapshot);
            int length = Math.Max(curSpan.Length, 1);
            var children = tree.Root.ChildrenIn(new FpTextSpan(curSpan.Start, length)).ToList();

            if (children.Count != 1)
            {
                return null;
                //return null; //TODO: do this instead -> There CAN be multiple tokens per word
                // bool areEqual = children[0] == children[1];
                // return children[0] as FpSyntaxNode;
                //throw new Exception("There should be only one token per word");
            }

            return children[0] as FpSyntaxNode;
        }

        private bool isDisposed = false;

        public void Dispose()
        {
            if (!isDisposed)
            {
                GC.SuppressFinalize(this);
                isDisposed = true;
            }
        }
    }
}