﻿using CodeAnalysis;
using CodeAnalysis.Semantics;
using CodeAnalysis.SyntaxTree;
using CodeAnalysis.SyntaxTree.OnDemandTree;
using Microsoft.VisualStudio.Text;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using WinProcess = System.Diagnostics.Process;

namespace ProjectSystem.CodeAnalysis
{
    [Export(typeof(TreeUpdater))]
    public class TreeUpdater
    {
        private Dictionary<ITextBuffer, FpSyntaxTree> buffers = new Dictionary<ITextBuffer, FpSyntaxTree>();

        public event Action<SnapshotSpan> TextChanged = delegate { };

        public UnitManager UnitManager { get; private set; }

        [ImportingConstructor]
        public TreeUpdater()
        {
            UnitManager = CreateUnitManager();
        }

        private UnitManager CreateUnitManager()
        {
            string ppuDumpPath = Properties.Settings.Default.PpuDumpPath;

            if (!File.Exists(ppuDumpPath) || !Directory.Exists(Properties.Settings.Default.StandardLibraryFolder))
            {
                WinProcess process = new WinProcess
                {
                    StartInfo = new System.Diagnostics.ProcessStartInfo
                    {
                        FileName = "where",
                        Arguments = "ppudump",
                        RedirectStandardOutput = true,
                        CreateNoWindow = true,
                        UseShellExecute = false
                    }
                };

                process.Start();
                process.WaitForExit();

                if (process.ExitCode != 0)
                    return new UnitManager(null, null);

                ppuDumpPath = process.StandardOutput.ReadToEnd();
                Properties.Settings.Default.PpuDumpPath = ppuDumpPath;
                if (!Directory.Exists(Properties.Settings.Default.StandardLibraryFolder))
                {
                    int lastIndex = ppuDumpPath.LastIndexOf("bin");
                    if (lastIndex >= 0)
                    {
                        string libraryFolder = ppuDumpPath.Remove(ppuDumpPath.LastIndexOf("ppudump.exe")).Remove(lastIndex, "bin".Length).Insert(lastIndex, "units");
                        Properties.Settings.Default.StandardLibraryFolder = libraryFolder;
                    }
                }
                Properties.Settings.Default.Save();
            }

            return new UnitManager(Properties.Settings.Default.PpuDumpPath, Properties.Settings.Default.StandardLibraryFolder);
        }

        private void InitBuffer(ITextBuffer textBuffer)
        {
            FpSyntaxTree tree = FpSyntaxTree.Parse(new StringSourceText(textBuffer.CurrentSnapshot.GetText()), UnitManager);

            buffers.Add(textBuffer, tree);
            textBuffer.Changed += TextBuffer_Changed;
        }

        private void TextBuffer_Changed(object sender, TextContentChangedEventArgs e)
        {
            var buffer = e.After.TextBuffer;
            buffers[buffer] = FpSyntaxTree.Parse(new StringSourceText(e.After.GetText()), UnitManager);

            int oldPosition = e.Changes[0].OldPosition;
            TextChanged(new SnapshotSpan(e.After, oldPosition, e.After.Length - oldPosition));
        }

        internal ISyntaxTree GetRoot(ITextBuffer textBuffer)
        {
            if (!buffers.ContainsKey(textBuffer))
            {
                InitBuffer(textBuffer);
            }

            return buffers[textBuffer];
        }
    }
}