﻿using CodeAnalysis;

//using Microsoft.VisualStudio.TextManager.Interop;
using CodeAnalysis.SyntaxTree;
using CodeAnalysis.SyntaxTree.OnDemandTree;
using FpLexer;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Adornments;
using Microsoft.VisualStudio.Text.Classification;
using Microsoft.VisualStudio.Text.Tagging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectSystem.CodeAnalysis
{
    internal class FpHighlightningTagger : ITagger<ClassificationTag>, ITagger<ErrorTag>
    {
        public event EventHandler<SnapshotSpanEventArgs> TagsChanged;

        private TreeUpdater treeUpdater;
        private ClassificationTypeProvider classificationProvider;

        private List<ITrackingSpan> multiLineComments = new List<ITrackingSpan>();

        private Span? lastSpan = null;
        private ITextVersion lastVersion = null;
        private List<SyntaxNode> lastChildren;

        public FpHighlightningTagger(IClassificationTypeRegistryService classificationService, TreeUpdater treeUpdater)
        {
            this.treeUpdater = treeUpdater;
            classificationProvider = new ClassificationTypeProvider(classificationService);
            treeUpdater.TextChanged += span =>
            {
                TagsChanged?.Invoke(this, new SnapshotSpanEventArgs(new SnapshotSpan(span.Snapshot, span.Start, span.Snapshot.Length - span.Start)));
            };
        }

        IEnumerable<ITagSpan<ClassificationTag>> ITagger<ClassificationTag>.GetTags(NormalizedSnapshotSpanCollection spans)
        {
            var tree = treeUpdater.GetRoot(spans[0].Snapshot.TextBuffer);

            ITextSnapshot snapshot = spans[0].Snapshot;

            List<SyntaxNode> children = GetChildrenAt(tree, spans);

            foreach (FpSyntaxNode child in children)
            {
                if (child.Kind == SyntaxKind.MultiLineComment)
                {
                    List<ITrackingSpan> tspans = new List<ITrackingSpan>(multiLineComments.Where(s =>
                        s.GetSpan(snapshot).Span == new Span(child.Start, child.Length)));
                    if (tspans.Count == 0)
                    {
                        multiLineComments.Add(snapshot.CreateTrackingSpan(new Span(child.Start, child.Length), SpanTrackingMode.EdgeExclusive));
                        TagsChanged?.Invoke(this, new SnapshotSpanEventArgs(new SnapshotSpan(snapshot, new Span(child.Start, child.Length))));
                    }
                }
                else
                {
                    CheckMultiLineCommentNotBroken(child, snapshot);
                }
                IClassificationType classificationType = classificationProvider.GetClassificationType(child);
                if (classificationType == null) continue;
                yield return new TagSpan<ClassificationTag>(new SnapshotSpan(snapshot, child.Start, child.Length),
                    new ClassificationTag(classificationType));
            }
        }

        private Span SpanCollectionUnion(NormalizedSnapshotSpanCollection spans)
        {
            int start = int.MaxValue;
            int end = int.MinValue;

            foreach (var span in spans)
            {
                start = Math.Min(start, span.Start);
                end = Math.Max(end, span.End);
            }

            return new Span(start, end - start);
        }

        private void CheckMultiLineCommentNotBroken(FpSyntaxNode child, ITextSnapshot snapshot)
        {
            List<ITrackingSpan> tspans = new List<ITrackingSpan>(multiLineComments.Where(s =>
                    s.GetSpan(snapshot).OverlapsWith(new Span(child.Start, child.Length))));
            if (tspans.Count > 0)
            {
                int start = snapshot.Length;
                int end = 0;

                foreach (var s in tspans)
                {
                    var changedSpan = s.GetSpan(snapshot);
                    start = Math.Min(start, changedSpan.Start);
                    end = Math.Max(end, changedSpan.End);
                    multiLineComments.Remove(s);
                }
                TagsChanged?.Invoke(this, new SnapshotSpanEventArgs(new SnapshotSpan(snapshot, new Span(start, end - start))));
            }
        }

        private List<SyntaxNode> GetChildrenAt(ISyntaxTree tree, NormalizedSnapshotSpanCollection spans)
        {
            Span totalSpan = SpanCollectionUnion(spans);

            var version = spans[0].Snapshot.Version;
            if (lastSpan != totalSpan || version != lastVersion)
            {
                lastSpan = totalSpan;
                lastVersion = version;
                lastChildren = tree.Root.ChildrenIn(new TextSpan(totalSpan.Start, totalSpan.Length)).ToList();
            }

            return lastChildren;
        }

        IEnumerable<ITagSpan<ErrorTag>> ITagger<ErrorTag>.GetTags(NormalizedSnapshotSpanCollection spans)
        {
            var tree = treeUpdater.GetRoot(spans[0].Snapshot.TextBuffer);

            ITextSnapshot snapshot = spans[0].Snapshot;
            List<SyntaxNode> children = GetChildrenAt(tree, spans);

            foreach (var child in children)
            {
                if (child is UnexpectedTrivia)
                {
                    yield return new TagSpan<ErrorTag>(new SnapshotSpan(snapshot, child.Start, child.Length),
                        new ErrorTag(PredefinedErrorTypeNames.SyntaxError, child.ToString()));
                }
            }
        }
    }
}