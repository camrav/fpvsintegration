﻿using Microsoft.VisualStudio.Utilities;
using System.ComponentModel.Composition;

namespace ProjectSystem.CodeAnalysis
{
    internal static class FileAndContentTypeDefinitions
    {
        [Export]
        [Name(FpConstants.ContentType)]
        [BaseDefinition("code")]
        internal static ContentTypeDefinition FreePascalContentTypeDefinition = null;
        
        [Export]
        [FileExtension(".pas")]
        [ContentType(FpConstants.ContentType)]
        internal static FileExtensionToContentTypeDefinition FreePascalExtensionDefinition = null;
    }
}