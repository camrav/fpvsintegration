﻿using CodeAnalysis.Semantics;
using CodeAnalysis.SyntaxTree;
using CodeAnalysis.SyntaxTree.OnDemandTree;
using FpLexer;
using Microsoft.VisualStudio.Text.Classification;
using System.ComponentModel.Composition;

namespace ProjectSystem.CodeAnalysis
{
    internal class ClassificationTypeProvider
    {
        private IClassificationTypeRegistryService classificationService;

        private IClassificationType keywordType;
        private IClassificationType identifierType;
        private IClassificationType commentType;
        private IClassificationType stringType;
        private IClassificationType typeType;
        private IClassificationType assemblerType;
        private IClassificationType punctuationType;
        private IClassificationType operatorType;
        private IClassificationType whitespaceType;
        private IClassificationType enumType;
        
        public ClassificationTypeProvider(IClassificationTypeRegistryService classificationService)
        {
            this.classificationService = classificationService;
            keywordType = classificationService.GetClassificationType("Keyword");
            identifierType = classificationService.GetClassificationType("Identifier");
            commentType = classificationService.GetClassificationType("Comment");
            stringType = classificationService.GetClassificationType("String");
            typeType = classificationService.GetClassificationType("class name");
            assemblerType = classificationService.GetClassificationType("preprocessor keyword");
            punctuationType = classificationService.GetClassificationType("punctuation");
            operatorType = classificationService.GetClassificationType("operator");
            whitespaceType = classificationService.GetClassificationType("whitespace");
            enumType = classificationService.GetClassificationType("enum name");
        }

        public IClassificationType GetClassificationType(SyntaxNode node)
        {
            var fpNode = node as FpSyntaxNode;
            if (fpNode.Kind.IsKeyword() || fpNode.Kind.IsModifier() || fpNode.Kind == SyntaxKind.NilConstant)
            {
                return keywordType;
            }
            else if (fpNode is CommentTrivia)
            {
                return commentType;
            }
            else if (fpNode.Kind == SyntaxKind.StringConstant)
            {
                return stringType;
            }
            else if (fpNode.Parent is FpEnumMembers || fpNode.Parent is FpInitializedEnumMember)
            {
                return enumType;
            }
            else if (node is FpSyntaxIdentifier)
            {
                var symbol = (fpNode.Tree as FpSyntaxTree).SemanticModel.GetSymbol(node as FpSyntaxIdentifier);

                if (fpNode.Kind == SyntaxKind.TypeIdentifier)
                    return typeType;
                else if (symbol != null && symbol.SymbolKind == SymbolKind.Constant && symbol.ContainingSymbol is EnumTypeSymbol)
                {
                    return enumType;
                }
                else
                    return identifierType;
            }
            else if (node is AssemblerTrivia)
            {
                return assemblerType;
            }
            else if (fpNode.Kind.IsOperator())
            {
                return operatorType;
            }
            else if (fpNode.Kind.IsPunctuation())
            {
                return punctuationType;
            }
            else if (fpNode is WhitespaceTrivia)
            {
                return whitespaceType;
            }

            return null;
        }
    }
}