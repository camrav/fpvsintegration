﻿using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Classification;
using Microsoft.VisualStudio.Text.Tagging;
using Microsoft.VisualStudio.Utilities;
using System.ComponentModel.Composition;

namespace ProjectSystem.CodeAnalysis
{
    [Export(typeof(ITaggerProvider))]
    [ContentType(FpConstants.ContentType)]
    [TagType(typeof(ClassificationTag))]
    [TagType(typeof(ErrorTag))]
    internal sealed class TaggerProvider : ITaggerProvider
    {
        [Import]
        internal IClassificationTypeRegistryService ClassificationTypeRegistry = null;

        [Import(typeof(TreeUpdater))]
        public TreeUpdater TreeUpdater { get; set; }

        public ITagger<T> CreateTagger<T>(ITextBuffer buffer) where T : ITag
        {
            return new FpHighlightningTagger(ClassificationTypeRegistry, TreeUpdater) as ITagger<T>;
        }
    }
}