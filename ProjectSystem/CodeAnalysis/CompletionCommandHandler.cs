﻿using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Language.Intellisense;
using Microsoft.VisualStudio.OLE.Interop;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.TextManager.Interop;
using System;
using System.Runtime.InteropServices;
using System.Windows.Input;

namespace ProjectSystem.CodeAnalysis
{
    internal class CompletionCommandHandler : IOleCommandTarget
    {
        private IOleCommandTarget nextCommandHandler;
        private ITextView textView;
        private CompletionHandlerProvider provider;
        private ICompletionSession session;

        public CompletionCommandHandler(IVsTextView textViewAdapter, ITextView textView, CompletionHandlerProvider provider)
        {
            this.textView = textView;
            this.provider = provider;

            textViewAdapter.AddCommandFilter(this, out nextCommandHandler);
        }

        public int Exec(ref Guid pguidCmdGroup, uint nCmdID, uint nCmdexecopt, IntPtr pvaIn, IntPtr pvaOut)
        {
            if (VsShellUtilities.IsInAutomationFunction(provider.ServiceProvider))
            {
                return nextCommandHandler.Exec(pguidCmdGroup, nCmdID, nCmdexecopt, pvaIn, pvaOut);
            }

            uint commandId = nCmdID;
            char typedChar = char.MinValue;

            if (pguidCmdGroup == VSConstants.VSStd2K && nCmdID == (uint)VSConstants.VSStd2KCmdID.TYPECHAR)
            {
                typedChar = (char)(ushort)Marshal.GetObjectForNativeVariant(pvaIn);
            }

            bool doNotWrite = false;
            if (nCmdID == (uint)VSConstants.VSStd2KCmdID.RETURN || nCmdID == (uint)VSConstants.VSStd2KCmdID.TAB || (char.IsWhiteSpace(typedChar)) || (char.IsPunctuation(typedChar) && typedChar != '_'))
            {
                if (session != null && !session.IsDismissed)
                {
                    if (session.SelectedCompletionSet.SelectionStatus.IsSelected)
                    {
                        session.Commit();
                        if (nCmdID == (uint)VSConstants.VSStd2KCmdID.RETURN) // Enter button
                            doNotWrite = true;
                    }
                    else
                    {
                        session.Dismiss();
                    }
                }
                else //init new session if there Ctrl + space
                {
                    if (Keyboard.Modifiers == ModifierKeys.Control && typedChar == ' ')
                    {
                        TriggerCompletion();
                        return VSConstants.S_OK;
                    }
                }
            }

            int retVal = VSConstants.S_OK;
            if (!doNotWrite)
                retVal = nextCommandHandler.Exec(pguidCmdGroup, nCmdID, nCmdexecopt, pvaIn, pvaOut);

            bool handled = false;

            if (!typedChar.Equals(char.MinValue) && (char.IsLetterOrDigit(typedChar) || typedChar == '_' || typedChar == '.'))
            {
                if (session == null || session.IsDismissed)
                {
                    TriggerCompletion();
                }
                session?.Filter();

                handled = true;
            }
            else if (commandId == (uint)VSConstants.VSStd2KCmdID.BACKSPACE || commandId == (uint)VSConstants.VSStd2KCmdID.DELETE)
            {
                if (session != null && !session.IsDismissed)
                {
                    session.Dismiss();
                    TriggerCompletion(); //recalculate whole session to fix line width in intellisense....
                }
                handled = true;
            }

            if (handled) return VSConstants.S_OK;
            return retVal;
        }

        private bool TriggerCompletion()
        {
            SnapshotPoint? caretPoint = textView.Caret.Position.Point.GetPoint(textBuffer => !textBuffer.ContentType.IsOfType("projection"), PositionAffinity.Predecessor);
            if (!caretPoint.HasValue)
                return false;

            session = provider.CompletionBroker.CreateCompletionSession(textView, caretPoint.Value.Snapshot.CreateTrackingPoint(caretPoint.Value.Position, PointTrackingMode.Positive), true);

            session.Dismissed += OnSessionDismissed;
            session.Start();

            return true;
        }

        private void OnSessionDismissed(object sender, EventArgs e)
        {
            session.Dismissed -= OnSessionDismissed;
            session = null;
        }

        public int QueryStatus(ref Guid pguidCmdGroup, uint cCmds, OLECMD[] prgCmds, IntPtr pCmdText)
        {
            return nextCommandHandler.QueryStatus(ref pguidCmdGroup, cCmds, prgCmds, pCmdText);
        }
    }
}