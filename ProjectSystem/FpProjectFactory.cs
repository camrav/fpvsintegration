﻿using Microsoft.VisualStudio.Project;
using System;
using System.Runtime.InteropServices;
using IOleServiceProvider = Microsoft.VisualStudio.OLE.Interop.IServiceProvider;

namespace ProjectSystem
{
    [Guid(PackageGuids.FpProjectTypeGuidString)]
    internal class FpProjectFactory : ProjectFactory
    {
        private FpPackage freePascalPackage;

        public FpProjectFactory(FpPackage freePascalPackage)
            : base(freePascalPackage)
        {
            this.freePascalPackage = freePascalPackage;
        }

        protected override ProjectNode CreateProject()
        {
            FpProjectNode project = new FpProjectNode(freePascalPackage);

            project.SetSite((IOleServiceProvider)((IServiceProvider)freePascalPackage).GetService(typeof(IOleServiceProvider)));
            return project;
        }
    }
}