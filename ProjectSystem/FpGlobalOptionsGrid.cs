﻿using Microsoft.VisualStudio.Shell;
using System.ComponentModel;

namespace ProjectSystem
{
    internal class FpGlobalOptionsGrid : DialogPage
    {
        [Category("Free pascal")]
        [DisplayName("Match project and program file names.")]
        [Description("When creating new project, rename 'program.pas' to 'projectName.pas'")]
        public bool RenameSoruceFileToMatchProjectName
        {
            get { return Properties.Settings.Default.RenameSourceFileToMatchProjectName; }
            set { Properties.Settings.Default.RenameSourceFileToMatchProjectName = value; }
        }

        [Category("Free pascal")]
        [DisplayName("GDB 32-bit executable path")]
        [Description("GDB executable to use when debugging (32-bit)")]
        public string GdbExecutable32bit
        {
            get { return Properties.Settings.Default.Gdb32bitPath; }
            set { Properties.Settings.Default.Gdb32bitPath = value; }
        }

        [Category("Free pascal")]
        [DisplayName("Pascal source file extensions")]
        [Description("Valid extensions for source files. Delimited by semicolon (';').")]
        public string PascalSourceExtensions
        {
            get { return Properties.Settings.Default.PascalSourceExtensions; }
            set { Properties.Settings.Default.PascalSourceExtensions = value; }
        }

        [Category("Free pascal")]
        [DisplayName("Standard library folder")]
        public string StandardLibraryFolder
        {
            get { return Properties.Settings.Default.StandardLibraryFolder; }
            set { Properties.Settings.Default.StandardLibraryFolder = value; }
        }

        [Category("Free pascal")]
        [DisplayName("PpuDump executable path")]
        public string PpuDumpPath
        {
            get { return Properties.Settings.Default.PpuDumpPath; }
            set { Properties.Settings.Default.PpuDumpPath = value; }
        }
    }
}