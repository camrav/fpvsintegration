﻿namespace ProjectSystem
{
    public static class FpConstants
    {
        public const string OutputPath = "OutputPath";
        public const string OptimizationLevel = "OptimizationLevel";
        public const string AdditionalLibraryPaths = "AdditionalLibraryPaths";
        public const string CompilerArgs = "CompilerArgs";
        public const string CompileSymbols = "CompileSymbols";
        public const string EnableGoto = "EnableGoto";

        public const string LanguageName = "Free Pascal";
        public const string ContentType = "Free Pascal";
    }
}