﻿using Microsoft.Build.Framework;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Project;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using Microsoft.VisualStudio.TextManager.Interop;
using System;
using System.IO;

namespace ProjectSystem
{
    internal class LoggerWithNavigation : IDEBuildLogger
    {
        private FpProjectNode project;
        private TaskProvider taskProvider;

        public LoggerWithNavigation(IVsOutputWindowPane output, TaskProvider taskProvider, IVsHierarchy hierarchy, FpProjectNode project) : base(output, taskProvider, hierarchy)
        {
            this.project = project;
            this.taskProvider = taskProvider;
        }

        protected override Func<ErrorTask> CreateTaskEvent(BuildEventArgs errorEvent)
        {
            return () =>
            {
                ErrorTask errorTask = base.CreateTaskEvent(errorEvent)();

                errorTask.Navigate += (sender, args) => Navigate(errorTask);

                return errorTask;
            };
        }

        private void Navigate(ErrorTask task)
        {
            string filePath = Path.IsPathRooted(task.Document) ? task.Document : Path.Combine(project.ProjectFolder, task.Document);

            uint itemID;
            if (project.ParseCanonicalName(filePath, out itemID) != VSConstants.S_OK)
                return;

            HierarchyNode file = project.NodeFromItemId(itemID);

            IVsWindowFrame documentFrame;
            Guid guid = VSConstants.LOGVIEWID.Code_guid;
            project.OpenItem(file.ID, ref guid, IntPtr.Zero, out documentFrame);
            documentFrame.Show();

            IVsTextView textView = VsShellUtilities.GetTextView(documentFrame);
            textView.SetCaretPos(task.Line, task.Column - 1);
            textView.CenterLines(task.Line, 1);
        }
    }
}