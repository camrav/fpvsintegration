﻿using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Project;
using System;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace ProjectSystem
{
    [Guid(PackageGuids.FpProjectCommonPropertiesGuidString)]
    [DisplayName("Compiler")]
    internal class FpCommonProjectProperties : SettingsPage
    {
        public FpCommonProjectProperties()
        {
            Name = "Common Properties";
        }

        protected override int ApplyChanges()
        {
            return VSConstants.S_OK;
        }

        protected override void BindProperties()
        {
        }
    }
}