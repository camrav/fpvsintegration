﻿using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Project;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ProjectSystem
{
    internal class FpProjectNode : ProjectNode
    {
        private FpPackage package;

        public FpProjectNode(FpPackage package)
        {
            this.package = package;
        }

        public override Guid ProjectGuid => PackageGuids.FpProjectTypeGuid;

        public override string ProjectType => "Free Pascal Project";

        public override void AddFileFromTemplate(string source, string target)
        {
            string safeProjectname = GetSafeProjectName();

            FileTemplateProcessor.AddReplace("$ProjectName$", safeProjectname);
            FileTemplateProcessor.AddReplace("$FileName$", Path.GetFileNameWithoutExtension(target));
            if (Properties.Settings.Default.RenameSourceFileToMatchProjectName && Path.GetFileName(target) == "program.pas")
            {
                string fileName = safeProjectname + ".pas";
                target = Path.Combine(Path.GetDirectoryName(target), fileName);
                BuildProject.Items.Where(item => item.EvaluatedInclude == "program.pas").FirstOrDefault()?.Rename(fileName);
            }

            FileTemplateProcessor.UntokenFile(source, target);
            FileTemplateProcessor.Reset();
        }

        private string GetSafeProjectName()
        {
            string projectName = Path.GetFileNameWithoutExtension(this.FileName);
            StringBuilder builder = new StringBuilder();
            string asciiName = Encoding.ASCII.GetString(Encoding.ASCII.GetBytes(projectName));

            foreach (var c in projectName)
            {
                if ((int)c > 127 || (int)c < 32) continue;
                if (Char.IsLetter(c) || c == '_') builder.Append(c);
                if (Char.IsDigit(c) && builder.Length != 0) builder.Append(c);
            }

            return builder.Length == 0 ? "Program" : builder.ToString();
        }

        protected override ConfigProvider CreateConfigProvider()
        {
            return new FpConfigProvider(this);
        }

        protected override Guid[] GetConfigurationIndependentPropertyPages()
        {
            return new[] { PackageGuids.FpProjectCommonPropertiesGuid };
        }

        protected override Guid[] GetConfigurationDependentPropertyPages()
        {
            return new[] { PackageGuids.FpProjectPropertiesPageGuid };
        }

        protected override IDEBuildLogger GetLogger(IVsOutputWindowPane output, TaskProvider taskProvider, IVsHierarchy hierarchy)
        {
            return new LoggerWithNavigation(output, taskProvider, hierarchy, this);
        }

        public override bool IsCodeFile(string fileName)
        {
            string[] validExtensions = Properties.Settings.Default.PascalSourceExtensions.Split(';');

            string ext = Path.GetExtension(fileName);
            foreach (var extension in validExtensions)
            {
                if (string.Compare(ext, extension, ignoreCase: true) == 0)
                    return true;
            }

            return false;
        }

        public override int DragLeave()
        {
            return VSConstants.S_OK;
        }

        protected override int ExecCommandThatDependsOnSelectedNodes(Guid cmdGroup, uint cmdId, uint cmdExecOpt, IntPtr vaIn, IntPtr vaOut, CommandOrigin commandOrigin, IList<HierarchyNode> selectedNodes, out bool handled)
        {
            if (cmdGroup == PackageGuids.FpCmdSet)
            {
                switch (cmdId)
                {
                    case PackageIds.DeleteItem:
                        handled = true;
                        DeleteNodes(selectedNodes);
                        return VSConstants.S_OK;
                }
            }

            return base.ExecCommandThatDependsOnSelectedNodes(cmdGroup, cmdId, cmdExecOpt, vaIn, vaOut, commandOrigin, selectedNodes, out handled);
        }

        private void DeleteNodes(IList<HierarchyNode> selectedNodes)
        {
            foreach (var node in selectedNodes)
            {
                if (node.ProjectMgr != ProjectMgr)
                    continue; //this node is from different project

                DeleteNode(node);
            }
        }

        private void DeleteNode(HierarchyNode node)
        {
            if (node is FileNode)
            {
                (node as FileNode).Remove(true);
            }
            else if (node is FolderNode)
            {
                (node as FolderNode).Remove(true);
            }
        }
    }
}