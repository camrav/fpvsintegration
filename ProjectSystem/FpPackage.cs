﻿using Microsoft.VisualStudio.Project;
using Microsoft.VisualStudio.Shell;
using System;
using System.Runtime.InteropServices;

namespace ProjectSystem
{
    [PackageRegistration(UseManagedResourcesOnly = true)]
    [InstalledProductRegistration("#110", "#112", "1.0", IconResourceID = 400)] // Info on this package for Help/About
    [Guid(PackageGuids.FpPackageGuidString)]
    [ProvideProjectFactory(typeof(FpProjectFactory), FpConstants.LanguageName, "Free Pascal Project Files (*.fpproj)", "fpproj", "fpproj", @"Templates\Projects\HelloWorldProject", LanguageVsTemplate = "Free Pascal")]
    [ProvideProjectItem(typeof(FpProjectFactory), "Source files", @"Templates\Items\EmptyFile", 100)]
    [ProvideObject(typeof(FpProjectProperties))]
    [ProvideObject(typeof(FpCommonProjectProperties))]
    [ProvideOptionPage(typeof(FpGlobalOptionsGrid), FpConstants.LanguageName, "General", 0, 0, true)]
    [ProvideMenuResource("Menus.ctmenu", 1)]
    public sealed class FpPackage : ProjectPackage
    {
        public FpPackage()
        {
            // Inside this method you can place any initialization code that does not require
            // any Visual Studio service because at this point the package object is created but
            // not sited yet inside Visual Studio environment. The place to do all the other
            // initialization is the Initialize method.
        }

        public override string ProductUserContext => "";

        #region Package Members

        /// <summary>
        /// Initialization of the package; this method is called right after the package is sited, so this is the place
        /// where you can put all the initialization code that rely on services provided by VisualStudio.
        /// </summary>
        protected override void Initialize()
        {
            base.Initialize();

            RegisterProjectFactory(new FpProjectFactory(this));
        }

        #endregion Package Members

    }
}