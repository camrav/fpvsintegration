﻿using Microsoft.VisualStudio.Project;

namespace ProjectSystem
{
    internal class FpConfigProvider : ConfigProvider
    {
        private ProjectNode project;

        public FpConfigProvider(ProjectNode manager) : base(manager)
        {
            project = manager;
        }

        protected override ProjectConfig CreateProjectConfiguration(string configName)
        {
            return new FpProjectConfig(project, configName);
        }
    }
}