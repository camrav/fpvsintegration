﻿using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Project;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace ProjectSystem
{
    [Guid(PackageGuids.FpProjectPropertiesPageGuidString)]
    internal class FpProjectProperties : SettingsPage
    {
        private const string CompilerCategory = "Compiler";
        private const string DebugCategory = "Debug";

        private class LocalPropertyCopy
        {
            public string Value;

            public static implicit operator string(LocalPropertyCopy localCopy)
            {
                return localCopy.Value;
            }
        }

        [Category(CompilerCategory)]
        [DisplayName("Output path")]
        public string OutputPath
        {
            get { return properties[FpConstants.OutputPath]; }
            set { SetProperty(FpConstants.OutputPath, value); }
        }

        [Category(CompilerCategory)]
        [DisplayName("Optimization Level")]
        [Description("Compiler optimizations, valid values between 0 (lowest optimizations) and 3 (highest optimizations), if the value is not between 0  and 3, then the optimization level is set to 0.")]
        public string OptimizationLevel
        {
            get { return properties[FpConstants.OptimizationLevel]; }
            set { SetProperty(FpConstants.OptimizationLevel, value); }
        }

        [Category(CompilerCategory)]
        [DisplayName("Additional Library Paths")]
        public string AdditionalLibraryPaths
        {
            get { return properties[FpConstants.AdditionalLibraryPaths]; }
            set { SetProperty(FpConstants.AdditionalLibraryPaths, value); }
        }

        [Category(DebugCategory)]
        [DisplayName("Console arguments")]
        public string ConsoleArgs
        {
            get { return properties[FpConstants.CompilerArgs]; }
            set { SetProperty(FpConstants.CompilerArgs, value); }
        }

        /*[Category(CompilerCategory)]
        [DisplayName("Additional compile symbols")]
        [Description("Additional compile symbols, separated by semicolon (;).")]
        public string CompileSymbols
        {
            get { return properties[FpConstants.CompileSymbols]; }
            set { SetProperty(FpConstants.CompileSymbols, value); }
        }*/

        /*    [Category(CompilerCategory)]
            [DisplayName("Enable goto statement")]
            [Description("Enable goto statement to be used without having to use {$GOTO ON}.")]
            public string EnableGoto
            {
                get { return properties[FpConstants.EnableGoto]; }
                set { SetProperty(FpConstants.EnableGoto, value); }
            }*/

        private Dictionary<string, LocalPropertyCopy> properties = new Dictionary<string, LocalPropertyCopy>();

        public FpProjectProperties()
        {
            Name = "Compiler";

            InitProperties(FpConstants.OutputPath, FpConstants.OptimizationLevel, FpConstants.AdditionalLibraryPaths,
                FpConstants.CompilerArgs);
        }

        private void InitProperties(params string[] propertyNames)
        {
            foreach (var propertyName in propertyNames)
            {
                properties.Add(propertyName, new LocalPropertyCopy());
            }
        }

        protected override int ApplyChanges()
        {
            ApplyToAll((name, value) => SetConfigProperty(name, value));

            return VSConstants.S_OK;
        }

        protected override void BindProperties()
        {
            ApplyToAll((name, value) => value.Value = GetConfigProperty(name));
        }

        private void ApplyToAll(Action<string, LocalPropertyCopy> action)
        {
            foreach (var item in properties)
            {
                action(item.Key, item.Value);
            }
        }

        private void SetProperty(string propertyName, string value)
        {
            properties[propertyName].Value = value;
            IsDirty = true;
        }
    }
}