﻿using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Project;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace ProjectSystem
{
    internal class FpProjectConfig : ProjectConfig
    {
        private ProjectNode project;

        public FpProjectConfig(ProjectNode project, string configuration) : base(project, configuration)
        {
            this.project = project;
        }

        public override int DebugLaunch(uint grfLaunch)
        {
            try
            {
                VsDebugTargetInfo info = new VsDebugTargetInfo();
                info.cbSize = (uint)Marshal.SizeOf(info);
                info.dlo = DEBUG_LAUNCH_OPERATION.DLO_CreateProcess;

                string projectPath = project.ProjectFolder;
                info.bstrExe = Path.Combine(project.ProjectFolder,
                    GetConfigurationProperty(FpConstants.OutputPath, false),
                    Path.ChangeExtension(Path.GetFileName(project.ProjectFile), ".exe"));
                info.bstrArg = project.GetProjectProperty(FpConstants.CompilerArgs);
                info.bstrCurDir = Path.Combine(project.ProjectFolder, Path.GetDirectoryName(info.bstrExe));
                //info.bstrArg = GetConfigurationProperty("CmdArgs", false);

                info.fSendStdoutToOutputWindow = 0;

                info.clsidCustom = PackageGuids.MIDebuggerGuid;

                if (((__VSDBGLAUNCHFLAGS)grfLaunch & __VSDBGLAUNCHFLAGS.DBGLAUNCH_NoDebug) == __VSDBGLAUNCHFLAGS.DBGLAUNCH_NoDebug)
                {
                    string arg = string.Format("/C title \"{0}\" && \"{1}\" {2} && pause", Path.GetFileName(info.bstrExe), info.bstrExe, info.bstrArg);
                    var proc = Process.Start("cmd.exe", arg); // /C runs the command and then terminates
                    return VSConstants.S_OK;
                }
                else
                {
                    if (DebuggerPathValid())
                    {
                        info.bstrOptions = GetMIOptions(info.bstrExe);
                        info.grfLaunch = grfLaunch;

                        VsShellUtilities.LaunchDebugger(ProjectMgr.Site, info);
                    }
                    else
                    {
                        VsShellUtilities.LogError("FreePascal extension", string.Format("Error: GDB path invalid. Provided path: {0}", Properties.Settings.Default.Gdb32bitPath));
                    }
                }
            }
            catch (Exception e)
            {
                return Marshal.GetHRForException(e);
            }

            return VSConstants.S_OK;
        }

        private bool DebuggerPathValid()
        {
            string gdbPath = GetGdb32BitPath();
            return File.Exists(gdbPath);
        }

        private string GetMIOptions(string exePath)
        {
            StringBuilder sb = new StringBuilder();

            string gdbPath = GetGdb32BitPath();

            sb.AppendLine("<LocalLaunchOptions xmlns=\"http://schemas.microsoft.com/vstudio/MDDDebuggerOptions/2014\"");
            sb.AppendFormat("MIDebuggerPath=\"{0}\" \n", gdbPath);
            sb.AppendFormat("ExePath=\"{0}\" \n", exePath);
            sb.AppendLine("TargetArchitecture=\"x86\"");
            sb.AppendLine("MIMode=\"gdb\"");
            sb.AppendLine("ExternalConsole=\"true\"");
            sb.AppendLine(">");
            sb.AppendLine("<SetupCommands>");
            sb.AppendLine("<Command>set disassembly-flavor intel</Command>");
            sb.AppendLine("<Command>set language pascal</Command>");
            sb.AppendLine("</SetupCommands>");
            sb.AppendLine("</LocalLaunchOptions>");

            return sb.ToString();
            // return string2;
        }

        private string GetGdb32BitPath()
        {
            string path = Properties.Settings.Default.Gdb32bitPath;
            if (!File.Exists(path))
            {
                Process process = new Process
                {
                    StartInfo = new ProcessStartInfo
                    {
                        FileName = "where",
                        Arguments = "gdb",
                        RedirectStandardOutput = true,
                        CreateNoWindow = true,
                        UseShellExecute = false
                    }
                };

                process.Start();
                process.WaitForExit();

                if (process.ExitCode != 0)
                    return null;

                string filePath = process.StandardOutput.ReadToEnd();

                Properties.Settings.Default.Gdb32bitPath = filePath;
                Properties.Settings.Default.Save();
            }

            return path;
        }
    }
}