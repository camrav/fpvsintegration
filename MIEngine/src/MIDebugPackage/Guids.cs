﻿// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

// MUST match guids.h

using System;

namespace Microsoft.MIDebugPackage
{
    internal static class GuidList
    {
        public const string guidMIDebugPackagePkgString = "93F8D5EB-228F-4EE8-A6B3-81CCB62BB332";
        public const string guidMIDebugPackageCmdSetString = "70040C64-2EB4-4FA9-9087-E80BC319CDD3";

        public static readonly Guid guidMIDebugPackageCmdSet = new Guid(guidMIDebugPackageCmdSetString);
    };
}
