﻿using System.Collections.Generic;

namespace FpLexer
{
    public class TokenSequence
    {
        private IEnumerator<Token> tokens;
        private List<TriviaToken> currentTrivia;
        private bool sequenceEnd;

        public TokenSequence(IEnumerable<Token> tokens)
        {
            this.tokens = tokens.GetEnumerator();
            EatToken();
        }

        public void EatToken()
        {
            sequenceEnd = !tokens.MoveNext();
            currentTrivia = null;
        }

        /// <summary>
        /// Returns all trivia tokens until Peek() != trivia
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TriviaToken> GetAllTrivia()
        {
            if (currentTrivia != null)
            {
                var result = currentTrivia;
                currentTrivia = null;
                return result;
            }
            else
                return GetAllTriviaImpl();
        }

        private IEnumerable<TriviaToken> GetAllTriviaImpl()
        {
            while (!sequenceEnd)
            {
                if (tokens.Current is TriviaToken)
                {
                    yield return tokens.Current as TriviaToken;
                    sequenceEnd = !tokens.MoveNext();
                }
                else
                    break;
            }
        }

        public Token PeekNextNonTrivia()
        {
            if (currentTrivia == null)
                currentTrivia = new List<TriviaToken>(GetAllTriviaImpl());

            if (sequenceEnd)
                return new EndOfSequence();

            return tokens.Current;
        }
    }
}