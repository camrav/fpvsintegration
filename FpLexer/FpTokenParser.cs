﻿using Antlr4.Runtime;
using System.Collections.Generic;

namespace FpLexer
{
    public class FpTokenParser
    {
        public FpTokenParser()
        {
        }

        public IEnumerable<Token> GetTokens(ISourceText source)
        {
            string input = source.GetText();

            AntlrInputStream inputStream = new AntlrInputStream(input);
            AntlrLexer lexer = new AntlrLexer(inputStream);

            while (true)
            {
                IToken tok = lexer.NextToken();
                SyntaxKind kind = (SyntaxKind)tok.Type;
                if (kind.IsKeyword() || kind.IsModifier())
                {
                    yield return new KeywordToken(tok.Text, kind);
                }
                else if (kind.IsPunctuation() || kind.IsOperator())
                {
                    yield return new PunctuationToken(tok.Text, kind);
                }
                else if (kind == SyntaxKind.Comment || kind == SyntaxKind.MultiLineComment)
                {
                    yield return new CommentToken(tok.Text, kind);
                }
                else if (kind == SyntaxKind.StringConstant)
                {
                    yield return new StringToken(tok.Text);
                }
                else if (kind == SyntaxKind.NilConstant)
                {
                    yield return new NilToken(tok.Text);
                }
                else if (kind == SyntaxKind.Whitespace)
                {
                    yield return new WhitespaceToken(tok.Text);
                }
                else if (kind == SyntaxKind.UintConstant)
                {
                    yield return new UintToken(tok.Text);
                }
                else if (kind == SyntaxKind.RealConstant)
                {
                    yield return new RealToken(tok.Text);
                }
                else if (kind == SyntaxKind.IdentifierToken)
                {
                    yield return new IdentifierToken(tok.Text);
                }
                else if (kind == SyntaxKind.Unknown)
                {
                    yield return new UnknownToken(tok.Text);
                }
                else if (tok.Type == -1) //EOF
                    break;
            }
        }
    }
}