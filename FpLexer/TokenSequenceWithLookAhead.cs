﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FpLexer
{
    public class TokenSequenceWithLookAhead
    {
        private TokenSequence sequence;
        private Queue<Tuple<List<TriviaToken>, Token>> currentTokens = new Queue<Tuple<List<TriviaToken>, Token>>();

        public TokenSequenceWithLookAhead(TokenSequence sequence)
        {
            this.sequence = sequence;
        }

        public void EatToken()
        {
            currentTokens.Dequeue();
        }

        public IEnumerable<TriviaToken> GetAllTrivia()
        {
            EnsureEnoughQueued(0);
            return currentTokens.Peek().Item1;
        }

        public Token PeekNextNonTrivia(int lookahead = 0)
        {
            EnsureEnoughQueued(lookahead);
            return currentTokens.ElementAt(lookahead).Item2;
        }

        private void EnsureEnoughQueued(int lookahead)
        {
            while (currentTokens.Count <= lookahead)
            {
                List<TriviaToken> nextTrivia = new List<TriviaToken>(sequence.GetAllTrivia());
                Token nextToken = sequence.PeekNextNonTrivia();
                currentTokens.Enqueue(new Tuple<List<TriviaToken>, Token>(nextTrivia, nextToken));
                sequence.EatToken();
            }
        }
    }
}