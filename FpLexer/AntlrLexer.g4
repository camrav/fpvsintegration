lexer grammar AntlrLexer;
@header
{
 // using CodeAnalysis.FpTokens;
}


BEGIN: B E G I N { Kind = SyntaxKind.BeginToken; };
END: E N D { Kind = SyntaxKind.EndToken; };
PROGRAM: P R O G R A M { Kind = SyntaxKind.ProgramToken; };
VAR: V A R { Kind = SyntaxKind.VarToken; };
IF: I F { Kind = SyntaxKind.IfToken; };
DIV: D I V { Kind = SyntaxKind.DivToken; };
FOR: F O R { Kind = SyntaxKind.ForToken; };
REPEAT: R E P E A T { Kind = SyntaxKind.RepeatToken; };
PROCEDURE: P R O C E D U R E { Kind = SyntaxKind.ProcedureToken; };
FUNCTION: F U N C T I O N { Kind = SyntaxKind.FunctionToken; };
TYPE: T Y P E { Kind = SyntaxKind.TypeToken; };
USES: U S E S { Kind = SyntaxKind.UsesToken; };
IN: I N { Kind = SyntaxKind.InToken; };
ASM: A S M { Kind = SyntaxKind.AsmToken; };
THEN: T H E N { Kind = SyntaxKind.ThenToken; };
ELSE: E L S E { Kind = SyntaxKind.ElseToken; };
TO: T O { Kind = SyntaxKind.ToToken; };
DOWNTO: D O W N T O { Kind = SyntaxKind.DowntoToken; };
DO: D O { Kind = SyntaxKind.DoToken; };
UNTIL: U N T I L { Kind = SyntaxKind.UntilToken; };
WHILE: W H I L E { Kind = SyntaxKind.WhileToken; };
WITH: W I T H { Kind = SyntaxKind.WithToken; };
GOTO: G O T O { Kind = SyntaxKind.GotoToken; };
CASE: C A S E { Kind = SyntaxKind.CaseToken; };
OF: O F { Kind = SyntaxKind.OfToken; };
OTHERWISE: O T H E R W I S E { Kind = SyntaxKind.OtherwiseToken; };
IS: I S { Kind = SyntaxKind.IsToken; };
OR: O R { Kind = SyntaxKind.OrToken; };
XOR: X O R { Kind = SyntaxKind.XorToken; };
MOD: M O D { Kind = SyntaxKind.ModToken; };
AND: A N D { Kind = SyntaxKind.AndToken; };
SHL: S H L { Kind = SyntaxKind.ShlToken; };
SHR: S H R { Kind = SyntaxKind.ShrToken; };
AS: A S { Kind = SyntaxKind.AsToken; };
NOT: N O T { Kind = SyntaxKind.NotToken; };
LABEL: L A B E L { Kind = SyntaxKind.LabelToken; };
CONST: C O N S T { Kind = SyntaxKind.ConstToken; };
ARRAY: A R R A Y { Kind = SyntaxKind.ArrayToken; };
RECORD: R E C O R D { Kind = SyntaxKind.RecordToken; };
OUT: O U T { Kind = SyntaxKind.OutToken; };
DEPRECATED: D E P R E C A T E D { Kind = SyntaxKind.DeprecatedToken; };
EXPERIMENTAL: E X P E R I M E N T A L { Kind = SyntaxKind.ExperimentalToken; };
PLATFORM: P L A T F O R M { Kind = SyntaxKind.PlatformToken; };
UNIMPLEMENTED: U N I M P L E M E N T E D { Kind = SyntaxKind.UnimplementedToken; };
ABSOLUTE: A B S O L U T E { Kind = SyntaxKind.AbsoluteToken; };
EXPORT: E X P O R T { Kind = SyntaxKind.ExportToken; };
CVAR: C V A R { Kind = SyntaxKind.CvarToken; };
EXTERNAL: E X T E R N A L { Kind = SyntaxKind.ExternalToken; };
NAME: N A M E { Kind = SyntaxKind.NameToken; };
FORWARD: F O R W A R D { Kind = SyntaxKind.ForwardToken; };
ASSEMBLER: A S S E M B L E R { Kind = SyntaxKind.AssemblerToken; };
INDEX: I N D E X { Kind = SyntaxKind.IndexToken; };
NIL: N I L {Kind = SyntaxKind.NilConstant; };
FILE: F I L E {Kind = SyntaxKind.FileToken;};
SET: S E T {Kind = SyntaxKind.SetToken;};
STRING: S T R I N G {Kind = SyntaxKind.StringToken;};
ANSISTRING: A N S I S T R I N G {Kind = SyntaxKind.AnsistringToken;};

DOT: '.' { Kind = SyntaxKind.DotToken; };
COLON: ':' { Kind = SyntaxKind.ColonToken; };
SEMICOLON: ';' { Kind = SyntaxKind.SemicolonToken; };
COMMA: ',' { Kind = SyntaxKind.CommaToken; };
OPENBRACKET: '[' { Kind = SyntaxKind.OpenBracketToken; };
CLOSEBRACKET: ']' { Kind = SyntaxKind.CloseBracketToken; };
OPENPARANTHESIS: '(' { Kind = SyntaxKind.OpenParanthesisToken; };
CLOSEPARANTHESIS: ')' { Kind = SyntaxKind.CloseParanthesisToken; };
ROOF: '^' { Kind = SyntaxKind.RoofToken; };
AT: '@' { Kind = SyntaxKind.AtToken; };
DOTDOT: '..' { Kind = SyntaxKind.DotDotToken; };
ASSIGN: ':=' { Kind = SyntaxKind.AssignToken; };
PLUS: '+' { Kind = SyntaxKind.PlusToken; };
MINUS: '-' { Kind = SyntaxKind.MinusToken; };
ASTERISK: '*' { Kind = SyntaxKind.AsteriskToken; };
SLASH: '/' { Kind = SyntaxKind.SlashToken; };
EQUALS: '=' { Kind = SyntaxKind.EqualsToken; };
DOESNOTEQUAL: '<>' { Kind = SyntaxKind.DoesNotEqualToken; };
PLUSEQUALS: '+=' { Kind = SyntaxKind.PlusEqualsToken; };
MINUSEQUALS: '-=' { Kind = SyntaxKind.MinusEqualsToken; };
ASTERISKEQUALS: '*=' { Kind = SyntaxKind.AsteriskEqualsToken; };
SLASHEQUALS: '/=' { Kind = SyntaxKind.SlashEqualsToken; };
LESSTHAN: '<' { Kind = SyntaxKind.LessThanToken; };
GREATERTHAN: '>' { Kind = SyntaxKind.GreaterThanToken; };
LESSOREQUALTHAN: '<=' { Kind = SyntaxKind.LessOrEqualThanToken; };
GREATEROREQUALTHAN: '>=' { Kind = SyntaxKind.GreaterOrEqualThanToken; };
LEFTSHIFT: '<<' { Kind = SyntaxKind.LeftShiftToken; }; 
RIGHTSHIFT: '>>' { Kind = SyntaxKind.RightShiftToken; };

OneLineComment: '//' ~[\n]* { Kind = SyntaxKind.Comment; };
MultiLineCommentOld: '(*' (~'*'* '*'+ ~[*)])* ~'*'* '*'+ ')' { Kind = SyntaxKind.MultiLineComment; };
MultiLineCommentOldIncomplete: '(*' (~'*'* '*'+ ~[*)])* ~'*'* '*'* { Kind = SyntaxKind.MultiLineComment; };
MultiLineCommentNew: '{' ~'}'* '}' { Kind = SyntaxKind.MultiLineComment; };
MultiLineCommentNewIncomplete: '{' ~'}'* { Kind = SyntaxKind.MultiLineComment; };

String: '\'' (~['\n] | '\'\'')* ['\n] {Kind = SyntaxKind.StringConstant;};
Int: ([0-9]+ | '$' [0-9a-fA-F]+ | '&' [0-7]+ | '%' [0-1]+) {Kind = SyntaxKind.UintConstant;};
Real: [0-9]+ ( '.' [0-9]+ )? ( E [+-]? [0-9]+ )? {Kind = SyntaxKind.RealConstant;};

Identifier: '&'? [a-zA-Z_] [a-zA-Z0-9_]* {Kind = SyntaxKind.IdentifierToken;};


Whitespace : [ \n\r]+ {Kind = SyntaxKind.Whitespace;};
ERROR : . { Kind = SyntaxKind.Unknown;};

EndOfFile : EOF { Kind = SyntaxKind.None; };



fragment A: [aA];
fragment B: [bB];
fragment C: [cC];
fragment D: [dD];
fragment E: [eE];
fragment F: [fF];
fragment G: [gG];
fragment H: [hH];
fragment I: [iI];
fragment J: [jJ];
fragment K: [kK];
fragment L: [lL];
fragment M: [mM];
fragment N: [nN];
fragment O: [oO];
fragment P: [pP];
fragment Q: [qQ];
fragment R: [rR];
fragment S: [sS];
fragment T: [tT];
fragment U: [uU];
fragment V: [vV];
fragment W: [wW];
fragment X: [xX];
fragment Y: [yY];
fragment Z: [zZ];
