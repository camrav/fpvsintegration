﻿namespace FpLexer
{
    public interface ISourceText
    {
        string GetText();
    }
}