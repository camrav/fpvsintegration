﻿namespace FpLexer
{
    public abstract class Token
    {
        public virtual string UnderlyingText { get; set; }
        public SyntaxKind Kind { get; }

        public Token(string underlyingText, SyntaxKind kind)
        {
            UnderlyingText = underlyingText;
            Kind = kind;
        }

        public override string ToString()
        {
            return string.Format("{0}: {1}", Kind, UnderlyingText);
        }
    }

    public abstract class TriviaToken : Token
    {
        public TriviaToken(string underlyingText, SyntaxKind kind) : base(underlyingText, kind)
        {
        }
    }

    public abstract class ConstantToken : Token
    {
        public ConstantToken(string underlyingText, SyntaxKind kind) : base(underlyingText, kind)
        {
        }
    }

    public class WhitespaceToken : TriviaToken
    {
        public WhitespaceToken(string underlyingText) : base(underlyingText, SyntaxKind.Whitespace)
        { }
    }

    public class KeywordToken : Token
    {
        public KeywordToken(string underlyingText, SyntaxKind kind) : base(underlyingText, kind)
        {
        }
    }

    public class PunctuationToken : Token
    {
        public PunctuationToken(string underlyingText, SyntaxKind kind)
            : base(underlyingText, kind)
        {
        }
    }

    public class EndOfSequence : Token
    {
        public EndOfSequence() : base(string.Empty, SyntaxKind.None)
        { }
    }

    public class UnknownToken : TriviaToken
    {
        public UnknownToken(string underlyingText) : base(underlyingText, SyntaxKind.Unknown)
        {
        }
    }

    public class IdentifierToken : Token
    {
        public string Identifier => UnderlyingText.ToUpperInvariant();

        public IdentifierToken(string underlyingText, SyntaxKind identifierType = SyntaxKind.IdentifierToken) : base(underlyingText, identifierType)
        { }
    }

    public class CommentToken : TriviaToken
    {
        public CommentToken(string underlyingText, SyntaxKind kind = SyntaxKind.Comment) : base(underlyingText, kind)
        {
        }
    }

    public class StringToken : ConstantToken
    {
        /// <summary>
        /// Gets the content of the string
        /// </summary>
        public string Text { get; private set; }

        public bool EndsCorrectly { get; private set; } = true;

        public StringToken(string underlyingText) : base(underlyingText, SyntaxKind.StringConstant)
        {
            int len = underlyingText.Length - 2;
            if (underlyingText[underlyingText.Length - 1] != '\'')
            {
                len++;
                EndsCorrectly = false;
            }

            Text = underlyingText.Substring(1, len).Replace("''", "'");
        }

        public override string ToString()
        {
            return string.Format("{0}: \"{1}\"", Kind, UnderlyingText);
        }
    }

    public class UintToken : ConstantToken
    {
        public bool Overflow { get; private set; }
        public int Value { get; private set; }

        public UintToken(string underlyingText) : base(underlyingText, SyntaxKind.UintConstant)
        {
            int result;
            Overflow = !int.TryParse(underlyingText, out result);
            Value = result;
        }
    }

    public class NilToken : ConstantToken
    {
        public NilToken(string underlyingText) : base(underlyingText, SyntaxKind.NilConstant)
        {
        }
    }

    public class RealToken : ConstantToken
    {
        public bool ErrorOnParsing { get; private set; }
        public double Value { get; private set; }

        public RealToken(string underlyingText) : base(underlyingText, SyntaxKind.RealConstant)
        {
            double result;
            ErrorOnParsing = !double.TryParse(underlyingText, out result);
            Value = result;
        }
    }
}