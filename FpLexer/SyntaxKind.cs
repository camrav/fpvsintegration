﻿namespace FpLexer
{
    public enum SyntaxKind
    {
        None = 0,

        //Identifier types
        ProgramIdentifier = 10,

        TypeIdentifier,
        IdentifierToken,
        ProcedureIdentifier,
        FunctionIdentifier,
        LabelIdentifier,
        ConstantIdentifier,
        EnumIdentifier,
        VariableIdentifier,
        EnumMemberIdentifier,
        NewIdentifier,
        FieldIdentifier,

        //keywords
        // start at 50
        ProgramToken = 50,

        BeginToken,
        EndToken,
        IfToken,
        DivToken,
        ForToken,
        RepeatToken,
        ProcedureToken,
        FunctionToken,
        TypeToken,
        VarToken,
        UsesToken,
        InToken,
        AsmToken,
        ThenToken,
        ElseToken,
        ToToken,
        DowntoToken,
        DoToken,
        UntilToken,
        WhileToken,
        WithToken,
        GotoToken,
        CaseToken,
        OfToken,
        OtherwiseToken,
        IsToken,
        OrToken,
        XorToken,
        ModToken,
        AndToken,
        ShlToken,
        ShrToken,
        AsToken,
        NotToken,
        LabelToken,
        ConstToken,
        ArrayToken,
        RecordToken,
        OutToken,
        FileToken,
        SetToken,
        StringToken,

        //punctuation
        // start at 200
        SemicolonToken = 200,

        DotToken,
        ColonToken,
        CommaToken,
        OpenBracketToken,
        CloseBracketToken,
        OpenParanthesisToken,
        CloseParanthesisToken,
        RoofToken,
        AtToken,
        DotDotToken,

        //operators
        // start at 220

        //relational operators
        EqualsToken = 220,

        DoesNotEqualToken,
        LessThanToken,
        GreaterThanToken,
        LessOrEqualThanToken,
        GreaterOrEqualThanToken,

        //others
        AssignToken = 230,

        PlusToken,
        MinusToken,
        AsteriskToken,
        SlashToken,
        PlusEqualsToken,
        MinusEqualsToken,
        AsteriskEqualsToken,
        SlashEqualsToken,
        LeftShiftToken,
        RightShiftToken,

        //constants
        //start at 260
        StringConstant = 260,

        UintConstant,
        NilConstant,
        RealConstant,

        //trivia
        // start at 300
        Whitespace = 300,

        Comment,
        MultiLineComment,
        Unknown,
        MissingToken,

        EndOfFile = 350,

        //modifiers
        //start at 400
        UnimplementedToken = 400,

        DeprecatedToken,
        ExperimentalToken,
        PlatformToken,
        AbsoluteToken,
        ExportToken,
        CvarToken,
        ExternalToken,
        NameToken,
        ForwardToken,
        AssemblerToken,
        IndexToken,
        AnsistringToken,

        //non-terminals
        //NonTerminal,
        Constant = 1000,

        ProgramRoot,
        Block,
        VarDeclaration,
        StatementSequence,
        VarDeclarationLine,
        Expression,
        VarDeclarationLines,
        ActualParameterList,
        ActualNonEmptyParameterList,

        //statements
        Statement,

        NoStatement,
        AssignStatement,
        CallStatement,

        //GotoStatement
        //raise statement
        CompoundStatement,

        IfStatement,
        ForLoopStatement,
        Variable,
        ForDirection,
        WhileLoopStatement,
        RepeatLoopStatement,
        AssemblerStatement,
        RegisterList,
        StringConstantList,
        WithStatement,
        VariableList,
        GotoStatement,
        Label,
        CaseStatement,
        CaseList,
        CaseElse,
        Case,
        OrdinalConstantList,
        OrdinalConstant,
        OrdinalConstantNoRange,
        SimpleExpression,
        SignBetweenExpressions,
        Term,
        SignBetweenTerms,
        Factor,
        SignBetweenFactors,
        DeclarationBlock,
        Declaration,
        LabelDeclaration,
        LabelList,
        ConstantDeclaration,
        ConstDeclarationLine,
        ConstDeclarationLines,
        ConstUntypedDeclarationLine,
        HintDirective,
        Type,
        VariableInitialization,
        VariableModifiers,
        VariableIdentifierList,
        VariableModifier,
        AbsoluteModifier,
        ExportModifier,
        CvarModifier,
        ExternalModifier,
        ArrayType,
        SubrangeList,
        Subrange,
        RecordType,
        FieldList,
        FieldLine,
        PointerType,
        EnumType,
        EnumMemberList,
        EnumMember,
        InitializedEnumMember,
        TypeDeclaration,
        TypeDeclarationLines,
        TypeDeclarationLine,
        ProcedureDeclaration,
        ProcedureHeader,
        SubroutineBlock,
        ExternalDirective,
        AssemblerBlock,
        FormalParameterList,
        SubroutineModifiers,
        ParameterDeclarations,
        ParameterDeclaration,
        NonEmptyParameterDeclarations,
        ValueParameter,
        VariableParameter,
        OutParameter,
        ConstantParameter,
        FunctionDeclaration,
        FunctionHeader,
        NewLabel,
        IndexList,
        UsesBlock,
        UnitList,
        UnitDeclarationLine,
        SubprogramIdentifier,
        FileType,
        SetType,
        StringWithFixedSizeType,
        StringWithCodePageType,
        DereferencedPointerVariable,
        RecordFieldVariable,
        ArrayVariable,
        ActualParameter,
    }

    public static class SyntaxKindExtensions
    {
        public static bool IsTerminal(this SyntaxKind kind)
        {
            return (int)kind < 1000;
        }

        public static bool IsTrivia(this SyntaxKind kind)
        {
            return (int)kind >= 300 && (int)kind < 400;
        }

        public static bool IsModifier(this SyntaxKind kind)
        {
            return (int)kind >= 400 && (int)kind < 1000;
        }

        public static bool IsKeyword(this SyntaxKind kind)
        {
            return (int)kind >= 50 && (int)kind < 200;
        }

        public static bool IsPunctuation(this SyntaxKind kind)
        {
            return (int)kind >= 200 && (int)kind < 220;
        }

        public static bool IsOperator(this SyntaxKind kind)
        {
            return (int)kind >= 220 && (int)kind < 260;
        }

        public static bool IsIdentifier(this SyntaxKind kind)
        {
            return ((int)kind >= 10 && (int)kind < 50) || kind.IsModifier();
        }

        public static bool IsRelationalOperator(this SyntaxKind kind)
        {
            return (int)kind >= 220 && (int)kind < 230;
        }

        public static bool IsAnyOf(this SyntaxKind kind, params SyntaxKind[] toTest)
        {
            for (int i = 0; i < toTest.Length; i++)
            {
                if (kind == toTest[i]) return true;
            }
            return false;
        }

        public static bool IsComment(this SyntaxKind kind)
        {
            return kind == SyntaxKind.Comment || kind == SyntaxKind.MultiLineComment;
        }
    }
}