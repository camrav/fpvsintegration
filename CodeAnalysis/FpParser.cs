﻿using CodeAnalysis.Grammar;
using CodeAnalysis.SyntaxTree.PersistentTree;
using FpLexer;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CodeAnalysis
{
    internal class FpParser
    {
        public FpParser()
        {
        }

        private List<PersistentSyntaxTrivia> CurrentTrivia = null;
        private TokenSequenceWithLookAhead sequence;
        private Token CurrentToken => sequence.PeekNextNonTrivia();
        private Token NextToken => sequence.PeekNextNonTrivia(1);

        public PersistentSyntaxNode Parse(ISourceText sourceText)
        {
            FpTokenParser lexer = new FpTokenParser();
            sequence = new TokenSequenceWithLookAhead(new TokenSequence(lexer.GetTokens(sourceText)));

            return ParseProgramRoot();
        }

        [Production(SyntaxKind.ProgramRoot,
            SyntaxKind.ProgramToken, SyntaxKind.ProgramIdentifier, SyntaxKind.SemicolonToken, SyntaxKind.UsesBlock, SyntaxKind.Block, SyntaxKind.DotToken)]
        private PersistentSyntaxNode ParseProgramRoot()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(GetKeyword(SyntaxKind.ProgramToken));
            children.Add(GetIdentifier());
            children.Add(GetPunctuation(SyntaxKind.SemicolonToken));
            children.Add(ParseUsesBlock());
            children.Add(ParseBlock());
            children.Add(GetPunctuation(SyntaxKind.DotToken));

            PersistentSyntaxTriviaList triviaAfter = EatAllTokens();

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            var root = new PersistentProgramRootNode(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
            return root.WithTriviaAfter(triviaAfter);
        }

        [Production(SyntaxKind.UsesBlock)]
        [Production(SyntaxKind.UsesBlock,
            SyntaxKind.UsesToken, SyntaxKind.UnitList, SyntaxKind.SemicolonToken)]
        private PersistentSyntaxNode ParseUsesBlock()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();
            if (CurrentToken.Kind == SyntaxKind.UsesToken)
            {
                children.Add(GetKeyword(SyntaxKind.UsesToken));
                children.Add(ParseUnitList());
                children.Add(GetPunctuation(SyntaxKind.SemicolonToken));
            }

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentUsesBlock(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.UnitList,
            SyntaxKind.UnitDeclarationLine)]
        [Production(SyntaxKind.UnitList,
            SyntaxKind.UnitDeclarationLine, SyntaxKind.CommaToken, SyntaxKind.UnitDeclarationLine)]
        private PersistentSyntaxNode ParseUnitList()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(ParseUnitDeclarationLine());
            while (CurrentToken.Kind == SyntaxKind.CommaToken)
            {
                children.Add(GetPunctuation(SyntaxKind.CommaToken));
                children.Add(ParseUnitDeclarationLine());
            }

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentUnitList(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.UnitDeclarationLine, SyntaxKind.NewIdentifier)]
        [Production(SyntaxKind.UnitDeclarationLine,
            SyntaxKind.NewIdentifier, SyntaxKind.InToken, SyntaxKind.StringConstant)]
        private PersistentSyntaxNode ParseUnitDeclarationLine()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(GetIdentifier());
            if (CurrentToken.Kind == SyntaxKind.InToken)
            {
                children.Add(GetKeyword(SyntaxKind.InToken));
                children.Add(GetConstant());
            }

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentUnitDeclaration(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.Block,
            SyntaxKind.DeclarationBlock, SyntaxKind.CompoundStatement)]
        private PersistentSyntaxNode ParseBlock()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(ParseDeclarations());
            children.Add(ParseCompoundStatement());

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentBlockSyntax(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.DeclarationBlock)]
        [Production(SyntaxKind.DeclarationBlock,
            SyntaxKind.Declaration, SyntaxKind.DeclarationBlock)]
        [Production(SyntaxKind.Declaration, SyntaxKind.VarDeclaration)]
        [Production(SyntaxKind.Declaration, SyntaxKind.LabelDeclaration)]
        [Production(SyntaxKind.Declaration, SyntaxKind.ConstantDeclaration)]
        [Production(SyntaxKind.Declaration, SyntaxKind.TypeDeclaration)]
        [Production(SyntaxKind.Declaration, SyntaxKind.ProcedureDeclaration)]
        [Production(SyntaxKind.Declaration, SyntaxKind.FunctionDeclaration)]
        private PersistentSyntaxNode ParseDeclarations()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            bool foundBegin = false;
            while (!foundBegin)
            {
                if (CurrentToken is EndOfSequence) break;

                switch (CurrentToken.Kind)
                {
                    case SyntaxKind.LabelToken:
                        children.Add(ParseLabelDeclaration());
                        break;

                    case SyntaxKind.ConstToken:
                        children.Add(ParseConstantDeclaration());
                        break;

                    case SyntaxKind.VarToken:
                        children.Add(ParseVarDeclaration());
                        break;

                    case SyntaxKind.TypeToken:
                        children.Add(ParseTypeDeclaration());
                        break;

                    case SyntaxKind.ProcedureToken:
                        children.Add(ParseProcedureDeclaration());
                        break;

                    case SyntaxKind.FunctionToken:
                        children.Add(ParseFunctionDeclaration());
                        break;

                    case SyntaxKind.BeginToken:
                        foundBegin = true;
                        break;

                    default:
                        EnsureTriviaRead();
                        CurrentTrivia.Add(new PersistentUnexpectedSyntaxTrivia(CurrentToken, SyntaxKind.VarToken, SyntaxKind.BeginToken, SyntaxKind.ConstToken,
                            SyntaxKind.TypeToken, SyntaxKind.ProcedureToken, SyntaxKind.FunctionToken, SyntaxKind.LabelToken));
                        sequence.EatToken();
                        CurrentTrivia.AddRange(GetTriviaImpl());
                        break;
                }
            }

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentDeclarationBlock(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.FunctionDeclaration,
            SyntaxKind.FunctionHeader, SyntaxKind.SemicolonToken, SyntaxKind.SubroutineBlock, SyntaxKind.SemicolonToken)]
        private PersistentSyntaxNode ParseFunctionDeclaration()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(ParseFunctionHeader());
            children.Add(GetPunctuation(SyntaxKind.SemicolonToken));
            children.Add(ParseSubroutineBlock());
            children.Add(GetPunctuation(SyntaxKind.SemicolonToken));

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentFunctionDeclaration(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.FunctionHeader,
            SyntaxKind.FunctionToken, SyntaxKind.NewIdentifier, SyntaxKind.FormalParameterList, SyntaxKind.ColonToken, SyntaxKind.Type, SyntaxKind.HintDirective)]
        private PersistentSyntaxNode ParseFunctionHeader()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(GetKeyword(SyntaxKind.FunctionToken));
            children.Add(GetIdentifier());
            children.Add(ParseFormalParameterList());
            children.Add(GetPunctuation(SyntaxKind.ColonToken));
            children.Add(ParseType());
            children.Add(ParseHintDirective());

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentFunctionHeader(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.ProcedureDeclaration,
            SyntaxKind.ProcedureHeader, SyntaxKind.SemicolonToken, SyntaxKind.SubroutineBlock, SyntaxKind.SemicolonToken)]
        private PersistentSyntaxNode ParseProcedureDeclaration()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(ParseProcedureHeader());
            children.Add(GetPunctuation(SyntaxKind.SemicolonToken));
            children.Add(ParseSubroutineBlock());
            children.Add(GetPunctuation(SyntaxKind.SemicolonToken));

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentProcedureDeclaration(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.SubroutineBlock, SyntaxKind.Block)]
        [Production(SyntaxKind.SubroutineBlock, SyntaxKind.ExternalDirective)]
        [Production(SyntaxKind.SubroutineBlock, SyntaxKind.AssemblerBlock)]
        [Production(SyntaxKind.SubroutineBlock, SyntaxKind.ForwardToken)]
        private PersistentSyntaxNode ParseSubroutineBlock()
        {
            if (CurrentToken.Kind == SyntaxKind.ForwardToken)
                return GetKeyword(SyntaxKind.ForwardToken);
            else if (CurrentToken.Kind == SyntaxKind.ExternalToken)
                return ParseExternalDirective();
            else if (CurrentToken.Kind == SyntaxKind.AssemblerToken)
                return ParseAssemblerBlock();
            else
                return ParseBlock();
        }

        [Production(SyntaxKind.AssemblerBlock,
            SyntaxKind.AssemblerToken, SyntaxKind.SemicolonToken, SyntaxKind.DeclarationBlock, SyntaxKind.AssemblerStatement)]
        private PersistentSyntaxNode ParseAssemblerBlock()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(GetKeyword(SyntaxKind.AssemblerToken));
            children.Add(GetPunctuation(SyntaxKind.SemicolonToken));
            children.Add(ParseDeclarations());
            children.Add(ParseAsm());

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentAssemblerBlock(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.ExternalDirective,
            SyntaxKind.ExternalToken)]
        [Production(SyntaxKind.ExternalDirective,
            SyntaxKind.ExternalToken, SyntaxKind.StringConstant)]
        [Production(SyntaxKind.ExternalDirective,
            SyntaxKind.ExternalToken, SyntaxKind.StringConstant, SyntaxKind.NameToken, SyntaxKind.StringConstant)]
        [Production(SyntaxKind.ExternalDirective,
            SyntaxKind.ExternalToken, SyntaxKind.StringConstant, SyntaxKind.IndexToken, SyntaxKind.StringConstant)]
        private PersistentSyntaxNode ParseExternalDirective()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(GetKeyword(SyntaxKind.ExternalToken));
            if (CurrentToken.Kind == SyntaxKind.StringConstant)
            {
                children.Add(GetConstant());
                if (CurrentToken.Kind == SyntaxKind.NameToken)
                {
                    children.Add(GetKeyword(SyntaxKind.NameToken));
                    children.Add(GetConstant());
                }
                else if (CurrentToken.Kind == SyntaxKind.IndexToken)
                {
                    children.Add(GetKeyword(SyntaxKind.NameToken));
                    children.Add(GetConstant());
                }
            }

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentExternalDirective(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.ProcedureHeader,
            SyntaxKind.ProcedureToken, SyntaxKind.NewIdentifier, SyntaxKind.FormalParameterList, SyntaxKind.HintDirective)]
        private PersistentSyntaxNode ParseProcedureHeader()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(GetKeyword(SyntaxKind.ProcedureToken));
            children.Add(GetIdentifier());
            children.Add(ParseFormalParameterList());
            children.Add(ParseHintDirective());

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentProcedureHeader(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.FormalParameterList,
            SyntaxKind.OpenParanthesisToken, SyntaxKind.ParameterDeclarations, SyntaxKind.CloseParanthesisToken)]
        private PersistentSyntaxNode ParseFormalParameterList()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(GetPunctuation(SyntaxKind.OpenParanthesisToken));
            children.Add(ParseParameterDeclarations());
            children.Add(GetPunctuation(SyntaxKind.CloseParanthesisToken));

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentFormalParameterList(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.ParameterDeclarations)]
        [Production(SyntaxKind.ParameterDeclarations, SyntaxKind.NonEmptyParameterDeclarations)]
        [Production(SyntaxKind.NonEmptyParameterDeclarations, SyntaxKind.ParameterDeclaration)]
        [Production(SyntaxKind.NonEmptyParameterDeclarations,
            SyntaxKind.ParameterDeclaration, SyntaxKind.SemicolonToken, SyntaxKind.NonEmptyParameterDeclarations)]
        private PersistentSyntaxNode ParseParameterDeclarations()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            if (CurrentToken.Kind != SyntaxKind.CloseParanthesisToken)
            {
                children.Add(ParseParameterDeclaration());
                while (CurrentToken.Kind == SyntaxKind.SemicolonToken)
                {
                    children.Add(GetPunctuation(SyntaxKind.SemicolonToken));
                    children.Add(ParseParameterDeclaration());
                }
            }

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentParameterDeclarations(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.ParameterDeclaration, SyntaxKind.ValueParameter)]
        [Production(SyntaxKind.ParameterDeclaration, SyntaxKind.VariableParameter)]
        [Production(SyntaxKind.ParameterDeclaration, SyntaxKind.OutParameter)]
        [Production(SyntaxKind.ParameterDeclaration, SyntaxKind.ConstantParameter)]
        private PersistentSyntaxNode ParseParameterDeclaration()
        {
            if (CurrentToken.Kind == SyntaxKind.VarToken)
                return ParseVariableParameter();
            else if (CurrentToken.Kind == SyntaxKind.OutToken)
                return ParseOutParameter();
            else if (CurrentToken.Kind == SyntaxKind.ConstToken)
                return ParseConstParameter();
            else
                return ParseValueParameter();
        }

        [Production(SyntaxKind.ConstantParameter,
            SyntaxKind.ConstToken, SyntaxKind.VariableIdentifierList, SyntaxKind.ColonToken, SyntaxKind.Type)]
        private PersistentSyntaxNode ParseConstParameter()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(GetKeyword(SyntaxKind.ConstToken));
            children.Add(ParseVariableIdentifierList());
            children.Add(GetPunctuation(SyntaxKind.ColonToken));
            children.Add(ParseType());

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentConstantParameter(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.OutParameter,
            SyntaxKind.OutToken, SyntaxKind.VariableIdentifierList, SyntaxKind.ColonToken, SyntaxKind.Type)]
        private PersistentSyntaxNode ParseOutParameter()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(GetKeyword(SyntaxKind.OutToken));
            children.Add(ParseVariableIdentifierList());
            children.Add(GetPunctuation(SyntaxKind.ColonToken));
            children.Add(ParseType());

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentOutParameter(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.VariableParameter,
            SyntaxKind.VarToken, SyntaxKind.VariableIdentifierList, SyntaxKind.ColonToken, SyntaxKind.Type)]
        private PersistentSyntaxNode ParseVariableParameter()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(GetKeyword(SyntaxKind.VarToken));
            children.Add(ParseVariableIdentifierList());
            children.Add(GetPunctuation(SyntaxKind.ColonToken));
            children.Add(ParseType());

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentVariableParameter(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.ValueParameter,
            SyntaxKind.VariableIdentifierList, SyntaxKind.ColonToken, SyntaxKind.Type)] // we do not support auto value properties yet
        private PersistentSyntaxNode ParseValueParameter()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(ParseVariableIdentifierList());
            children.Add(GetPunctuation(SyntaxKind.ColonToken));
            children.Add(ParseType());

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentValueParameter(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.TypeDeclaration,
            SyntaxKind.TypeToken, SyntaxKind.TypeDeclarationLines)]
        private PersistentSyntaxNode ParseTypeDeclaration()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(GetKeyword(SyntaxKind.TypeToken));
            children.Add(ParseTypeDeclarationLines());

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentTypeDeclaration(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.TypeDeclarationLines, SyntaxKind.TypeDeclarationLine)]
        [Production(SyntaxKind.TypeDeclarationLines,
            SyntaxKind.TypeDeclarationLine, SyntaxKind.TypeDeclarationLines)]
        private PersistentSyntaxNode ParseTypeDeclarationLines()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(ParseTypeDeclarationLine());
            while (CurrentToken.Kind.IsIdentifier())
                children.Add(ParseTypeDeclarationLine());

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentTypeDeclarationLines(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.TypeDeclarationLine,
            SyntaxKind.NewIdentifier, SyntaxKind.EqualsToken, SyntaxKind.Type, SyntaxKind.HintDirective, SyntaxKind.SemicolonToken)]
        private PersistentSyntaxNode ParseTypeDeclarationLine()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(GetIdentifier(SyntaxKind.TypeIdentifier));
            children.Add(GetPunctuation(SyntaxKind.EqualsToken));
            children.Add(ParseType());
            children.Add(ParseHintDirective());
            children.Add(GetPunctuation(SyntaxKind.SemicolonToken));

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentTypeDeclarationLine(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.ConstantDeclaration,
            SyntaxKind.ConstToken, SyntaxKind.ConstDeclarationLines)]
        private PersistentSyntaxNode ParseConstantDeclaration()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(GetKeyword(SyntaxKind.ConstToken));
            children.Add(ParseConstDeclarationLines());

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentConstDeclaration(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.ConstDeclarationLines,
            SyntaxKind.ConstDeclarationLine)]
        [Production(SyntaxKind.ConstDeclarationLine,
            SyntaxKind.ConstDeclarationLine, SyntaxKind.ConstDeclarationLines)]
        private PersistentSyntaxNode ParseConstDeclarationLines()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(ParseConstDeclarationLine());
            while (CurrentToken.Kind.IsIdentifier())
                children.Add(ParseConstDeclarationLine());

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentConstDeclarationLines(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.ConstDeclarationLine, SyntaxKind.ConstUntypedDeclarationLine)]
        //[Production()]
        private PersistentSyntaxNode ParseConstDeclarationLine()
        {
            return ParseConstUntypedDeclarationLine();
        }

        [Production(SyntaxKind.ConstUntypedDeclarationLine,
            SyntaxKind.NewIdentifier, SyntaxKind.EqualsToken, SyntaxKind.Expression, SyntaxKind.HintDirective, SyntaxKind.SemicolonToken)]
        private PersistentSyntaxNode ParseConstUntypedDeclarationLine()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(GetIdentifier(SyntaxKind.ConstantIdentifier));
            children.Add(GetPunctuation(SyntaxKind.EqualsToken));
            children.Add(ParseExpression());
            children.Add(ParseHintDirective());
            children.Add(GetPunctuation(SyntaxKind.SemicolonToken));

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentConstUntypedDeclarationLine(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.HintDirective)]
        [Production(SyntaxKind.HintDirective, SyntaxKind.DeprecatedToken)]
        [Production(SyntaxKind.HintDirective, SyntaxKind.DeprecatedToken, SyntaxKind.StringConstant)]
        [Production(SyntaxKind.HintDirective, SyntaxKind.ExperimentalToken)]
        [Production(SyntaxKind.HintDirective, SyntaxKind.PlatformToken)]
        [Production(SyntaxKind.HintDirective, SyntaxKind.UnimplementedToken)]
        private PersistentSyntaxNode ParseHintDirective()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            // only string constants are allowed here, not even named string constants (i.e. with identifiers)
            if (CurrentToken.Kind == SyntaxKind.DeprecatedToken && NextToken.Kind == SyntaxKind.StringConstant)
            {
                children.Add(GetKeyword(SyntaxKind.DeprecatedToken));
                children.Add(GetConstant());
            }
            else if (CurrentToken.Kind.IsAnyOf(SyntaxKind.DeprecatedToken, SyntaxKind.ExperimentalToken, SyntaxKind.PlatformToken, SyntaxKind.UnimplementedToken))
            {
                children.Add(GetKeyword(SyntaxKind.DeprecatedToken, SyntaxKind.ExperimentalToken, SyntaxKind.PlatformToken, SyntaxKind.UnimplementedToken));
            }

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentHintDirective(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.LabelDeclaration,
            SyntaxKind.LabelToken, SyntaxKind.LabelList, SyntaxKind.SemicolonToken)]
        private PersistentSyntaxNode ParseLabelDeclaration()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(GetKeyword(SyntaxKind.LabelToken));
            children.Add(ParseLabelList());
            children.Add(GetPunctuation(SyntaxKind.SemicolonToken));

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentLabelDeclaration(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.LabelList,
            SyntaxKind.NewLabel)]
        [Production(SyntaxKind.LabelList,
            SyntaxKind.NewLabel, SyntaxKind.CommaToken, SyntaxKind.LabelList)]
        [Production(SyntaxKind.NewLabel, SyntaxKind.UintConstant)]
        [Production(SyntaxKind.NewLabel, SyntaxKind.NewIdentifier)]
        private PersistentSyntaxNode ParseLabelList()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(ParseLabel());
            while (CurrentToken.Kind == SyntaxKind.CommaToken)
            {
                children.Add(GetPunctuation(SyntaxKind.CommaToken));
                children.Add(ParseLabel());
            }

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentLabelList(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.CompoundStatement,
            SyntaxKind.BeginToken, SyntaxKind.StatementSequence, SyntaxKind.EndToken)]
        private PersistentSyntaxNode ParseCompoundStatement()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(GetKeyword(SyntaxKind.BeginToken));
            children.Add(ParseStatementSequence(SyntaxKind.EndToken));
            children.Add(GetKeyword(SyntaxKind.EndToken));

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentCompoundStatement(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.VarDeclaration,
            SyntaxKind.VarToken, SyntaxKind.VarDeclarationLines)]
        private PersistentSyntaxNode ParseVarDeclaration()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(GetKeyword(SyntaxKind.VarToken));
            children.Add(ParseVarDeclarationLines());

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentVarSyntax(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.VarDeclarationLines,
           SyntaxKind.VarDeclarationLine)]
        [Production(SyntaxKind.VarDeclarationLines,
            SyntaxKind.VarDeclarationLine, SyntaxKind.VarDeclarationLines)]
        private PersistentSyntaxNode ParseVarDeclarationLines()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(ParseVarDeclarationLine());
            while (CurrentToken.Kind.IsIdentifier())
            {
                children.Add(ParseVarDeclarationLine());
            }

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentVarDeclarationLinesSyntax(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.VarDeclarationLine,
            SyntaxKind.VariableIdentifierList, SyntaxKind.ColonToken, SyntaxKind.Type, SyntaxKind.VariableInitialization,
            SyntaxKind.VariableModifiers, SyntaxKind.HintDirective, SyntaxKind.SemicolonToken)]
        private PersistentSyntaxNode ParseVarDeclarationLine()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(ParseVariableIdentifierList());
            children.Add(GetPunctuation(SyntaxKind.ColonToken));
            children.Add(ParseType());
            children.Add(ParseVariableInitialization());
            children.Add(ParseVariableModifiers());
            children.Add(ParseHintDirective());
            children.Add(GetPunctuation(SyntaxKind.SemicolonToken));

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentVarDeclarationLineSyntax(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.VariableModifiers)]
        [Production(SyntaxKind.VariableModifiers,
            SyntaxKind.VariableModifier, SyntaxKind.VariableModifiers)]
        private PersistentSyntaxNode ParseVariableModifiers()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            while (CurrentToken.Kind == SyntaxKind.AbsoluteToken
                || (CurrentToken.Kind == SyntaxKind.SemicolonToken && NextToken.Kind.IsAnyOf(SyntaxKind.ExportToken, SyntaxKind.CvarToken, SyntaxKind.ExternalToken)))
            {
                children.Add(ParseVariableModifier());
            }

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentVariableModifiers(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.VariableModifier, SyntaxKind.AbsoluteModifier)]
        [Production(SyntaxKind.VariableModifier, SyntaxKind.ExportModifier)]
        [Production(SyntaxKind.VariableModifier, SyntaxKind.CvarModifier)]
        [Production(SyntaxKind.VariableModifier, SyntaxKind.ExternalModifier)]
        private PersistentSyntaxNode ParseVariableModifier()
        {
            if (CurrentToken.Kind == SyntaxKind.AbsoluteToken)
                return ParseAbsoluteModifier();
            if (CurrentToken.Kind == SyntaxKind.SemicolonToken)
            {
                if (NextToken.Kind == SyntaxKind.ExportToken)
                    return ParseExportModifier();
                else if (NextToken.Kind == SyntaxKind.CvarToken)
                    return ParseCvarModifier();
                else if (NextToken.Kind == SyntaxKind.ExternalToken)
                    return ParseExternalModifier();
            }

            return new PersistentMissingTerminal(SyntaxKind.AbsoluteToken, SyntaxKind.SemicolonToken);
        }

        [Production(SyntaxKind.ExternalModifier,
            SyntaxKind.SemicolonToken, SyntaxKind.ExternalToken)]
        [Production(SyntaxKind.ExternalModifier,
            SyntaxKind.SemicolonToken, SyntaxKind.ExternalToken, SyntaxKind.StringConstant)]
        [Production(SyntaxKind.ExternalModifier,
            SyntaxKind.SemicolonToken, SyntaxKind.ExternalToken, SyntaxKind.StringConstant, SyntaxKind.NameToken, SyntaxKind.StringConstant)]
        [Production(SyntaxKind.ExternalModifier,
            SyntaxKind.SemicolonToken, SyntaxKind.ExternalToken, SyntaxKind.NameToken, SyntaxKind.StringConstant)]
        private PersistentSyntaxNode ParseExternalModifier()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(GetPunctuation(SyntaxKind.SemicolonToken));
            children.Add(GetKeyword(SyntaxKind.ExternalToken));
            if (CurrentToken.Kind == SyntaxKind.StringConstant)
                children.Add(GetConstant());
            if (CurrentToken.Kind == SyntaxKind.NameToken)
            {
                children.Add(GetKeyword(SyntaxKind.NameToken));
                children.Add(GetKeyword(SyntaxKind.StringConstant));
            }

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentExternalModifier(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.CvarModifier,
            SyntaxKind.SemicolonToken, SyntaxKind.CvarToken)]
        private PersistentSyntaxNode ParseCvarModifier()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(GetPunctuation(SyntaxKind.SemicolonToken));
            children.Add(GetKeyword(SyntaxKind.CvarToken));

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentCvarModifier(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.ExportModifier,
            SyntaxKind.SemicolonToken, SyntaxKind.ExportToken)]
        private PersistentSyntaxNode ParseExportModifier()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(GetPunctuation(SyntaxKind.SemicolonToken));
            children.Add(GetKeyword(SyntaxKind.ExportToken));

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentExportModifier(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.AbsoluteModifier,
            SyntaxKind.AbsoluteToken, SyntaxKind.Expression)]
        private PersistentSyntaxNode ParseAbsoluteModifier()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(GetKeyword(SyntaxKind.AbsoluteToken));
            children.Add(ParseExpression()); //this also covers identifier

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentAbsoluteModifier(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.VariableInitialization)]
        [Production(SyntaxKind.VariableInitialization,
            SyntaxKind.EqualsToken, SyntaxKind.Expression)]
        private PersistentSyntaxNode ParseVariableInitialization()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            if (CurrentToken.Kind == SyntaxKind.EqualsToken)
            {
                children.Add(GetPunctuation(SyntaxKind.EqualsToken));
                children.Add(GetIdentifier());
            }

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentVariableInitialization(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.VariableIdentifierList,
            SyntaxKind.NewIdentifier)]
        [Production(SyntaxKind.VariableIdentifierList,
            SyntaxKind.NewIdentifier, SyntaxKind.CommaToken, SyntaxKind.VariableIdentifierList)]
        private PersistentSyntaxNode ParseVariableIdentifierList()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(GetIdentifier());
            while (CurrentToken.Kind == SyntaxKind.CommaToken)
            {
                children.Add(GetPunctuation(SyntaxKind.CommaToken));
                children.Add(GetIdentifier());
            }

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentVariableIdentifierList(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.Type, SyntaxKind.TypeIdentifier)]
        [Production(SyntaxKind.Type, SyntaxKind.ArrayType)]
        [Production(SyntaxKind.Type, SyntaxKind.RecordType)]
        [Production(SyntaxKind.Type, SyntaxKind.PointerType)]
        [Production(SyntaxKind.Type, SyntaxKind.EnumType)]
        [Production(SyntaxKind.Type, SyntaxKind.FileType)]
        [Production(SyntaxKind.Type, SyntaxKind.SetType)]
        [Production(SyntaxKind.Type, SyntaxKind.StringWithFixedSizeType)]
        [Production(SyntaxKind.Type, SyntaxKind.StringWithCodePageType)]
        private PersistentSyntaxNode ParseType()
        {
            if (CurrentToken.Kind.IsIdentifier())
                return GetIdentifier(SyntaxKind.TypeIdentifier);
            else if (CurrentToken.Kind == SyntaxKind.ArrayToken)
                return ParseArrayType();
            else if (CurrentToken.Kind == SyntaxKind.RecordToken)
                return ParseRecordType();
            else if (CurrentToken.Kind == SyntaxKind.RoofToken)
                return ParsePointerType();
            else if (CurrentToken.Kind == SyntaxKind.OpenParanthesisToken)
                return ParseEnumType();
            else if (CurrentToken.Kind == SyntaxKind.FileToken)
                return ParseFileType();
            else if (CurrentToken.Kind == SyntaxKind.SetToken)
                return ParseSetType();
            else if (CurrentToken.Kind == SyntaxKind.StringToken)
                return ParseStringWithFixedSizeType();
            else if (CurrentToken.Kind == SyntaxKind.TypeToken)
                return ParseStringWithCodePageType();
            else return new PersistentMissingTerminal(SyntaxKind.TypeIdentifier, SyntaxKind.ArrayToken, SyntaxKind.RecordToken, SyntaxKind.RoofToken, SyntaxKind.OpenParanthesisToken);
        }

        [Production(SyntaxKind.StringWithCodePageType,
            SyntaxKind.TypeToken, SyntaxKind.StringToken, SyntaxKind.OpenParanthesisToken, SyntaxKind.Expression, SyntaxKind.CloseParanthesisToken)]
        [Production(SyntaxKind.StringWithCodePageType,
            SyntaxKind.TypeToken, SyntaxKind.AnsistringToken, SyntaxKind.OpenParanthesisToken, SyntaxKind.Expression, SyntaxKind.CloseParanthesisToken)]
        private PersistentSyntaxNode ParseStringWithCodePageType()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(GetKeyword(SyntaxKind.TypeToken));
            children.Add(GetKeyword(SyntaxKind.StringToken, SyntaxKind.AnsistringToken));
            children.Add(GetPunctuation(SyntaxKind.OpenParanthesisToken));
            children.Add(ParseExpression());
            children.Add(GetPunctuation(SyntaxKind.CloseParanthesisToken));

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentStringWithCodePageType(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.StringWithFixedSizeType, SyntaxKind.StringToken)]
        [Production(SyntaxKind.StringWithFixedSizeType,
            SyntaxKind.OpenBracketToken, SyntaxKind.Expression, SyntaxKind.CloseBracketToken)]
        private PersistentSyntaxNode ParseStringWithFixedSizeType()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(GetKeyword(SyntaxKind.StringToken));
            if (CurrentToken.Kind == SyntaxKind.OpenBracketToken)
            {
                children.Add(GetPunctuation(SyntaxKind.OpenBracketToken));
                children.Add(ParseExpression());
                children.Add(GetPunctuation(SyntaxKind.CloseBracketToken));
            }

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentStringWithFixedSizeType(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.SetType,
            SyntaxKind.SetToken, SyntaxKind.OfToken, SyntaxKind.Type)]
        private PersistentSyntaxNode ParseSetType()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(GetKeyword(SyntaxKind.SetToken));
            children.Add(GetKeyword(SyntaxKind.OfToken));
            children.Add(ParseType());

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentSetType(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.FileType, SyntaxKind.FileToken)]
        [Production(SyntaxKind.FileType,
            SyntaxKind.FileToken, SyntaxKind.OfToken, SyntaxKind.Type)]
        private PersistentSyntaxNode ParseFileType()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(GetKeyword(SyntaxKind.FileToken));
            if (CurrentToken.Kind == SyntaxKind.OfToken)
            {
                children.Add(GetKeyword(SyntaxKind.OfToken));
                children.Add(ParseType());
            }

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentFileType(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.EnumType,
            SyntaxKind.OpenParanthesisToken, SyntaxKind.EnumMemberList, SyntaxKind.CloseParanthesisToken)]
        private PersistentSyntaxNode ParseEnumType()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(GetPunctuation(SyntaxKind.OpenParanthesisToken));
            children.Add(ParseEnumMembers());
            children.Add(GetPunctuation(SyntaxKind.CloseParanthesisToken));

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentEnumType(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.EnumMemberList, SyntaxKind.EnumMember)]
        [Production(SyntaxKind.EnumMemberList,
            SyntaxKind.EnumMember, SyntaxKind.CommaToken, SyntaxKind.EnumMemberList)]
        private PersistentSyntaxNode ParseEnumMembers()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(ParseEnumMember());
            while (CurrentToken.Kind == SyntaxKind.CommaToken)
            {
                children.Add(GetPunctuation(SyntaxKind.CommaToken));
                children.Add(ParseEnumMember());
            }

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentEnumMembers(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.EnumMember, SyntaxKind.EnumMemberIdentifier)]
        [Production(SyntaxKind.EnumMember, SyntaxKind.InitializedEnumMember)]
        private PersistentSyntaxNode ParseEnumMember()
        {
            if (NextToken.Kind == SyntaxKind.AssignToken)
                return ParseInitializedEnumMember();
            else
                return GetIdentifier();
        }

        [Production(SyntaxKind.InitializedEnumMember,
            SyntaxKind.EnumMemberIdentifier, SyntaxKind.AssignToken, SyntaxKind.Expression)]
        private PersistentSyntaxNode ParseInitializedEnumMember()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(GetIdentifier());
            children.Add(GetPunctuation(SyntaxKind.AssignToken));
            children.Add(ParseExpression());

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentInitializedEnumMember(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.PointerType,
            SyntaxKind.RoofToken, SyntaxKind.TypeIdentifier)]
        private PersistentSyntaxNode ParsePointerType()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(GetPunctuation(SyntaxKind.RoofToken));
            children.Add(GetIdentifier(SyntaxKind.TypeIdentifier));

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentPointerType(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.RecordType,
            SyntaxKind.RecordToken, SyntaxKind.FieldList, SyntaxKind.EndToken)]
        [Production(SyntaxKind.RecordType,
            SyntaxKind.RecordToken, SyntaxKind.FieldList, SyntaxKind.SemicolonToken, SyntaxKind.EndToken)]
        private PersistentSyntaxNode ParseRecordType()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(GetKeyword(SyntaxKind.RecordToken));
            children.Add(ParseFieldList());
            if (CurrentToken.Kind == SyntaxKind.SemicolonToken)
                children.Add(GetPunctuation(SyntaxKind.SemicolonToken));
            children.Add(GetKeyword(SyntaxKind.EndToken));

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentRecordType(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.FieldList, SyntaxKind.FieldLine)]
        [Production(SyntaxKind.FieldList, SyntaxKind.FieldLine, SyntaxKind.SemicolonToken, SyntaxKind.FieldList)]
        private PersistentSyntaxNode ParseFieldList()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(ParseFieldLine());
            while (CurrentToken.Kind == SyntaxKind.SemicolonToken && NextToken.Kind != SyntaxKind.EndToken)
            {
                children.Add(GetPunctuation(SyntaxKind.SemicolonToken));
                children.Add(ParseFieldLine());
            }

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentFieldList(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.FieldLine,
            SyntaxKind.VariableIdentifierList, SyntaxKind.ColonToken, SyntaxKind.Type)]
        private PersistentSyntaxNode ParseFieldLine()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(ParseVariableIdentifierList());
            children.Add(GetPunctuation(SyntaxKind.ColonToken));
            children.Add(ParseType());

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentFieldLine(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.ArrayType,
            SyntaxKind.ArrayToken, SyntaxKind.OpenBracketToken, SyntaxKind.SubrangeList, SyntaxKind.CloseBracketToken, SyntaxKind.OfToken, SyntaxKind.Type)]
        [Production(SyntaxKind.ArrayType,
            SyntaxKind.ArrayToken, SyntaxKind.OfToken, SyntaxKind.Type)]
        private PersistentSyntaxNode ParseArrayType()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(GetKeyword(SyntaxKind.ArrayToken));
            if (CurrentToken.Kind == SyntaxKind.OpenBracketToken)
            {
                children.Add(GetPunctuation(SyntaxKind.OpenBracketToken));
                children.Add(ParseSubrangeList());
                children.Add(GetPunctuation(SyntaxKind.CloseBracketToken));
            }
            children.Add(GetKeyword(SyntaxKind.OfToken));
            children.Add(ParseType());

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentArrayType(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.SubrangeList, SyntaxKind.Subrange)]
        [Production(SyntaxKind.SubrangeList,
            SyntaxKind.Subrange, SyntaxKind.CommaToken, SyntaxKind.SubrangeList)]
        private PersistentSyntaxNode ParseSubrangeList()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(ParseSubrange());
            while (CurrentToken.Kind == SyntaxKind.CommaToken)
            {
                children.Add(GetPunctuation(SyntaxKind.CommaToken));
                children.Add(ParseSubrange());
            }

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentSubrangeList(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.Subrange,
            SyntaxKind.Constant, SyntaxKind.DotDotToken, SyntaxKind.Constant)]
        private PersistentSyntaxNode ParseSubrange()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(GetConstant());
            children.Add(GetPunctuation(SyntaxKind.DotDotToken));
            children.Add(GetConstant());

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentSubrange(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.StatementSequence,
            SyntaxKind.Statement)]
        [Production(SyntaxKind.StatementSequence,
            SyntaxKind.Statement, SyntaxKind.SemicolonToken, SyntaxKind.StatementSequence)]
        private PersistentSyntaxNode ParseStatementSequence(params SyntaxKind[] possibleEndingTokensOfSequence)
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();
            possibleEndingTokensOfSequence = possibleEndingTokensOfSequence ?? new SyntaxKind[0];

            while (true)
            {
                if (CurrentToken is EndOfSequence || possibleEndingTokensOfSequence.Contains(CurrentToken.Kind))
                    break;

                if (children.Count > 0 && !(children[children.Count - 1] is PersistentUnexpectedSyntaxTrivia))
                    children.Add(GetPunctuation(SyntaxKind.SemicolonToken));

                children.Add(ParseStatement(possibleEndingTokensOfSequence));
            }

            if (children.Count == 0)
                children.Add(ParseStatement(possibleEndingTokensOfSequence));

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentStatementSequenceSyntax(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        #region Statement

        [Production(SyntaxKind.Statement)]
        [Production(SyntaxKind.Statement,
            SyntaxKind.AssignStatement)]
        [Production(SyntaxKind.Statement,
            SyntaxKind.CallStatement)]
        [Production(SyntaxKind.Statement,
            SyntaxKind.CompoundStatement)]
        [Production(SyntaxKind.Statement,
            SyntaxKind.IfStatement)]
        [Production(SyntaxKind.Statement,
            SyntaxKind.ForLoopStatement)]
        [Production(SyntaxKind.Statement,
            SyntaxKind.WhileLoopStatement)]
        [Production(SyntaxKind.Statement,
            SyntaxKind.RepeatLoopStatement)]
        [Production(SyntaxKind.Statement,
            SyntaxKind.WithStatement)]
        [Production(SyntaxKind.Statement,
            SyntaxKind.GotoStatement)]
        [Production(SyntaxKind.Statement,
            SyntaxKind.CaseStatement)]
        private PersistentSyntaxNode ParseStatement(params SyntaxKind[] PossibleFollowingTokens) //to be prepended with empty statements
        {
            PossibleFollowingTokens = PossibleFollowingTokens ?? new SyntaxKind[0];
            while (true)
            {
                if (CurrentToken is EndOfSequence)
                {
                    return new PersistentNoStatementSyntax(PersistentSyntaxTriviaList.Empty, PersistentSyntaxTriviaList.Empty);
                }

                if (CurrentToken.Kind.IsIdentifier())
                {
                    SyntaxKind next = NextToken.Kind;
                    if (next.IsAnyOf(SyntaxKind.AssignToken, SyntaxKind.RoofToken, SyntaxKind.DotToken, SyntaxKind.OpenBracketToken))
                        return ParseAssignment();
                    else if (next.IsAnyOf(SyntaxKind.SemicolonToken, SyntaxKind.EndToken, SyntaxKind.OpenParanthesisToken))
                        return ParseCall();
                    else
                    {
                        EnsureTriviaRead();
                        CurrentTrivia.Add(new PersistentUnexpectedSyntaxTrivia(CurrentToken,
                            SyntaxKind.ProcedureIdentifier, SyntaxKind.FunctionIdentifier, SyntaxKind.VariableIdentifier, SyntaxKind.IfToken, SyntaxKind.ForToken,
                            SyntaxKind.BeginToken, SyntaxKind.WhileToken, SyntaxKind.CaseToken, SyntaxKind.WithToken, SyntaxKind.GotoToken,
                            SyntaxKind.RepeatToken));
                        sequence.EatToken();
                        CurrentTrivia.AddRange(GetTriviaImpl());
                    }
                    continue;
                }

                if (PossibleFollowingTokens.Contains(CurrentToken.Kind))
                    return new PersistentNoStatementSyntax(PersistentSyntaxTriviaList.Empty, PersistentSyntaxTriviaList.Empty);

                switch (CurrentToken.Kind)
                {
                    case SyntaxKind.BeginToken:
                        return ParseCompoundStatement();

                    case SyntaxKind.SemicolonToken:
                        return new PersistentNoStatementSyntax(PersistentSyntaxTriviaList.Empty, PersistentSyntaxTriviaList.Empty);

                    case SyntaxKind.IfToken:
                        return ParseIf();

                    case SyntaxKind.ForToken:
                        return ParseForLoop();

                    case SyntaxKind.RepeatToken:
                        return ParseRepeatLoop();

                    case SyntaxKind.WhileToken:
                        return ParseWhileLoop();

                    case SyntaxKind.AsmToken:
                        return ParseAsm();

                    case SyntaxKind.WithToken:
                        return ParseWithStatement();

                    case SyntaxKind.GotoToken:
                        return ParseGotoStatement();

                    case SyntaxKind.CaseToken:
                        return ParseCaseStatement();

                    default:
                        EnsureTriviaRead();
                        CurrentTrivia.Add(new PersistentUnexpectedSyntaxTrivia(CurrentToken, SyntaxKind.IdentifierToken, SyntaxKind.EndToken, SyntaxKind.SemicolonToken));
                        sequence.EatToken();
                        CurrentTrivia.AddRange(GetTriviaImpl());
                        break;
                }
            }
        }

        [Production(SyntaxKind.CaseStatement,
            SyntaxKind.CaseToken, SyntaxKind.Expression, SyntaxKind.OfToken, SyntaxKind.CaseList, SyntaxKind.CaseElse, SyntaxKind.EndToken)]
        [Production(SyntaxKind.CaseStatement,
           SyntaxKind.CaseToken, SyntaxKind.Expression, SyntaxKind.OfToken, SyntaxKind.CaseList, SyntaxKind.CaseElse, SyntaxKind.SemicolonToken, SyntaxKind.EndToken)]
        [Production(SyntaxKind.CaseElse)]
        [Production(SyntaxKind.CaseElse,
            SyntaxKind.SemicolonToken, SyntaxKind.ElseToken, SyntaxKind.StatementSequence)]
        [Production(SyntaxKind.CaseElse,
            SyntaxKind.SemicolonToken, SyntaxKind.OtherwiseToken, SyntaxKind.StatementSequence)]
        private PersistentSyntaxNode ParseCaseStatement()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(GetKeyword(SyntaxKind.CaseToken));
            children.Add(ParseExpression());
            children.Add(GetKeyword(SyntaxKind.OfToken));
            children.Add(ParseCaseList());
            if (NextToken.Kind == SyntaxKind.ElseToken || NextToken.Kind == SyntaxKind.OtherwiseToken)
            {
                children.Add(GetPunctuation(SyntaxKind.SemicolonToken));
                children.Add(GetKeyword(SyntaxKind.ElseToken, SyntaxKind.OtherwiseToken));
                children.Add(ParseStatementSequence(SyntaxKind.EndToken));
            }
            if (CurrentToken.Kind == SyntaxKind.SemicolonToken)
                children.Add(GetPunctuation(SyntaxKind.SemicolonToken));
            children.Add(GetKeyword(SyntaxKind.EndToken));

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentCaseStatement(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.CaseList,
            SyntaxKind.Case)]
        [Production(SyntaxKind.CaseList,
            SyntaxKind.Case, SyntaxKind.SemicolonToken, SyntaxKind.CaseList)]
        private PersistentSyntaxNode ParseCaseList()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            do
            {
                children.Add(ParseCase());
                if (CurrentToken is EndOfSequence) break;
                if (CurrentToken.Kind == SyntaxKind.EndToken)
                    break;
                if (NextToken.Kind == SyntaxKind.EndToken ||
                    NextToken.Kind == SyntaxKind.ElseToken ||
                    NextToken.Kind == SyntaxKind.OtherwiseToken)
                    break;

                children.Add(GetPunctuation(SyntaxKind.SemicolonToken));
            } while (true);

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentCaseList(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.Case,
            SyntaxKind.OrdinalConstantList, SyntaxKind.ColonToken, SyntaxKind.Statement)]
        private PersistentSyntaxNode ParseCase()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(ParseOrdinalConstantList());
            children.Add(GetPunctuation(SyntaxKind.ColonToken));
            children.Add(ParseStatement(SyntaxKind.EndToken, SyntaxKind.ElseToken, SyntaxKind.OtherwiseToken));

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentCaseSyntax(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.OrdinalConstantList,
            SyntaxKind.OrdinalConstant)]
        [Production(SyntaxKind.OrdinalConstantList,
            SyntaxKind.OrdinalConstant, SyntaxKind.CommaToken, SyntaxKind.OrdinalConstantList)]
        private PersistentSyntaxNode ParseOrdinalConstantList()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            do
            {
                if (children.Count > 0)
                    children.Add(GetPunctuation(SyntaxKind.CommaToken));

                children.Add(ParseOrdinalConstant());
            } while (!(NextToken is EndOfSequence)
                && CurrentToken.Kind != SyntaxKind.ColonToken
                && CurrentToken.Kind != SyntaxKind.EndToken
                && CurrentToken.Kind != SyntaxKind.SemicolonToken);

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentOrdinalConstantList(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.OrdinalConstant,
            SyntaxKind.OrdinalConstantNoRange)]
        [Production(SyntaxKind.OrdinalConstant,
            SyntaxKind.OrdinalConstantNoRange, SyntaxKind.DotDotToken, SyntaxKind.OrdinalConstantNoRange)]
        [Production(SyntaxKind.OrdinalConstantNoRange,
            SyntaxKind.StringConstant)]
        [Production(SyntaxKind.OrdinalConstantNoRange,
            SyntaxKind.UintConstant)]
        [Production(SyntaxKind.OrdinalConstantNoRange,
            SyntaxKind.ConstantIdentifier)]
        [Production(SyntaxKind.OrdinalConstantNoRange,
            SyntaxKind.EnumIdentifier)]
        private PersistentSyntaxNode ParseOrdinalConstant()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(GetConstant());
            if (CurrentToken.Kind == SyntaxKind.DotDotToken)
            {
                children.Add(GetPunctuation(SyntaxKind.DotDotToken));
                children.Add(GetConstant());
            }

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentOrdinalConstant(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.GotoStatement,
            SyntaxKind.GotoToken, SyntaxKind.Label)]
        private PersistentSyntaxNode ParseGotoStatement()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(GetKeyword(SyntaxKind.GotoToken));
            children.Add(ParseLabel());

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentGotoStatement(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.Label,
            SyntaxKind.UintConstant)]
        [Production(SyntaxKind.Label,
            SyntaxKind.LabelIdentifier)]
        private PersistentSyntaxNode ParseLabel()
        {
            Token next = sequence.PeekNextNonTrivia();

            if (next.Kind == SyntaxKind.UintConstant)
                return GetConstant();
            else
                return GetIdentifier(); //will return MissingTerminal in case there is no identifier
        }

        [Production(SyntaxKind.WithStatement,
            SyntaxKind.WithToken, SyntaxKind.VariableList, SyntaxKind.DoToken, SyntaxKind.Statement)]
        private PersistentSyntaxNode ParseWithStatement()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(GetKeyword(SyntaxKind.WithToken));
            children.Add(ParseVariableList());
            children.Add(GetKeyword(SyntaxKind.DoToken));
            children.Add(ParseStatement());

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentWithStatement(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.VariableList,
            SyntaxKind.Variable)]
        [Production(SyntaxKind.VariableList,
            SyntaxKind.Variable, SyntaxKind.CommaToken, SyntaxKind.VariableList)]
        private PersistentSyntaxNode ParseVariableList()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            do
            {
                if (children.Count > 0)
                    children.Add(GetPunctuation(SyntaxKind.CommaToken));

                children.Add(ParseVariable());
            } while (CurrentToken.Kind == SyntaxKind.CommaToken);

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentVariableListSyntax(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.AssemblerStatement,
            SyntaxKind.AsmToken, SyntaxKind.EndToken)]
        private PersistentSyntaxNode ParseAsm()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(GetKeyword(SyntaxKind.AsmToken));

            CurrentTrivia = new List<PersistentSyntaxTrivia>();
            while (true)
            {
                foreach (var trivia in sequence.GetAllTrivia())
                {
                    CurrentTrivia.Add(new PersistentAssemblerTrivia(trivia));
                }

                Token next = sequence.PeekNextNonTrivia();
                if (next.Kind == SyntaxKind.EndToken || next is EndOfSequence)
                    break;

                CurrentTrivia.Add(new PersistentAssemblerTrivia(next));
                sequence.EatToken();
            }
            children.Add(GetKeyword(SyntaxKind.EndToken));

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentAssemblerStatement(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.RepeatLoopStatement,
            SyntaxKind.RepeatToken, SyntaxKind.StatementSequence, SyntaxKind.UntilToken, SyntaxKind.Expression)]
        private PersistentSyntaxNode ParseRepeatLoop()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(GetKeyword(SyntaxKind.RepeatToken));
            children.Add(ParseStatementSequence(SyntaxKind.UntilToken));
            children.Add(GetKeyword(SyntaxKind.UntilToken));
            children.Add(ParseExpression());

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentRepeatLoopStatement(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.WhileLoopStatement,
            SyntaxKind.WhileToken, SyntaxKind.Expression, SyntaxKind.DoToken, SyntaxKind.Statement)]
        private PersistentSyntaxNode ParseWhileLoop()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(GetKeyword(SyntaxKind.WhileToken));
            children.Add(ParseExpression());
            children.Add(GetKeyword(SyntaxKind.DoToken));
            children.Add(ParseStatement());

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentWhileLoopStatement(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.ForLoopStatement,
            SyntaxKind.ForToken, SyntaxKind.Variable, SyntaxKind.AssignToken, SyntaxKind.SimpleExpression, SyntaxKind.ForDirection, SyntaxKind.SimpleExpression, SyntaxKind.DoToken, SyntaxKind.Statement)]
        [Production(SyntaxKind.ForDirection,
            SyntaxKind.ToToken)]
        [Production(SyntaxKind.ForDirection,
            SyntaxKind.DowntoToken)]
        private PersistentSyntaxNode ParseForLoop()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(GetKeyword(SyntaxKind.ForToken));
            children.Add(ParseVariable());
            children.Add(GetPunctuation(SyntaxKind.AssignToken));
            children.Add(ParseSimpleExpression());
            children.Add(GetKeyword(SyntaxKind.ToToken, SyntaxKind.DowntoToken));
            children.Add(ParseSimpleExpression());
            children.Add(GetKeyword(SyntaxKind.DoToken));
            children.Add(ParseStatement());

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentForLoopStatement(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.IfStatement,
            SyntaxKind.IfToken, SyntaxKind.Expression, SyntaxKind.ThenToken, SyntaxKind.Statement)]
        [Production(SyntaxKind.IfStatement,
            SyntaxKind.IfToken, SyntaxKind.Expression, SyntaxKind.ThenToken, SyntaxKind.Statement, SyntaxKind.ElseToken, SyntaxKind.Statement)]
        private PersistentSyntaxNode ParseIf()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(GetKeyword(SyntaxKind.IfToken));
            children.Add(ParseExpression());
            children.Add(GetKeyword(SyntaxKind.ThenToken));
            children.Add(ParseStatement(SyntaxKind.ElseToken));

            if (sequence.PeekNextNonTrivia().Kind == SyntaxKind.ElseToken)
            {
                children.Add(GetKeyword(SyntaxKind.ElseToken));
                children.Add(ParseStatement());
            }

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentIfStatement(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.CallStatement,
            SyntaxKind.SubprogramIdentifier)]
        [Production(SyntaxKind.CallStatement,
            SyntaxKind.SubprogramIdentifier, SyntaxKind.OpenParanthesisToken, SyntaxKind.ActualParameterList, SyntaxKind.CloseParanthesisToken)]
        [Production(SyntaxKind.SubprogramIdentifier, SyntaxKind.ProcedureIdentifier)]
        [Production(SyntaxKind.SubprogramIdentifier, SyntaxKind.FunctionIdentifier)]
        private PersistentSyntaxNode ParseCall(PersistentSyntaxNode identifier = null)
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            if (identifier == null)
                children.Add(GetIdentifier());
            else
                children.Add(identifier);

            if (CurrentToken.Kind == SyntaxKind.OpenParanthesisToken)
            {
                children.Add(GetPunctuation(SyntaxKind.OpenParanthesisToken));
                children.Add(ParseActualParameterList());

                children.Add(GetPunctuation(SyntaxKind.CloseParanthesisToken));
            }

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentCallSyntax(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.AssignStatement,
             SyntaxKind.Variable, SyntaxKind.AssignToken, SyntaxKind.Expression)]
        [Production(SyntaxKind.AssignStatement,
            SyntaxKind.FunctionIdentifier, SyntaxKind.AssignToken, SyntaxKind.Expression)]
        private PersistentSyntaxNode ParseAssignment()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(ParseVariable());
            children.Add(GetPunctuation(SyntaxKind.AssignToken));
            children.Add(ParseExpression());

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentAssignmentStatementSyntax(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        #endregion Statement

        [Production(SyntaxKind.Variable,
            SyntaxKind.VariableIdentifier)]
        [Production(SyntaxKind.Variable,
            SyntaxKind.DereferencedPointerVariable)]
        [Production(SyntaxKind.Variable,
            SyntaxKind.RecordFieldVariable)]
        [Production(SyntaxKind.Variable,
            SyntaxKind.ArrayVariable)]
        private PersistentSyntaxNode ParseVariable(PersistentSyntaxNode innerVariable = null)
        {
            if (innerVariable == null)
                innerVariable = GetIdentifier();

            PersistentSyntaxNode var = null;
            if (CurrentToken.Kind == SyntaxKind.RoofToken)
            {
                var = ParseDereferencedVariable(innerVariable);
            }
            else if (CurrentToken.Kind == SyntaxKind.DotToken)
            {
                var = ParseRecordFieldVariable(innerVariable);
            }
            else if (CurrentToken.Kind == SyntaxKind.OpenBracketToken)
            {
                var = ParseArrayVariable(innerVariable);
            }
            else
            {
                return innerVariable;
            }

            return ParseVariable(var);
        }

        [Production(SyntaxKind.DereferencedPointerVariable,
            SyntaxKind.Variable, SyntaxKind.RoofToken)]
        private PersistentSyntaxNode ParseDereferencedVariable(PersistentSyntaxNode firstChild)
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(firstChild);
            children.Add(GetPunctuation(SyntaxKind.RoofToken));

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentDereferencedPointerVariableSyntax(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.RecordFieldVariable,
            SyntaxKind.Variable, SyntaxKind.DotToken, SyntaxKind.FieldIdentifier)]
        private PersistentSyntaxNode ParseRecordFieldVariable(PersistentSyntaxNode firstChild)
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(firstChild);
            children.Add(GetPunctuation(SyntaxKind.DotToken));
            children.Add(GetIdentifier(SyntaxKind.FieldIdentifier));

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentRecordFieldVariableSyntax(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.ArrayVariable,
            SyntaxKind.Variable, SyntaxKind.OpenBracketToken, SyntaxKind.IndexList, SyntaxKind.CloseBracketToken)]
        private PersistentSyntaxNode ParseArrayVariable(PersistentSyntaxNode firstChild)
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(firstChild);
            children.Add(GetPunctuation(SyntaxKind.OpenBracketToken));
            children.Add(ParseIndexList());
            children.Add(GetPunctuation(SyntaxKind.CloseBracketToken));

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentArrayVariableSyntax(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.IndexList,
            SyntaxKind.Expression)]
        [Production(SyntaxKind.IndexList,
            SyntaxKind.Expression, SyntaxKind.CommaToken, SyntaxKind.IndexList)]
        private PersistentSyntaxNode ParseIndexList()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(ParseExpression());
            while (CurrentToken.Kind == SyntaxKind.CommaToken)
            {
                children.Add(GetPunctuation(SyntaxKind.CommaToken));
                children.Add(ParseExpression());
            }

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentIndexList(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.ActualParameterList,
            SyntaxKind.ActualNonEmptyParameterList)]
        [Production(SyntaxKind.ActualParameterList)]
        [Production(SyntaxKind.ActualNonEmptyParameterList,
            SyntaxKind.ActualParameter)]
        [Production(SyntaxKind.ActualNonEmptyParameterList,
            SyntaxKind.Expression, SyntaxKind.CommaToken, SyntaxKind.ActualNonEmptyParameterList)]
        private PersistentSyntaxNode ParseActualParameterList()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            Token next = sequence.PeekNextNonTrivia();
            while (next.Kind != SyntaxKind.CloseParanthesisToken && !(next is EndOfSequence) && next.Kind != SyntaxKind.EndToken && next.Kind != SyntaxKind.SemicolonToken)
            {
                if (children.Count > 0)
                    children.Add(GetPunctuation(SyntaxKind.CommaToken));

                children.Add(ParseActualParameter());
                next = sequence.PeekNextNonTrivia();
            }

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentActualParameterList(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.ActualParameter,
            SyntaxKind.Expression)]
        [Production(SyntaxKind.ActualParameter,
            SyntaxKind.Expression, SyntaxKind.ColonToken, SyntaxKind.SimpleExpression)]
        [Production(SyntaxKind.ActualParameter,
            SyntaxKind.Expression, SyntaxKind.ColonToken, SyntaxKind.SimpleExpression, SyntaxKind.ColonToken, SyntaxKind.SimpleExpression)]
        private PersistentSyntaxNode ParseActualParameter()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(ParseExpression());
            if (CurrentToken.Kind == SyntaxKind.ColonToken)
            {
                children.Add(GetPunctuation(SyntaxKind.ColonToken));
                children.Add(ParseSimpleExpression());
                if (CurrentToken.Kind == SyntaxKind.ColonToken)
                {
                    children.Add(GetPunctuation(SyntaxKind.ColonToken));
                    children.Add(ParseSimpleExpression());
                }
            }

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentActualParameter(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.Expression,
            SyntaxKind.SimpleExpression)]
        [Production(SyntaxKind.Expression,
            SyntaxKind.SimpleExpression, SyntaxKind.SignBetweenExpressions, SyntaxKind.SimpleExpression)]
        [Production(SyntaxKind.SignBetweenExpressions, SyntaxKind.LessOrEqualThanToken)]
        [Production(SyntaxKind.SignBetweenExpressions, SyntaxKind.LessThanToken)]
        [Production(SyntaxKind.SignBetweenExpressions, SyntaxKind.GreaterOrEqualThanToken)]
        [Production(SyntaxKind.SignBetweenExpressions, SyntaxKind.GreaterThanToken)]
        [Production(SyntaxKind.SignBetweenExpressions, SyntaxKind.EqualsToken)]
        [Production(SyntaxKind.SignBetweenExpressions, SyntaxKind.DoesNotEqualToken)]
        [Production(SyntaxKind.SignBetweenExpressions, SyntaxKind.IsToken)]
        [Production(SyntaxKind.SignBetweenExpressions, SyntaxKind.InToken)]
        private PersistentSyntaxNode ParseExpression()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(ParseSimpleExpression());
            if (CurrentToken.Kind.IsRelationalOperator())
            {
                children.Add(GetPunctuation(
                    SyntaxKind.LessOrEqualThanToken,
                    SyntaxKind.GreaterOrEqualThanToken,
                    SyntaxKind.LessThanToken,
                    SyntaxKind.GreaterThanToken,
                    SyntaxKind.EqualsToken,
                    SyntaxKind.DoesNotEqualToken));

                children.Add(ParseSimpleExpression());
            }
            else if (CurrentToken.Kind == SyntaxKind.InToken || CurrentToken.Kind == SyntaxKind.IsToken)
            {
                children.Add(GetKeyword(SyntaxKind.InToken, SyntaxKind.IsToken));
                children.Add(ParseSimpleExpression());
            }

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentExpressionSyntax(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.SimpleExpression,
            SyntaxKind.Term)]
        [Production(SyntaxKind.SimpleExpression,
            SyntaxKind.Term, SyntaxKind.SignBetweenTerms, SyntaxKind.SimpleExpression)]
        [Production(SyntaxKind.SignBetweenTerms, SyntaxKind.PlusToken)]
        [Production(SyntaxKind.SignBetweenTerms, SyntaxKind.MinusToken)]
        [Production(SyntaxKind.SignBetweenTerms, SyntaxKind.OrToken)]
        [Production(SyntaxKind.SignBetweenTerms, SyntaxKind.XorToken)]
        private PersistentSyntaxNode ParseSimpleExpression()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(ParseTerm());
            while (true)
            {
                if (CurrentToken.Kind == SyntaxKind.PlusToken || CurrentToken.Kind == SyntaxKind.MinusToken)
                {
                    children.Add(GetPunctuation(SyntaxKind.PlusToken, SyntaxKind.MinusToken));
                }
                else if (CurrentToken.Kind == SyntaxKind.OrToken || CurrentToken.Kind == SyntaxKind.XorToken)
                {
                    children.Add(GetKeyword(SyntaxKind.OrToken, SyntaxKind.XorToken));
                }
                else
                    break;

                children.Add(ParseTerm());
            }

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentSimpleExpressionSyntax(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.Term,
            SyntaxKind.Factor)]
        [Production(SyntaxKind.Term,
            SyntaxKind.Factor, SyntaxKind.SignBetweenFactors, SyntaxKind.Term)]
        [Production(SyntaxKind.SignBetweenFactors, SyntaxKind.AsteriskToken)]
        [Production(SyntaxKind.SignBetweenFactors, SyntaxKind.SlashToken)]
        [Production(SyntaxKind.SignBetweenFactors, SyntaxKind.DivToken)]
        [Production(SyntaxKind.SignBetweenFactors, SyntaxKind.ModToken)]
        [Production(SyntaxKind.SignBetweenFactors, SyntaxKind.AndToken)]
        [Production(SyntaxKind.SignBetweenFactors, SyntaxKind.ShlToken)]
        [Production(SyntaxKind.SignBetweenFactors, SyntaxKind.ShrToken)]
        [Production(SyntaxKind.SignBetweenFactors, SyntaxKind.AsToken)]
        [Production(SyntaxKind.SignBetweenFactors, SyntaxKind.LeftShiftToken)]
        [Production(SyntaxKind.SignBetweenFactors, SyntaxKind.RightShiftToken)]
        private PersistentSyntaxNode ParseTerm()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            children.Add(ParseFactor());
            while (true)
            {
                if (CurrentToken.Kind.IsAnyOf(SyntaxKind.AsteriskToken, SyntaxKind.SlashToken, SyntaxKind.LeftShiftToken, SyntaxKind.RightShiftToken))
                {
                    children.Add(GetPunctuation(SyntaxKind.AsteriskToken, SyntaxKind.SlashToken, SyntaxKind.LeftShiftToken, SyntaxKind.RightShiftToken));
                }
                else if (CurrentToken.Kind.IsAnyOf(SyntaxKind.DivToken, SyntaxKind.ModToken, SyntaxKind.AndToken,
                    SyntaxKind.ShlToken, SyntaxKind.ShrToken, SyntaxKind.AsToken))
                {
                    children.Add(GetKeyword(SyntaxKind.DivToken, SyntaxKind.ModToken, SyntaxKind.AndToken,
                    SyntaxKind.ShlToken, SyntaxKind.ShrToken, SyntaxKind.AsToken));
                }
                else
                    break;

                children.Add(ParseFactor());
            }

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentTermSyntax(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.Factor, SyntaxKind.Constant)]
        [Production(SyntaxKind.Factor,
            SyntaxKind.NotToken, SyntaxKind.Factor)]
        [Production(SyntaxKind.Factor,
            SyntaxKind.PlusToken, SyntaxKind.Factor)]
        [Production(SyntaxKind.Factor,
            SyntaxKind.MinusToken, SyntaxKind.Factor)]
        [Production(SyntaxKind.Factor,
            SyntaxKind.OpenParanthesisToken, SyntaxKind.Expression, SyntaxKind.CloseParanthesisToken)]
        [Production(SyntaxKind.Factor,
            SyntaxKind.FunctionIdentifier)]
        [Production(SyntaxKind.Factor,
            SyntaxKind.FunctionIdentifier, SyntaxKind.OpenParanthesisToken, SyntaxKind.ActualParameterList, SyntaxKind.CloseParanthesisToken)]
        [Production(SyntaxKind.Factor,
            SyntaxKind.Variable)]
        private PersistentSyntaxNode ParseFactor()
        {
            List<PersistentSyntaxNode> children = new List<PersistentSyntaxNode>();

            if (CurrentToken.Kind == SyntaxKind.NotToken)
            {
                children.Add(GetKeyword(SyntaxKind.NotToken));
                children.Add(ParseFactor());
            }
            else if (CurrentToken.Kind.IsAnyOf(SyntaxKind.PlusToken, SyntaxKind.MinusToken))
            {
                children.Add(GetPunctuation(SyntaxKind.PlusToken, SyntaxKind.MinusToken));
                children.Add(ParseFactor());
            }
            else if (CurrentToken.Kind == SyntaxKind.OpenParanthesisToken)
            {
                children.Add(GetPunctuation(SyntaxKind.OpenParanthesisToken));
                children.Add(ParseExpression());
                children.Add(GetPunctuation(SyntaxKind.CloseParanthesisToken));
            }
            else if (CurrentToken.Kind.IsIdentifier())
            {
                if (NextToken.Kind == SyntaxKind.OpenParanthesisToken)
                {
                    children.Add(ParseCall());
                }
                else
                    children.Add(ParseVariable());
            }
            else
            {
                children.Add(GetConstant());
            }

            PersistentSyntaxTriviaList triviaBefore;
            ResolveTrivia(children, out triviaBefore);

            return new PersistentFactorSyntax(new PersistentChildrenCollection(children), triviaBefore, PersistentSyntaxTriviaList.Empty);
        }

        [Production(SyntaxKind.Constant, SyntaxKind.StringConstant)]
        [Production(SyntaxKind.Constant, SyntaxKind.ConstantIdentifier)]
        [Production(SyntaxKind.Constant, SyntaxKind.UintConstant)]
        [Production(SyntaxKind.Constant, SyntaxKind.RealConstant)]
        [Production(SyntaxKind.Constant, SyntaxKind.NilConstant)]
        [Production(SyntaxKind.Constant, SyntaxKind.EnumMemberIdentifier)]
        private PersistentSyntaxNode GetConstant()
        {
            var token = CurrentToken;
            EnsureTriviaRead();
            if (token is ConstantToken)
            {
                sequence.EatToken();
                return new PersistentConstant(token as ConstantToken, EatCurrentTrivia(), PersistentSyntaxTriviaList.Empty);
            }
            else if (CurrentToken.Kind.IsIdentifier())
            {
                sequence.EatToken();
                if (token.Kind.IsModifier())
                    token = new IdentifierToken(token.UnderlyingText);
                return new PersistentConstantIdentifier(token as IdentifierToken, EatCurrentTrivia(), PersistentSyntaxTriviaList.Empty);
            }
            else
            {
                return new PersistentMissingTerminal(SyntaxKind.Constant);
            }
        }

        #region Helpers

        private PersistentSyntaxNode GetKeyword(params SyntaxKind[] expected)
        {
            if (expected.Length == 0) throw new Exception();
            EnsureTriviaRead();

            const int maxTries = 1;
            for (int i = 0; i < maxTries; i++)
            {
                Token token = sequence.PeekNextNonTrivia();
                if (expected.Contains(token.Kind))
                {
                    sequence.EatToken();
                    return new PersistentKeyword(token as KeywordToken, EatCurrentTrivia(), PersistentSyntaxTriviaList.Empty);
                }
                else if (token.Kind == SyntaxKind.SemicolonToken || token.Kind == SyntaxKind.EndToken || token is EndOfSequence) //reset points
                {
                    return new PersistentMissingTerminal(expected);
                }
                else
                {
                    CurrentTrivia.Add(new PersistentUnexpectedSyntaxTrivia(token, expected));
                    sequence.EatToken();
                    CurrentTrivia.AddRange(GetTriviaImpl());
                }
            }
            return new PersistentMissingTerminal(expected);
        }

        private void ResolveTrivia(List<PersistentSyntaxNode> children, out PersistentSyntaxTriviaList triviaBefore)
        {
            var nonEmptyChildren = GetNonEmptyChildren(children);

            if (nonEmptyChildren.Count == 0)
            {
                triviaBefore = PersistentSyntaxTriviaList.Empty;
                return;
            }

            triviaBefore = nonEmptyChildren[0].TriviaBefore;

            for (int i = 0; i < nonEmptyChildren.Count - 1; i++)
            {
                var newChild = nonEmptyChildren[i].WithTriviaAfter(nonEmptyChildren[i + 1].TriviaBefore);
                var index = children.FindIndex(node => node == nonEmptyChildren[i]);
                children[index] = newChild;
            }
        }

        private List<PersistentSyntaxNodeWithTrivia> GetNonEmptyChildren(List<PersistentSyntaxNode> children)
        {
            List<PersistentSyntaxNodeWithTrivia> result = new List<PersistentSyntaxNodeWithTrivia>();

            for (int i = 0; i < children.Count; i++)
            {
                if (children[i].Length > 0)
                    result.Add(children[i] as PersistentSyntaxNodeWithTrivia);
            }

            return result;
        }

        private void EnsureTriviaRead()
        {
            if (CurrentTrivia != null) return;
            CurrentTrivia = new List<PersistentSyntaxTrivia>(GetTriviaImpl());
        }

        private PersistentSyntaxTriviaList EatCurrentTrivia()
        {
            var result = new PersistentSyntaxTriviaList(CurrentTrivia);
            CurrentTrivia = null;
            return result;
        }

        private PersistentSyntaxNode GetPunctuation(params SyntaxKind[] expected)
        {
            if (expected.Length == 0) throw new Exception();
            PersistentSyntaxTriviaList triviaBefore;
            EnsureTriviaRead();

            const int maxTries = 1;
            for (int i = 0; i < maxTries; i++)
            {
                Token token = sequence.PeekNextNonTrivia();
                if (expected.Contains(token.Kind))
                {
                    sequence.EatToken();
                    triviaBefore = EatCurrentTrivia();
                    return new PersistentPunctuation(token as PunctuationToken, triviaBefore, PersistentSyntaxTriviaList.Empty);
                }
                else if (token.Kind == SyntaxKind.SemicolonToken || token.Kind == SyntaxKind.EndToken || token is EndOfSequence //reset points
                    || expected.Contains(SyntaxKind.SemicolonToken))
                {
                    return new PersistentMissingTerminal(expected);
                }
                else
                {
                    CurrentTrivia.Add(new PersistentUnexpectedSyntaxTrivia(token, expected));
                    sequence.EatToken();
                    CurrentTrivia.AddRange(GetTriviaImpl());
                }
            }
            return new PersistentMissingTerminal(expected);
        }

        private PersistentSyntaxNode GetIdentifier(SyntaxKind identifierType = SyntaxKind.IdentifierToken)
        {
            PersistentSyntaxTriviaList triviaBefore;
            EnsureTriviaRead();
            Token token = sequence.PeekNextNonTrivia();
            if (token.Kind.IsIdentifier())
            {
                sequence.EatToken();
                triviaBefore = EatCurrentTrivia();
                token = new IdentifierToken(token.UnderlyingText, identifierType);
                return new PersistentIdentifier(token as IdentifierToken, triviaBefore, PersistentSyntaxTriviaList.Empty);
            }
            else
            {
                triviaBefore = null;
                return new PersistentMissingTerminal(SyntaxKind.IdentifierToken);
            }
        }

        private IEnumerable<PersistentSyntaxTrivia> GetTriviaImpl()
        {
            var tokens = sequence.GetAllTrivia();
            foreach (var token in tokens)
            {
                switch (token.Kind)
                {
                    case SyntaxKind.Unknown:
                        yield return new PersistentUnknownSyntaxTrivia(token as UnknownToken);
                        break;

                    case SyntaxKind.Whitespace:
                        yield return new PersistentWhitespaceSyntaxTrivia(token as WhitespaceToken);
                        break;

                    case SyntaxKind.Comment:
                    case SyntaxKind.MultiLineComment:
                        yield return new PersistentCommentSyntaxTrivia(token as CommentToken);
                        break;

                    default:
                        throw new Exception("Should not happen; most probably forgot to add new TriviaKind here");
                }
            }
        }

        private PersistentSyntaxTriviaList EatAllTokens()
        {
            List<PersistentSyntaxTrivia> trivia = new List<PersistentSyntaxTrivia>();
            while (!(sequence.PeekNextNonTrivia() is EndOfSequence))
            {
                trivia.AddRange(GetTriviaImpl());
                trivia.Add(new PersistentUnexpectedSyntaxTrivia(sequence.PeekNextNonTrivia(), SyntaxKind.None));
                sequence.EatToken();
            }
            trivia.AddRange(GetTriviaImpl());
            return new PersistentSyntaxTriviaList(trivia);
        }

        #endregion Helpers
    }
}