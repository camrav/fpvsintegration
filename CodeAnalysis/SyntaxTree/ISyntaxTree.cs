﻿namespace CodeAnalysis.SyntaxTree
{
    public interface ISyntaxTree
    {
        SyntaxNode Root { get; }
    }
}