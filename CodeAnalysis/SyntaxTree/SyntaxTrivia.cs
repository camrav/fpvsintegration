﻿namespace CodeAnalysis.SyntaxTree
{
    public abstract class SyntaxTrivia : SyntaxNode
    {
        public abstract string Text { get; }
    }
}