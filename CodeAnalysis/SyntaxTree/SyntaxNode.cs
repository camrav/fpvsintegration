﻿using System.Collections.Generic;
using System.Linq;

namespace CodeAnalysis.SyntaxTree
{
    public abstract class SyntaxNode
    {
        public abstract SyntaxNode Parent { get; }
        public abstract ISyntaxTree Tree { get; }

        /// <summary>
        /// Span of this node in characters, including its leading and trailing trivia.
        /// </summary>
        public virtual TextSpan SpanWithTrivia
        {
            get
            {
                int start = Start;
                int length = Length;
                if (HasTriviaBefore)
                {
                    int len = GetTriviaBefore().Length;
                    start -= len;
                    length += len;
                }
                if (HasTriviaAfter)
                    length += GetTriviaAfter().Length;

                return new TextSpan(start, length);
            }
        }

        public virtual TextSpan Span => new TextSpan(Start, Length);
        public abstract int Start { get; }
        public abstract int Length { get; }
        public virtual int End => Start + Length;
        public abstract IReadOnlyList<SyntaxNode> Children { get; }
        public abstract bool IsMissing { get; }

        public virtual IEnumerable<SyntaxNode> ChildrenIn(TextSpan span)
        {
            IEnumerable<SyntaxNode> result = new SyntaxNode[0];
            if (!SpanWithTrivia.OverlapsWith(span)) return result;

            if (span.Contains(SpanWithTrivia))
                return AllChildren();

            if (HasTriviaBefore)
            {
                var triviaBefore = GetTriviaBefore();
                result = GetChildrenInTrivia(span, triviaBefore);
                span = span.Exluding(triviaBefore.Span);
            }

            result = result.Concat(GetThisOrChildren(span));

            return result;
        }

        private IEnumerable<SyntaxNode> AllChildren()
        {
            int position = HasTriviaBefore ? GetTriviaBefore().Start : Start;

            Queue<SyntaxNode> toReturn = new Queue<SyntaxNode>();
            toReturn.Enqueue(this);

            while (toReturn.Count > 0)
            {
                var node = toReturn.Dequeue();
                foreach (var child in node.Children)
                    toReturn.Enqueue(child);

                if (node.HasTriviaBefore && position < Start)
                {
                    foreach (var trivia in node.GetTriviaBefore())
                        yield return trivia;
                    position = Start;
                }

                if (node.Children.Count == 0 && node.Length > 0)
                    yield return node;

                if (node.HasTriviaAfter)
                {
                    foreach (var trivia in node.GetTriviaAfter())
                        yield return trivia;
                    position = End + node.GetTriviaAfter().Length;
                }
            }
        }

        protected virtual IEnumerable<SyntaxNode> GetThisOrChildren(TextSpan span)
        {
            if (span.Length == 0)
                yield break;
            if (Children.Count == 0)
            {
                if (span.OverlapsWith(new TextSpan(Start, Length)))
                    yield return this;

                foreach (var trivia in GetChildrenInTrivia(span, GetTriviaAfter()))
                    yield return trivia;
                yield break;
            }

            if (SpanWithTrivia.OverlapsWith(span))
            {
                foreach (var child in Children)
                {
                    if (child.SpanWithTrivia.End < span.Start)
                        continue;
                    if (child.SpanWithTrivia.Start > span.End)
                        break;

                    foreach (var result in child.ChildrenIn(span))
                    {
                        //  if (!span.OverlapsWith(new TextSpan(result.Start, result.Length)))
                        //      throw new Exception("Should not happen"); //TODO: maybe ignore the result?

                        yield return result;
                    }
                    span = span.Exluding(child.SpanWithTrivia);
                }
            }
        }

        protected virtual IEnumerable<SyntaxNode> GetChildrenInTrivia(TextSpan span, SyntaxTriviaList triviaList)
        {
            if (span.OverlapsWith(new TextSpan(triviaList.Start, triviaList.Length)))
            {
                foreach (var trivia in triviaList)
                {
                    foreach (var result in trivia.ChildrenIn(span))
                    {
                        yield return result;
                    }
                }
            }
        }

        public virtual bool HasTriviaBefore => GetTriviaBefore().Count > 0;
        public virtual bool HasTriviaAfter => GetTriviaAfter().Count > 0;

        public abstract SyntaxTriviaList GetTriviaBefore();

        public abstract SyntaxTriviaList GetTriviaAfter();
    }
}