﻿using CodeAnalysis.SyntaxTree.OnDemandTree;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CodeAnalysis.SyntaxTree
{
    public class SyntaxTriviaList : IReadOnlyList<FpSyntaxNode>
    {
        private IReadOnlyList<FpSyntaxNode> list;

        public int Start { get; }
        public int Length => list.Aggregate(0, (sum, node) => sum + node.Length);
        public TextSpan Span => new TextSpan(Start, Length);

        public SyntaxTriviaList(int start, IEnumerable<FpSyntaxNode> trivia)
        {
            Start = start;
            list = new List<FpSyntaxNode>(trivia);
        }

        public SyntaxTriviaList(int start)
            : this(start, new FpSyntaxNode[0])
        {
        }

        public FpSyntaxNode this[int index]
        {
            get
            {
                return list[index];
            }
        }

        public int Count => list.Count;

        public IEnumerator<FpSyntaxNode> GetEnumerator()
        {
            return list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}