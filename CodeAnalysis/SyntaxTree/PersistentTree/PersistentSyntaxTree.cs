﻿using FpLexer;

namespace CodeAnalysis.SyntaxTree.PersistentTree
{
    internal class PersistentSyntaxTree
    {
        public PersistentSyntaxNode Root { get; }

        private PersistentSyntaxTree(PersistentSyntaxNode root)
        {
            Root = root;
        }

        internal static PersistentSyntaxTree Parse(ISourceText sourceText)
        {
            FpParser parser = new FpParser();
            return new PersistentSyntaxTree(parser.Parse(sourceText));
            //   return new PersistentSyntaxTree(PersistentProgramRootNode.Parse(sourceText));
        }

        /*  public PersistentSyntaxTree ApplyChange(TextChange change)
          {
              return new PersistentSyntaxTree(Root.ApplyChange(change));
          }*/
    }
}