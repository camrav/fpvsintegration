﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace CodeAnalysis.SyntaxTree.PersistentTree
{
    internal abstract class PersistentSyntaxNodeCollection<T> : IReadOnlyList<T> where T : PersistentSyntaxNode
    {
        private IReadOnlyList<T> list;
        private Lazy<int> textLength;

        public PersistentSyntaxNodeCollection(IEnumerable<T> nodes)
        {
            AssertNodesDoNotContainNull(nodes);
            list = new List<T>(nodes);
            textLength = new Lazy<int>(() => list.Aggregate(0, (len, node) => len + node.Length));
        }

        [Conditional("DEBUG")]
        private void AssertNodesDoNotContainNull(IEnumerable<T> nodes)
        {
            if (nodes.Any(node => node == null))
                throw new Exception("A node is null!");
        }

        public int Length => textLength.Value;

        public T this[int index]
        {
            get
            {
                return list[index];
            }
        }

        public int Count => list.Count;

        public IEnumerator<T> GetEnumerator()
        {
            return list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        protected IEnumerable<T> WithSwappedNode(int index, T newNode)
        {
            return new List<T>(list) { [index] = newNode };
        }
    }
}