﻿using FpLexer;
using System.Text;

namespace CodeAnalysis.SyntaxTree.PersistentTree
{
    internal abstract class PersistentNonterminal : PersistentSyntaxNodeWithTrivia
    {
        private readonly PersistentChildrenCollection children;
        public override bool IsMissing => Children.Count == 0;
        public override int Length { get; }

        public PersistentNonterminal(SyntaxKind syntaxKind, PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter) :
            base(syntaxKind, triviaBefore, triviaAfter)
        {
            this.children = children;
            Length = CalculateTextLength();
        }

        public PersistentNonterminal(SyntaxKind syntaxKind, PersistentChildrenCollection children, bool isMissing = true)
            : base(syntaxKind, isMissing)
        {
            this.children = children;
            Length = 0;
        }

        private int CalculateTextLength()
        {
            int lengthInChars = 0;
            PersistentSyntaxTriviaList lastTrivia = null;
            bool firstChild = true;

            foreach (var child in Children)
            {
                if (child is PersistentSyntaxNodeWithTrivia && child.Length != 0)
                {
                    var triviaChild = child as PersistentSyntaxNodeWithTrivia;
                    if (!firstChild && triviaChild.TriviaBefore != lastTrivia)
                    {
                        lengthInChars += triviaChild.TriviaBefore.Length;
                        lastTrivia = triviaChild.TriviaBefore;
                    }
                    if (triviaChild.TriviaAfter != lastTrivia)
                    {
                        lengthInChars += triviaChild.TriviaAfter.Length;
                        lastTrivia = triviaChild.TriviaAfter;
                    }
                    lengthInChars += child.Length;
                    firstChild = false;
                }
            }
            if (lastTrivia != null)
                lengthInChars -= lastTrivia.Length;

            return lengthInChars;
        }

        public override PersistentChildrenCollection Children => children;

        public override string GetText()
        {
            StringBuilder sb = new StringBuilder();
            bool appendedTrivia = true;

            foreach (var child in children)
            {
                if (child is PersistentSyntaxNodeWithTrivia)
                {
                    var childWithTrivia = child as PersistentSyntaxNodeWithTrivia;

                    if (!appendedTrivia)
                        sb.Append(childWithTrivia.TriviaBefore.GetText());

                    sb.Append(child.GetText());

                    sb.Append(childWithTrivia.TriviaAfter.GetText());
                    appendedTrivia = true;
                }

                sb.Append(child.GetText());
                appendedTrivia = false;
            }

            return sb.ToString();
        }
    }
}