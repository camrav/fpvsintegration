﻿using FpLexer;

namespace CodeAnalysis.SyntaxTree.PersistentTree
{
    internal abstract class PersistentTerminal : PersistentSyntaxNode
    {
        public PersistentTerminal(SyntaxKind kind) :
            base(kind)
        {
        }

        public override PersistentChildrenCollection Children => PersistentChildrenCollection.Empty;
    }

    internal abstract class PersistentTerminalWithTrivia : PersistentTerminal, PersistentTriviaAroundNode
    {
        public PersistentSyntaxTriviaList TriviaAfter { get; }
        public PersistentSyntaxTriviaList TriviaBefore { get; }

        public PersistentTerminalWithTrivia(SyntaxKind kind, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter) : base(kind)
        {
            TriviaBefore = triviaBefore;
            TriviaAfter = triviaAfter;
        }
    }

    internal abstract class PersistentExistingTerminal : PersistentSyntaxNodeWithTrivia
    {
        protected Token token;
        public override bool IsMissing => false;
        public override int Length => token.UnderlyingText.Length;

        public override PersistentChildrenCollection Children => PersistentChildrenCollection.Empty;

        public PersistentExistingTerminal(Token token, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(token.Kind, triviaBefore, triviaAfter)
        {
            this.token = token;
        }

        public override string GetText()
        {
            return token.UnderlyingText;
        }
    }

    internal class PersistentMissingTerminal : PersistentTerminalWithTrivia
    {
        public override bool IsMissing => true;

        public override int Length => 0;

        public SyntaxKind[] Expected { get; }

        public PersistentMissingTerminal(params SyntaxKind[] expected)
            : base(expected[0], PersistentSyntaxTriviaList.Empty, PersistentSyntaxTriviaList.Empty)
        {
            Expected = expected;
        }

        public override string GetText()
        {
            return string.Empty;
        }
    }

    internal class PersistentKeyword : PersistentExistingTerminal
    {
        public PersistentKeyword(KeywordToken token, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(token, triviaBefore, triviaAfter)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentKeyword(token as KeywordToken, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentIdentifier : PersistentExistingTerminal
    {
        public PersistentIdentifier(IdentifierToken token, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter) : base(token, triviaBefore, triviaAfter)
        {
        }

        public PersistentIdentifier(KeywordToken modifier, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter) : base(modifier, triviaBefore, triviaAfter)
        { }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentIdentifier(token as IdentifierToken, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentPunctuation : PersistentExistingTerminal
    {
        public PersistentPunctuation(PunctuationToken token, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter) : base(token, triviaBefore, triviaAfter)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentPunctuation(token as PunctuationToken, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentConstant : PersistentExistingTerminal
    {
        public PersistentConstant(ConstantToken token, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(token, triviaBefore, triviaAfter)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentConstant(token as ConstantToken, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentConstantIdentifier : PersistentExistingTerminal
    {
        public PersistentConstantIdentifier(IdentifierToken token, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(token, triviaBefore, triviaAfter)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentConstantIdentifier(token as IdentifierToken, TriviaBefore, newTriviaAfter);
        }
    }
}