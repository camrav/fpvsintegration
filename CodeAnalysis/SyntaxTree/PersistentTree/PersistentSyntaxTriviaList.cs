﻿using System.Collections.Generic;
using System.Text;

namespace CodeAnalysis.SyntaxTree.PersistentTree
{
    internal class PersistentSyntaxTriviaList : PersistentSyntaxNodeCollection<PersistentSyntaxTrivia>
    {
        public static PersistentSyntaxTriviaList Empty => new PersistentSyntaxTriviaList(new PersistentSyntaxTrivia[0]);

        public PersistentSyntaxTriviaList(IEnumerable<PersistentSyntaxTrivia> nodes) : base(nodes)
        {
        }

        public string GetText()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var trivia in this)
            {
                sb.Append(trivia.GetText());
            }
            return sb.ToString();
        }

        public override string ToString()
        {
            return string.Format("Trivia: {0}", GetText());
        }
    }
}