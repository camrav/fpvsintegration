﻿using FpLexer;

namespace CodeAnalysis.SyntaxTree.PersistentTree
{
    internal abstract class PersistentSyntaxNodeWithTrivia : PersistentSyntaxNode
    {
        public PersistentSyntaxTriviaList TriviaBefore { get; private set; }
        public PersistentSyntaxTriviaList TriviaAfter { get; private set; }
        public override bool IsMissing { get; } = false;

        public PersistentSyntaxNodeWithTrivia(SyntaxKind kind, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(kind)
        {
            TriviaBefore = triviaBefore;
            TriviaAfter = triviaAfter;
        }

        public PersistentSyntaxNodeWithTrivia(SyntaxKind kind, bool isMissing)
            : this(kind, PersistentSyntaxTriviaList.Empty, PersistentSyntaxTriviaList.Empty)
        {
            IsMissing = isMissing;
        }

        public PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentSyntaxTriviaList newTriviaAfter)
        {
            for (int i = Children.Count - 1; i >= 0; i--)
            {
                if (Children[i] is PersistentSyntaxNodeWithTrivia && Children[i].Length > 0)
                {
                    return WithTriviaAfter(Children.WithSwappedChild(i, ((PersistentSyntaxNodeWithTrivia)Children[i]).WithTriviaAfter(newTriviaAfter)), newTriviaAfter);
                }
            }

            return WithTriviaAfter(Children, newTriviaAfter);
        }

        protected abstract PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter);
    }
}