﻿using FpLexer;

namespace CodeAnalysis.SyntaxTree.PersistentTree
{
    internal abstract class PersistentSyntaxTrivia : PersistentSyntaxNode
    {
        private Token token;
        public override bool IsMissing => false;

        public PersistentSyntaxTrivia(Token token) : base(token.Kind)
        {
            this.token = token;
        }

        public override PersistentChildrenCollection Children => PersistentChildrenCollection.Empty;

        public override int Length => token.UnderlyingText.Length;

        public override string GetText()
        {
            return token.UnderlyingText;
        }
    }

    internal class PersistentWhitespaceSyntaxTrivia : PersistentSyntaxTrivia
    {
        public PersistentWhitespaceSyntaxTrivia(WhitespaceToken token) : base(token)
        {
        }
    }

    internal class PersistentCommentSyntaxTrivia : PersistentSyntaxTrivia
    {
        public PersistentCommentSyntaxTrivia(CommentToken token) : base(token)
        {
        }
    }

    internal class PersistentUnknownSyntaxTrivia : PersistentSyntaxTrivia
    {
        public PersistentUnknownSyntaxTrivia(UnknownToken token)
            : base(token)
        {
        }
    }

    internal class PersistentUnexpectedSyntaxTrivia : PersistentSyntaxTrivia
    {
        public SyntaxKind[] Expected { get; }

        public PersistentUnexpectedSyntaxTrivia(Token token, params SyntaxKind[] expected) : base(token)
        {
            this.Expected = expected;
        }
    }

    internal class PersistentAssemblerTrivia : PersistentSyntaxTrivia
    {
        public PersistentAssemblerTrivia(Token token) : base(token)
        {
        }
    }
}