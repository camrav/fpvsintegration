﻿using System.Collections.Generic;

namespace CodeAnalysis.SyntaxTree.PersistentTree
{
    internal class PersistentChildrenCollection : PersistentSyntaxNodeCollection<PersistentSyntaxNode>
    {
        public static PersistentChildrenCollection Empty => new PersistentChildrenCollection(new PersistentSyntaxNode[0]);

        public PersistentChildrenCollection(IEnumerable<PersistentSyntaxNode> nodes) : base(nodes)
        {
        }

        public PersistentChildrenCollection WithSwappedChild(int index, PersistentSyntaxNode child)
        {
            return new PersistentChildrenCollection(WithSwappedNode(index, child));
        }
    }
}