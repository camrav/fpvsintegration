﻿namespace CodeAnalysis.SyntaxTree.PersistentTree
{
    internal interface PersistentTriviaAroundNode
    {
        PersistentSyntaxTriviaList TriviaBefore { get; }
        PersistentSyntaxTriviaList TriviaAfter { get; }
    }
}