﻿using FpLexer;

namespace CodeAnalysis.SyntaxTree.PersistentTree
{
    internal abstract class PersistentSyntaxNode
    {
        public abstract int Length { get; }
        public abstract PersistentChildrenCollection Children { get; }
        public SyntaxKind Kind { get; private set; }
        public abstract bool IsMissing { get; }

        public abstract string GetText();

        public PersistentSyntaxNode(SyntaxKind kind)
        {
            Kind = kind;
        }

        public override string ToString()
        {
            return string.Format("{0}Persistent {1} with {2} children; length: {3}",
                IsMissing ? "Missing " : string.Empty, Kind.ToString(), Children.Count, Length);
        }
    }
}