﻿using FpLexer;

namespace CodeAnalysis.SyntaxTree.PersistentTree
{
    internal class PersistentProgramRootNode : PersistentNonterminal
    {
        public PersistentProgramRootNode(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.ProgramRoot, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentProgramRootNode(PersistentChildrenCollection children)
            : base(SyntaxKind.ProgramRoot, children)
        { }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentProgramRootNode(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentBlockSyntax : PersistentNonterminal
    {
        public PersistentBlockSyntax(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.Block, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentBlockSyntax(PersistentChildrenCollection children)
            : base(SyntaxKind.Block, children)
        { }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentBlockSyntax(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentVarSyntax : PersistentNonterminal
    {
        public PersistentVarSyntax(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.VarDeclaration, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentVarSyntax(PersistentChildrenCollection children)
            : base(SyntaxKind.VarDeclaration, children)
        { }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentVarSyntax(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentVarDeclarationLineSyntax : PersistentNonterminal
    {
        public PersistentVarDeclarationLineSyntax(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.VarDeclarationLine, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentVarDeclarationLineSyntax(PersistentChildrenCollection children)
            : base(SyntaxKind.VarDeclarationLine, children)
        { }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentVarDeclarationLineSyntax(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentStatementSequenceSyntax : PersistentNonterminal
    {
        public PersistentStatementSequenceSyntax(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.StatementSequence, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentStatementSequenceSyntax(PersistentChildrenCollection children)
            : base(SyntaxKind.StatementSequence, children)
        { }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentStatementSequenceSyntax(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentAssignmentStatementSyntax : PersistentNonterminal
    {
        public PersistentAssignmentStatementSyntax(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.AssignStatement, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentAssignmentStatementSyntax(PersistentChildrenCollection children)
            : base(SyntaxKind.AssignStatement, children)
        { }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentAssignmentStatementSyntax(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentNoStatementSyntax : PersistentNonterminal
    {
        public override bool IsMissing => false;

        public PersistentNoStatementSyntax(PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.NoStatement, PersistentChildrenCollection.Empty, triviaBefore, triviaAfter)
        {
        }

        public PersistentNoStatementSyntax()
            : base(SyntaxKind.NoStatement, PersistentChildrenCollection.Empty)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentNoStatementSyntax(TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentExpressionSyntax : PersistentNonterminal
    {
        public PersistentExpressionSyntax(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.Expression, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentExpressionSyntax(PersistentChildrenCollection children)
            : base(SyntaxKind.Expression, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentExpressionSyntax(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentCallSyntax : PersistentNonterminal
    {
        public PersistentCallSyntax(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.CallStatement, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentCallSyntax(PersistentChildrenCollection children)
            : base(SyntaxKind.CallStatement, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentCallSyntax(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentActualParameterList : PersistentNonterminal
    {
        public PersistentActualParameterList(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.ActualParameterList, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentActualParameterList(PersistentChildrenCollection children)
            : base(SyntaxKind.ActualParameterList, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentActualParameterList(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentActualParameter : PersistentNonterminal
    {
        public PersistentActualParameter(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.ActualParameter, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentActualParameter(PersistentChildrenCollection children)
            : base(SyntaxKind.ActualParameter, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentActualParameter(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentCompoundStatement : PersistentNonterminal
    {
        public PersistentCompoundStatement(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.CompoundStatement, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentCompoundStatement(PersistentChildrenCollection children)
            : base(SyntaxKind.CompoundStatement, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentCompoundStatement(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentIfStatement : PersistentNonterminal
    {
        public PersistentIfStatement(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.IfStatement, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentIfStatement(PersistentChildrenCollection children)
            : base(SyntaxKind.IfStatement, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentIfStatement(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentVariableSyntax : PersistentNonterminal
    {
        public PersistentVariableSyntax(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.Variable, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentVariableSyntax(PersistentChildrenCollection children)
            : base(SyntaxKind.Variable, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentVariableSyntax(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentDereferencedPointerVariableSyntax : PersistentNonterminal
    {
        public PersistentDereferencedPointerVariableSyntax(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.DereferencedPointerVariable, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentDereferencedPointerVariableSyntax(PersistentChildrenCollection children)
            : base(SyntaxKind.DereferencedPointerVariable, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentDereferencedPointerVariableSyntax(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentRecordFieldVariableSyntax : PersistentNonterminal
    {
        public PersistentRecordFieldVariableSyntax(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.RecordFieldVariable, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentRecordFieldVariableSyntax(PersistentChildrenCollection children)
            : base(SyntaxKind.RecordFieldVariable, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentRecordFieldVariableSyntax(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentArrayVariableSyntax : PersistentNonterminal
    {
        public PersistentArrayVariableSyntax(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.ArrayVariable, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentArrayVariableSyntax(PersistentChildrenCollection children)
            : base(SyntaxKind.ArrayVariable, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentArrayVariableSyntax(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentForLoopStatement : PersistentNonterminal
    {
        public PersistentForLoopStatement(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.ForLoopStatement, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentForLoopStatement(PersistentChildrenCollection children)
            : base(SyntaxKind.ForLoopStatement, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentForLoopStatement(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentRepeatLoopStatement : PersistentNonterminal
    {
        public PersistentRepeatLoopStatement(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.RepeatLoopStatement, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentRepeatLoopStatement(PersistentChildrenCollection children)
            : base(SyntaxKind.RepeatLoopStatement, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentRepeatLoopStatement(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentWhileLoopStatement : PersistentNonterminal
    {
        public PersistentWhileLoopStatement(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.WhileLoopStatement, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentWhileLoopStatement(PersistentChildrenCollection children)
            : base(SyntaxKind.WhileLoopStatement, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentWhileLoopStatement(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentAssemblerStatement : PersistentNonterminal
    {
        public PersistentAssemblerStatement(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.AssemblerStatement, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentAssemblerStatement(PersistentChildrenCollection children)
            : base(SyntaxKind.AssemblerStatement, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentAssemblerStatement(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentWithStatement : PersistentNonterminal
    {
        public PersistentWithStatement(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.WithStatement, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentWithStatement(PersistentChildrenCollection children)
            : base(SyntaxKind.WithStatement, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentWithStatement(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentVariableListSyntax : PersistentNonterminal
    {
        public PersistentVariableListSyntax(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.VariableList, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentVariableListSyntax(PersistentChildrenCollection children)
            : base(SyntaxKind.VariableList, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentVariableListSyntax(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentGotoStatement : PersistentNonterminal
    {
        public PersistentGotoStatement(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.GotoStatement, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentGotoStatement(PersistentChildrenCollection children)
            : base(SyntaxKind.GotoStatement, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentGotoStatement(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentCaseStatement : PersistentNonterminal
    {
        public PersistentCaseStatement(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.CaseStatement, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentCaseStatement(PersistentChildrenCollection children)
            : base(SyntaxKind.CaseStatement, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentCaseStatement(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentCaseList : PersistentNonterminal
    {
        public PersistentCaseList(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.CaseList, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentCaseList(PersistentChildrenCollection children)
            : base(SyntaxKind.CaseList, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentCaseList(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentCaseSyntax : PersistentNonterminal
    {
        public PersistentCaseSyntax(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.Case, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentCaseSyntax(PersistentChildrenCollection children)
            : base(SyntaxKind.Case, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentCaseSyntax(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentOrdinalConstantList : PersistentNonterminal
    {
        public PersistentOrdinalConstantList(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.OrdinalConstantList, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentOrdinalConstantList(PersistentChildrenCollection children)
            : base(SyntaxKind.OrdinalConstantList, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentOrdinalConstantList(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentOrdinalConstant : PersistentNonterminal
    {
        public PersistentOrdinalConstant(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.OrdinalConstant, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentOrdinalConstant(PersistentChildrenCollection children)
            : base(SyntaxKind.OrdinalConstant, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentOrdinalConstant(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentSimpleExpressionSyntax : PersistentNonterminal
    {
        public PersistentSimpleExpressionSyntax(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.SimpleExpression, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentSimpleExpressionSyntax(PersistentChildrenCollection children)
            : base(SyntaxKind.SimpleExpression, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentSimpleExpressionSyntax(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentTermSyntax : PersistentNonterminal
    {
        public PersistentTermSyntax(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.Term, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentTermSyntax(PersistentChildrenCollection children)
            : base(SyntaxKind.Term, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentTermSyntax(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentFactorSyntax : PersistentNonterminal
    {
        public PersistentFactorSyntax(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.Factor, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentFactorSyntax(PersistentChildrenCollection children)
            : base(SyntaxKind.Factor, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentFactorSyntax(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentDeclarationBlock : PersistentNonterminal
    {
        public PersistentDeclarationBlock(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.DeclarationBlock, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentDeclarationBlock(PersistentChildrenCollection children)
            : base(SyntaxKind.DeclarationBlock, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentDeclarationBlock(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentLabelList : PersistentNonterminal
    {
        public PersistentLabelList(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.LabelList, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentLabelList(PersistentChildrenCollection children)
            : base(SyntaxKind.LabelList, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentLabelList(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentLabelDeclaration : PersistentNonterminal
    {
        public PersistentLabelDeclaration(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.LabelDeclaration, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentLabelDeclaration(PersistentChildrenCollection children)
            : base(SyntaxKind.LabelDeclaration, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentLabelDeclaration(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentConstDeclaration : PersistentNonterminal
    {
        public PersistentConstDeclaration(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.ConstantDeclaration, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentConstDeclaration(PersistentChildrenCollection children)
            : base(SyntaxKind.ConstantDeclaration, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentConstDeclaration(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentConstDeclarationLines : PersistentNonterminal
    {
        public PersistentConstDeclarationLines(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.ConstDeclarationLines, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentConstDeclarationLines(PersistentChildrenCollection children)
            : base(SyntaxKind.ConstDeclarationLines, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentConstDeclarationLines(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentConstUntypedDeclarationLine : PersistentNonterminal
    {
        public PersistentConstUntypedDeclarationLine(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.ConstUntypedDeclarationLine, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentConstUntypedDeclarationLine(PersistentChildrenCollection children)
            : base(SyntaxKind.ConstUntypedDeclarationLine, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentConstUntypedDeclarationLine(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentHintDirective : PersistentNonterminal
    {
        public PersistentHintDirective(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.HintDirective, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentHintDirective(PersistentChildrenCollection children)
            : base(SyntaxKind.HintDirective, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentHintDirective(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentVarDeclarationLinesSyntax : PersistentNonterminal
    {
        public PersistentVarDeclarationLinesSyntax(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.VarDeclarationLines, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentVarDeclarationLinesSyntax(PersistentChildrenCollection children)
            : base(SyntaxKind.VarDeclarationLines, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentVarDeclarationLinesSyntax(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentVariableModifiers : PersistentNonterminal
    {
        public PersistentVariableModifiers(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.VariableModifiers, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentVariableModifiers(PersistentChildrenCollection children)
            : base(SyntaxKind.VariableModifiers, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentVariableModifiers(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentExternalModifier : PersistentNonterminal
    {
        public PersistentExternalModifier(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.ExternalModifier, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentExternalModifier(PersistentChildrenCollection children)
            : base(SyntaxKind.ExternalModifier, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentExternalModifier(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentCvarModifier : PersistentNonterminal
    {
        public PersistentCvarModifier(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.CvarModifier, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentCvarModifier(PersistentChildrenCollection children)
            : base(SyntaxKind.CvarModifier, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentCvarModifier(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentExportModifier : PersistentNonterminal
    {
        public PersistentExportModifier(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.ExportModifier, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentExportModifier(PersistentChildrenCollection children)
            : base(SyntaxKind.ExportModifier, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentExportModifier(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentAbsoluteModifier : PersistentNonterminal
    {
        public PersistentAbsoluteModifier(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.AbsoluteModifier, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentAbsoluteModifier(PersistentChildrenCollection children)
            : base(SyntaxKind.AbsoluteModifier, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentAbsoluteModifier(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentVariableInitialization : PersistentNonterminal
    {
        public PersistentVariableInitialization(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.VariableInitialization, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentVariableInitialization(PersistentChildrenCollection children)
            : base(SyntaxKind.VariableInitialization, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentVariableInitialization(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentVariableIdentifierList : PersistentNonterminal
    {
        public PersistentVariableIdentifierList(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.VariableIdentifierList, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentVariableIdentifierList(PersistentChildrenCollection children)
            : base(SyntaxKind.VariableIdentifierList, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentVariableIdentifierList(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentSubrange : PersistentNonterminal
    {
        public PersistentSubrange(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.Subrange, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentSubrange(PersistentChildrenCollection children)
            : base(SyntaxKind.Subrange, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentSubrange(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentSubrangeList : PersistentNonterminal
    {
        public PersistentSubrangeList(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.SubrangeList, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentSubrangeList(PersistentChildrenCollection children)
            : base(SyntaxKind.SubrangeList, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentSubrangeList(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentArrayType : PersistentNonterminal
    {
        public PersistentArrayType(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.ArrayType, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentArrayType(PersistentChildrenCollection children)
            : base(SyntaxKind.ArrayType, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentArrayType(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentRecordType : PersistentNonterminal
    {
        public PersistentRecordType(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.RecordType, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentRecordType(PersistentChildrenCollection children)
            : base(SyntaxKind.RecordType, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentRecordType(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentFieldList : PersistentNonterminal
    {
        public PersistentFieldList(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.FieldList, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentFieldList(PersistentChildrenCollection children)
            : base(SyntaxKind.FieldList, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentFieldList(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentFieldLine : PersistentNonterminal
    {
        public PersistentFieldLine(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.FieldLine, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentFieldLine(PersistentChildrenCollection children)
            : base(SyntaxKind.FieldLine, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentFieldLine(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentPointerType : PersistentNonterminal
    {
        public PersistentPointerType(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.PointerType, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentPointerType(PersistentChildrenCollection children)
            : base(SyntaxKind.PointerType, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentPointerType(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentInitializedEnumMember : PersistentNonterminal
    {
        public PersistentInitializedEnumMember(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.InitializedEnumMember, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentInitializedEnumMember(PersistentChildrenCollection children)
            : base(SyntaxKind.InitializedEnumMember, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentInitializedEnumMember(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentEnumMembers : PersistentNonterminal
    {
        public PersistentEnumMembers(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.EnumMemberList, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentEnumMembers(PersistentChildrenCollection children)
            : base(SyntaxKind.EnumMemberList, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentEnumMembers(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentTypeDeclaration : PersistentNonterminal
    {
        public PersistentTypeDeclaration(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.TypeDeclaration, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentTypeDeclaration(PersistentChildrenCollection children)
            : base(SyntaxKind.TypeDeclaration, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentTypeDeclaration(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentEnumType : PersistentNonterminal
    {
        public PersistentEnumType(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.EnumType, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentEnumType(PersistentChildrenCollection children)
            : base(SyntaxKind.EnumType, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentEnumType(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentTypeDeclarationLines : PersistentNonterminal
    {
        public PersistentTypeDeclarationLines(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.TypeDeclarationLines, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentTypeDeclarationLines(PersistentChildrenCollection children)
            : base(SyntaxKind.TypeDeclarationLines, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentTypeDeclarationLines(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentTypeDeclarationLine : PersistentNonterminal
    {
        public PersistentTypeDeclarationLine(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.TypeDeclarationLine, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentTypeDeclarationLine(PersistentChildrenCollection children)
            : base(SyntaxKind.TypeDeclarationLine, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentTypeDeclarationLine(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentAssemblerBlock : PersistentNonterminal
    {
        public PersistentAssemblerBlock(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.AssemblerBlock, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentAssemblerBlock(PersistentChildrenCollection children)
            : base(SyntaxKind.AssemblerBlock, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentAssemblerBlock(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentExternalDirective : PersistentNonterminal
    {
        public PersistentExternalDirective(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.ExternalDirective, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentExternalDirective(PersistentChildrenCollection children)
            : base(SyntaxKind.ExternalDirective, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentExternalDirective(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentProcedureDeclaration : PersistentNonterminal
    {
        public PersistentProcedureDeclaration(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.ProcedureDeclaration, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentProcedureDeclaration(PersistentChildrenCollection children)
            : base(SyntaxKind.ProcedureDeclaration, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentProcedureDeclaration(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentConstantParameter : PersistentNonterminal
    {
        public PersistentConstantParameter(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.ConstantParameter, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentConstantParameter(PersistentChildrenCollection children)
            : base(SyntaxKind.ConstantParameter, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentConstantParameter(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentOutParameter : PersistentNonterminal
    {
        public PersistentOutParameter(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.OutParameter, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentOutParameter(PersistentChildrenCollection children)
            : base(SyntaxKind.OutParameter, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentOutParameter(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentVariableParameter : PersistentNonterminal
    {
        public PersistentVariableParameter(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.VariableParameter, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentVariableParameter(PersistentChildrenCollection children)
            : base(SyntaxKind.VariableParameter, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentVariableParameter(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentValueParameter : PersistentNonterminal
    {
        public PersistentValueParameter(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.ValueParameter, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentValueParameter(PersistentChildrenCollection children)
            : base(SyntaxKind.ValueParameter, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentValueParameter(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentFormalParameterList : PersistentNonterminal
    {
        public PersistentFormalParameterList(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.FormalParameterList, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentFormalParameterList(PersistentChildrenCollection children)
            : base(SyntaxKind.FormalParameterList, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentFormalParameterList(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentParameterDeclarations : PersistentNonterminal
    {
        public PersistentParameterDeclarations(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.ParameterDeclarations, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentParameterDeclarations(PersistentChildrenCollection children)
            : base(SyntaxKind.ParameterDeclarations, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentParameterDeclarations(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentProcedureHeader : PersistentNonterminal
    {
        public PersistentProcedureHeader(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.ProcedureHeader, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentProcedureHeader(PersistentChildrenCollection children)
            : base(SyntaxKind.ProcedureHeader, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentProcedureHeader(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentFunctionDeclaration : PersistentNonterminal
    {
        public PersistentFunctionDeclaration(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.FunctionDeclaration, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentFunctionDeclaration(PersistentChildrenCollection children)
            : base(SyntaxKind.FunctionDeclaration, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentFunctionDeclaration(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentFunctionHeader : PersistentNonterminal
    {
        public PersistentFunctionHeader(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.FunctionHeader, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentFunctionHeader(PersistentChildrenCollection children)
            : base(SyntaxKind.FunctionHeader, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentFunctionHeader(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentIndexList : PersistentNonterminal
    {
        public PersistentIndexList(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.IndexList, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentIndexList(PersistentChildrenCollection children)
            : base(SyntaxKind.IndexList, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentIndexList(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentUsesBlock : PersistentNonterminal
    {
        public PersistentUsesBlock(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.UsesBlock, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentUsesBlock(PersistentChildrenCollection children)
            : base(SyntaxKind.UsesBlock, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentUsesBlock(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentUnitList : PersistentNonterminal
    {
        public PersistentUnitList(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.UnitList, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentUnitList(PersistentChildrenCollection children)
            : base(SyntaxKind.UnitList, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentUnitList(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentUnitDeclaration : PersistentNonterminal
    {
        public PersistentUnitDeclaration(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.UnitDeclarationLine, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentUnitDeclaration(PersistentChildrenCollection children)
            : base(SyntaxKind.UnitDeclarationLine, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentUnitDeclaration(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentFileType : PersistentNonterminal
    {
        public PersistentFileType(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.FileType, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentFileType(PersistentChildrenCollection children)
            : base(SyntaxKind.FileType, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentFileType(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentSetType : PersistentNonterminal
    {
        public PersistentSetType(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.SetType, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentSetType(PersistentChildrenCollection children)
            : base(SyntaxKind.SetType, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentSetType(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentStringWithFixedSizeType : PersistentNonterminal
    {
        public PersistentStringWithFixedSizeType(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.StringWithFixedSizeType, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentStringWithFixedSizeType(PersistentChildrenCollection children)
            : base(SyntaxKind.StringWithFixedSizeType, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentStringWithFixedSizeType(newChildren, TriviaBefore, newTriviaAfter);
        }
    }

    internal class PersistentStringWithCodePageType : PersistentNonterminal
    {
        public PersistentStringWithCodePageType(PersistentChildrenCollection children, PersistentSyntaxTriviaList triviaBefore, PersistentSyntaxTriviaList triviaAfter)
            : base(SyntaxKind.StringWithCodePageType, children, triviaBefore, triviaAfter)
        {
        }

        public PersistentStringWithCodePageType(PersistentChildrenCollection children)
            : base(SyntaxKind.StringWithCodePageType, children)
        {
        }

        protected override PersistentSyntaxNodeWithTrivia WithTriviaAfter(PersistentChildrenCollection newChildren, PersistentSyntaxTriviaList newTriviaAfter)
        {
            return new PersistentStringWithCodePageType(newChildren, TriviaBefore, newTriviaAfter);
        }
    }
}