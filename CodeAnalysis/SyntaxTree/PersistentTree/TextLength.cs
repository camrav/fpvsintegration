﻿using System.Linq;

namespace CodeAnalysis.SyntaxTree.PersistentTree
{
    internal struct TextLength
    {
        public int Chars { get; private set; }
        public int NewLines { get; private set; }
        public int CharsOnLastLine { get; private set; }

        public TextLength(int chars, int lines, int charsOnLastLine)
        {
            Chars = chars;
            NewLines = lines;
            CharsOnLastLine = charsOnLastLine;
        }

        public TextLength(string text)
        {
            NewLines = text.Count(c => c == '\n');
            Chars = text.Length;
            if (NewLines == 0)
                CharsOnLastLine = Chars;
            else
                CharsOnLastLine = Chars - text.LastIndexOf('\n') - 1;
        }

        public static TextLength operator +(TextLength left, TextLength right)
        {
            if (right.NewLines == 0)
                return new TextLength(left.Chars + right.Chars, left.NewLines + right.NewLines, left.CharsOnLastLine + right.CharsOnLastLine);
            else
                return new TextLength(left.Chars + right.Chars, left.NewLines + right.NewLines, right.CharsOnLastLine);
        }
    }
}