﻿using CodeAnalysis.SyntaxTree.PersistentTree;
using System;
using System.Collections.Generic;

namespace CodeAnalysis.SyntaxTree.OnDemandTree
{
    internal static class FpSyntaxNodeFactory
    {
        private delegate SyntaxNode ChildFactory(ISyntaxTree tree, SyntaxNode parent, PersistentSyntaxNode child, int start);

        private static Dictionary<Type, ChildFactory> factories = new Dictionary<Type, ChildFactory>();

        static FpSyntaxNodeFactory()
        {
            factories.Add(typeof(PersistentKeyword), (tree, parent, child, start) => new FpSyntaxKeyword(tree, parent, child as PersistentKeyword, start));
            factories.Add(typeof(PersistentIdentifier), (tree, parent, child, start) => new FpSyntaxIdentifier(tree, parent, child as PersistentIdentifier, start));
            factories.Add(typeof(PersistentPunctuation), (tree, parent, child, start) => new FpSyntaxPunctuation(tree, parent, child as PersistentPunctuation, start));
            factories.Add(typeof(PersistentProgramRootNode), (tree, parent, child, start) => new FpProgramRootNode(tree, parent, child as PersistentProgramRootNode, start));
            factories.Add(typeof(PersistentBlockSyntax), (tree, parent, child, start) => new FpBlock(tree, parent, child as PersistentBlockSyntax, start));
            factories.Add(typeof(PersistentMissingTerminal), (tree, parent, child, start) => new FpSyntaxTerminal(tree, parent, child as PersistentTerminal, start));
            factories.Add(typeof(PersistentWhitespaceSyntaxTrivia), (tree, parent, child, start) => new WhitespaceTrivia(tree, parent, child as PersistentWhitespaceSyntaxTrivia, start));
            factories.Add(typeof(PersistentCommentSyntaxTrivia), (tree, parent, child, start) => new CommentTrivia(tree, parent, child as PersistentCommentSyntaxTrivia, start));
            factories.Add(typeof(PersistentUnknownSyntaxTrivia), (tree, parent, child, start) => new UnknownTrivia(tree, parent, child as PersistentUnknownSyntaxTrivia, start));
            factories.Add(typeof(PersistentVarSyntax), (tree, parent, child, start) => new FpVarBlock(tree, parent, child as PersistentVarSyntax, start));
            factories.Add(typeof(PersistentVarDeclarationLineSyntax), (tree, parent, child, start) => new FpVarDeclarationLine(tree, parent, child as PersistentVarDeclarationLineSyntax, start));
            factories.Add(typeof(PersistentAssignmentStatementSyntax), (tree, parent, child, start) => new FpAssignmentStatement(tree, parent, child as PersistentAssignmentStatementSyntax, start));
            factories.Add(typeof(PersistentConstant), (tree, parent, child, start) => FpConstant.Create(tree, parent, child as PersistentConstant, start));
            factories.Add(typeof(PersistentConstantIdentifier), (tree, parent, child, start) => new FpConstantIdentifier(tree, parent, child as PersistentConstantIdentifier, start));
            factories.Add(typeof(PersistentStatementSequenceSyntax), (tree, parent, child, start) => new FpStatementSequence(tree, parent, child as PersistentStatementSequenceSyntax, start));
            factories.Add(typeof(PersistentExpressionSyntax), (tree, parent, child, start) => new FpExpression(tree, parent, child as PersistentExpressionSyntax, start));
            factories.Add(typeof(PersistentUnexpectedSyntaxTrivia), (tree, parent, child, start) => new UnexpectedTrivia(tree, parent, child as PersistentUnexpectedSyntaxTrivia, start));
            factories.Add(typeof(PersistentNoStatementSyntax), (tree, parent, child, start) => new FpNoStatement(tree, parent, child as PersistentNoStatementSyntax, start));
            factories.Add(typeof(PersistentActualParameterList), (tree, parent, child, start) => new FpActualParameterList(tree, parent, child as PersistentActualParameterList, start));
            factories.Add(typeof(PersistentCallSyntax), (tree, parent, child, start) => new FpCallStatement(tree, parent, child as PersistentCallSyntax, start));
            factories.Add(typeof(PersistentCompoundStatement), (tree, parent, child, start) => new FpCompoundStatement(tree, parent, child as PersistentCompoundStatement, start));
            factories.Add(typeof(PersistentIfStatement), (tree, parent, child, start) => new FpIfStatement(tree, parent, child as PersistentIfStatement, start));
            factories.Add(typeof(PersistentVariableSyntax), (tree, parent, child, start) => new FpVariable(tree, parent, child as PersistentVariableSyntax, start));
            factories.Add(typeof(PersistentDereferencedPointerVariableSyntax), (tree, parent, child, start) => new FpDereferencedPointerVariable(tree, parent, child as PersistentDereferencedPointerVariableSyntax, start));
            factories.Add(typeof(PersistentRecordFieldVariableSyntax), (tree, parent, child, start) => new FpRecordFieldVariable(tree, parent, child as PersistentRecordFieldVariableSyntax, start));
            factories.Add(typeof(PersistentArrayVariableSyntax), (tree, parent, child, start) => new FpArrayVariable(tree, parent, child as PersistentArrayVariableSyntax, start));
            factories.Add(typeof(PersistentForLoopStatement), (tree, parent, child, start) => new FpForLoopStatement(tree, parent, child as PersistentForLoopStatement, start));
            factories.Add(typeof(PersistentRepeatLoopStatement), (tree, parent, child, start) => new FpRepeatLoopStatement(tree, parent, child as PersistentRepeatLoopStatement, start));
            factories.Add(typeof(PersistentWhileLoopStatement), (tree, parent, child, start) => new FpWhileLoopStatement(tree, parent, child as PersistentWhileLoopStatement, start));
            factories.Add(typeof(PersistentAssemblerTrivia), (tree, parent, child, start) => new AssemblerTrivia(tree, parent, child as PersistentAssemblerTrivia, start));
            factories.Add(typeof(PersistentAssemblerStatement), (tree, parent, child, start) => new FpAssemblerStatement(tree, parent, child as PersistentAssemblerStatement, start));
            factories.Add(typeof(PersistentWithStatement), (tree, parent, child, start) => new FpWithStatement(tree, parent, child as PersistentWithStatement, start));
            factories.Add(typeof(PersistentVariableListSyntax), (tree, parent, child, start) => new FpVariableList(tree, parent, child as PersistentVariableListSyntax, start));
            factories.Add(typeof(PersistentGotoStatement), (tree, parent, child, start) => new FpGotoStatement(tree, parent, child as PersistentGotoStatement, start));
            factories.Add(typeof(PersistentCaseStatement), (tree, parent, child, start) => new FpCaseStatement(tree, parent, child as PersistentCaseStatement, start));
            factories.Add(typeof(PersistentCaseSyntax), (tree, parent, child, start) => new FpCase(tree, parent, child as PersistentCaseSyntax, start));
            factories.Add(typeof(PersistentOrdinalConstantList), (tree, parent, child, start) => new FpOrdinalConstantList(tree, parent, child as PersistentOrdinalConstantList, start));
            factories.Add(typeof(PersistentOrdinalConstant), (tree, parent, child, start) => new FpOrdinalConstant(tree, parent, child as PersistentOrdinalConstant, start));
            factories.Add(typeof(PersistentCaseList), (tree, parent, child, start) => new FpCaseList(tree, parent, child as PersistentCaseList, start));
            factories.Add(typeof(PersistentSimpleExpressionSyntax), (tree, parent, child, start) => new FpSimpleExpression(tree, parent, child as PersistentSimpleExpressionSyntax, start));
            factories.Add(typeof(PersistentTermSyntax), (tree, parent, child, start) => new FpTerm(tree, parent, child as PersistentTermSyntax, start));
            factories.Add(typeof(PersistentFactorSyntax), (tree, parent, child, start) => new FpFactor(tree, parent, child as PersistentFactorSyntax, start));
            factories.Add(typeof(PersistentDeclarationBlock), (tree, parent, child, start) => new FpDeclarationBlock(tree, parent, child as PersistentDeclarationBlock, start));
            factories.Add(typeof(PersistentLabelDeclaration), (tree, parent, child, start) => new FpLabelDeclaration(tree, parent, child as PersistentLabelDeclaration, start));
            factories.Add(typeof(PersistentLabelList), (tree, parent, child, start) => new FpLabelList(tree, parent, child as PersistentLabelList, start));
            factories.Add(typeof(PersistentConstDeclaration), (tree, parent, child, start) => new FpConstantDeclaration(tree, parent, child as PersistentConstDeclaration, start));
            factories.Add(typeof(PersistentConstDeclarationLines), (tree, parent, child, start) => new FpConstantDeclarationLines(tree, parent, child as PersistentConstDeclarationLines, start));
            factories.Add(typeof(PersistentConstUntypedDeclarationLine), (tree, parent, child, start) => new FpConstantUntypedDeclarationLine(tree, parent, child as PersistentConstUntypedDeclarationLine, start));
            factories.Add(typeof(PersistentHintDirective), (tree, parent, child, start) => new FpHintDirective(tree, parent, child as PersistentHintDirective, start));
            factories.Add(typeof(PersistentVarDeclarationLinesSyntax), (tree, parent, child, start) => new FpVarDeclarationLines(tree, parent, child as PersistentVarDeclarationLinesSyntax, start));
            factories.Add(typeof(PersistentVariableModifiers), (tree, parent, child, start) => new FpVariableModifiers(tree, parent, child as PersistentVariableModifiers, start));
            factories.Add(typeof(PersistentExternalModifier), (tree, parent, child, start) => new FpExternalModifier(tree, parent, child as PersistentExternalModifier, start));
            factories.Add(typeof(PersistentCvarModifier), (tree, parent, child, start) => new FpCvarModifier(tree, parent, child as PersistentCvarModifier, start));
            factories.Add(typeof(PersistentExportModifier), (tree, parent, child, start) => new FpExportModifier(tree, parent, child as PersistentExportModifier, start));
            factories.Add(typeof(PersistentAbsoluteModifier), (tree, parent, child, start) => new FpAbsoluteModifier(tree, parent, child as PersistentAbsoluteModifier, start));
            factories.Add(typeof(PersistentVariableInitialization), (tree, parent, child, start) => new FpVariableInitialisation(tree, parent, child as PersistentVariableInitialization, start));
            factories.Add(typeof(PersistentVariableIdentifierList), (tree, parent, child, start) => new FpVariableIdentifierList(tree, parent, child as PersistentVariableIdentifierList, start));
            factories.Add(typeof(PersistentSubrange), (tree, parent, child, start) => new FpSubrange(tree, parent, child as PersistentSubrange, start));
            factories.Add(typeof(PersistentSubrangeList), (tree, parent, child, start) => new FpSubrangeList(tree, parent, child as PersistentSubrangeList, start));
            factories.Add(typeof(PersistentArrayType), (tree, parent, child, start) => new FpArrayType(tree, parent, child as PersistentArrayType, start));
            factories.Add(typeof(PersistentRecordType), (tree, parent, child, start) => new FpRecordType(tree, parent, child as PersistentRecordType, start));
            factories.Add(typeof(PersistentFieldList), (tree, parent, child, start) => new FpFieldList(tree, parent, child as PersistentFieldList, start));
            factories.Add(typeof(PersistentFieldLine), (tree, parent, child, start) => new FpFieldLine(tree, parent, child as PersistentFieldLine, start));
            factories.Add(typeof(PersistentPointerType), (tree, parent, child, start) => new FpPointerType(tree, parent, child as PersistentPointerType, start));
            factories.Add(typeof(PersistentEnumType), (tree, parent, child, start) => new FpEnumType(tree, parent, child as PersistentEnumType, start));
            factories.Add(typeof(PersistentEnumMembers), (tree, parent, child, start) => new FpEnumMembers(tree, parent, child as PersistentEnumMembers, start));
            factories.Add(typeof(PersistentInitializedEnumMember), (tree, parent, child, start) => new FpInitializedEnumMember(tree, parent, child as PersistentInitializedEnumMember, start));
            factories.Add(typeof(PersistentTypeDeclaration), (tree, parent, child, start) => new FpTypeDeclaration(tree, parent, child as PersistentTypeDeclaration, start));
            factories.Add(typeof(PersistentTypeDeclarationLines), (tree, parent, child, start) => new FpTypeDeclarationLines(tree, parent, child as PersistentTypeDeclarationLines, start));
            factories.Add(typeof(PersistentTypeDeclarationLine), (tree, parent, child, start) => new FpTypeDeclarationLine(tree, parent, child as PersistentTypeDeclarationLine, start));
            factories.Add(typeof(PersistentAssemblerBlock), (tree, parent, child, start) => new FpAssemblerBlock(tree, parent, child as PersistentAssemblerBlock, start));
            factories.Add(typeof(PersistentExternalDirective), (tree, parent, child, start) => new FpExternalDirective(tree, parent, child as PersistentExternalDirective, start));
            factories.Add(typeof(PersistentProcedureDeclaration), (tree, parent, child, start) => new FpProcedureDeclaration(tree, parent, child as PersistentProcedureDeclaration, start));
            factories.Add(typeof(PersistentConstantParameter), (tree, parent, child, start) => new FpConstParameter(tree, parent, child as PersistentConstantParameter, start));
            factories.Add(typeof(PersistentOutParameter), (tree, parent, child, start) => new FpOutParameter(tree, parent, child as PersistentOutParameter, start));
            factories.Add(typeof(PersistentVariableParameter), (tree, parent, child, start) => new FpVarParameter(tree, parent, child as PersistentVariableParameter, start));
            factories.Add(typeof(PersistentValueParameter), (tree, parent, child, start) => new FpValueParameter(tree, parent, child as PersistentValueParameter, start));
            factories.Add(typeof(PersistentFormalParameterList), (tree, parent, child, start) => new FpFormalParameterList(tree, parent, child as PersistentFormalParameterList, start));
            factories.Add(typeof(PersistentParameterDeclarations), (tree, parent, child, start) => new FpParameterDeclarations(tree, parent, child as PersistentParameterDeclarations, start));
            factories.Add(typeof(PersistentProcedureHeader), (tree, parent, child, start) => new FpProcedureHeader(tree, parent, child as PersistentProcedureHeader, start));
            factories.Add(typeof(PersistentFunctionDeclaration), (tree, parent, child, start) => new FpFunctionDeclaration(tree, parent, child as PersistentFunctionDeclaration, start));
            factories.Add(typeof(PersistentFunctionHeader), (tree, parent, child, start) => new FpFunctionHeader(tree, parent, child as PersistentFunctionHeader, start));
            factories.Add(typeof(PersistentIndexList), (tree, parent, child, start) => new FpIndexList(tree, parent, child as PersistentIndexList, start));
            factories.Add(typeof(PersistentUsesBlock), (tree, parent, child, start) => new FpUsesBlock(tree, parent, child as PersistentUsesBlock, start));
            factories.Add(typeof(PersistentUnitList), (tree, parent, child, start) => new FpUnitList(tree, parent, child as PersistentUnitList, start));
            factories.Add(typeof(PersistentUnitDeclaration), (tree, parent, child, start) => new FpUnitDeclaration(tree, parent, child as PersistentUnitDeclaration, start));
            factories.Add(typeof(PersistentFileType), (tree, parent, child, start) => new FpFileType(tree, parent, child as PersistentFileType, start));
            factories.Add(typeof(PersistentSetType), (tree, parent, child, start) => new FpSetType(tree, parent, child as PersistentSetType, start));
            factories.Add(typeof(PersistentStringWithFixedSizeType), (tree, parent, child, start) => new FpStringWithFixedSize(tree, parent, child as PersistentStringWithFixedSizeType, start));
            factories.Add(typeof(PersistentStringWithCodePageType), (tree, parent, child, start) => new FpStringWithCodePageType(tree, parent, child as PersistentStringWithCodePageType, start));
            factories.Add(typeof(PersistentActualParameter), (tree, parent, child, start) => new FpActualParameter(tree, parent, child as PersistentActualParameter, start));
        }

        public static SyntaxNode GetChild(ISyntaxTree tree, SyntaxNode parent, PersistentSyntaxNode child, int start)
        {
            return factories[child.GetType()](tree, parent, child, start);
        }
    }
}