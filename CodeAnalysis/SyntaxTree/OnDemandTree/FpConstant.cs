﻿using CodeAnalysis.SyntaxTree.PersistentTree;
using FpLexer;
using System;

namespace CodeAnalysis.SyntaxTree.OnDemandTree
{
    public abstract class FpConstant : FpSyntaxTerminal
    {
        internal FpConstant(ISyntaxTree tree, SyntaxNode parent, PersistentConstant innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }

        internal static FpConstant Create(ISyntaxTree tree, SyntaxNode parent, PersistentConstant innerNode, int start)
        {
            switch (innerNode.Kind)
            {
                case SyntaxKind.StringConstant:
                    return new FpStringConstant(tree, parent, innerNode, start);

                case SyntaxKind.UintConstant:
                    return new FpUintConstant(tree, parent, innerNode, start);

                case SyntaxKind.NilConstant:
                    return new FpNilConstant(tree, parent, innerNode, start);

                case SyntaxKind.RealConstant:
                    return new FpRealConstant(tree, parent, innerNode, start);
            }

            throw new Exception("Unsupported constant type");
        }
    }

    public class FpStringConstant : FpConstant
    {
        internal FpStringConstant(ISyntaxTree tree, SyntaxNode parent, PersistentConstant innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpUintConstant : FpConstant
    {
        internal FpUintConstant(ISyntaxTree tree, SyntaxNode parent, PersistentConstant innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpNilConstant : FpConstant
    {
        internal FpNilConstant(ISyntaxTree tree, SyntaxNode parent, PersistentConstant innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpRealConstant : FpConstant
    {
        internal FpRealConstant(ISyntaxTree tree, SyntaxNode parent, PersistentConstant innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpConstantIdentifier : FpSyntaxTerminal
    {
        internal FpConstantIdentifier(ISyntaxTree tree, SyntaxNode parent, PersistentConstantIdentifier innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }
}