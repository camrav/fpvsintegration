﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace CodeAnalysis.SyntaxTree.OnDemandTree
{
    internal class LazyChildrenCollection : IReadOnlyList<SyntaxNode>
    {
        private List<Lazy<SyntaxNode>> nodeInitializers;

        public LazyChildrenCollection(IEnumerable<Lazy<SyntaxNode>> initializers)
        {
            nodeInitializers = new List<Lazy<SyntaxNode>>(initializers);
        }

        public SyntaxNode this[int index]
        {
            get
            {
                return nodeInitializers[index].Value;
            }
        }

        public int Count => nodeInitializers.Count;

        public IEnumerator<SyntaxNode> GetEnumerator()
        {
            foreach (var item in nodeInitializers)
            {
                yield return item.Value;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}