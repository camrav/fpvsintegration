﻿using CodeAnalysis.Semantics;
using CodeAnalysis.SyntaxTree.PersistentTree;
using FpLexer;
using System;

namespace CodeAnalysis.SyntaxTree.OnDemandTree
{
    public class FpSyntaxTree : ISyntaxTree
    {
        public SyntaxNode Root => lazyRoot.Value;
        public SemanticModel SemanticModel => lazyModel.Value;

        private Lazy<SemanticModel> lazyModel;
        private PersistentSyntaxTree InnerTree;
        private Lazy<SyntaxNode> lazyRoot;

        internal FpSyntaxTree(PersistentSyntaxTree innerTree, UnitManager manager)
        {
            InnerTree = innerTree;
            lazyRoot = new Lazy<SyntaxNode>(() => new FpProgramRootNode(this, null, InnerTree.Root as PersistentProgramRootNode, GetStartPosition(InnerTree.Root)));
            lazyModel = new Lazy<SemanticModel>(() => new SemanticModel(this, manager));
        }

        public static FpSyntaxTree Parse(ISourceText source, UnitManager manager)
        {
            PersistentSyntaxTree tree = PersistentSyntaxTree.Parse(source);
            return new FpSyntaxTree(tree, manager);
        }

        private int GetStartPosition(PersistentSyntaxNode node)
        {
            if (node is PersistentSyntaxNodeWithTrivia)
            {
                var triviaNode = node as PersistentSyntaxNodeWithTrivia;
                return triviaNode.TriviaBefore.Length;
            }
            else
                return 0;
        }
    }
}