﻿using CodeAnalysis.SyntaxTree.PersistentTree;
using FpLexer;
using System;
using System.Collections.Generic;

namespace CodeAnalysis.SyntaxTree.OnDemandTree
{
    public abstract class FpSyntaxNode : SyntaxNode
    {
        public override ISyntaxTree Tree { get; }
        public override SyntaxNode Parent { get; }
        public override int Start { get; }
        public override int Length => InnerNode.Length;
        public override IReadOnlyList<SyntaxNode> Children { get; }
        public SyntaxKind Kind => InnerNode.Kind;

        internal PersistentSyntaxNode InnerNode { get; }

        private SyntaxTriviaList TriviaBefore => triviaBefore.Value;
        private SyntaxTriviaList TriviaAfter => triviaAfter.Value;

        public override TextSpan SpanWithTrivia { get; }

        private Lazy<SyntaxTriviaList> triviaBefore;
        private Lazy<SyntaxTriviaList> triviaAfter;

        public override bool IsMissing => InnerNode.IsMissing;

        internal FpSyntaxNode(ISyntaxTree tree, SyntaxNode parent, PersistentSyntaxNode innerNode, int start)
        {
            InnerNode = innerNode;
            Tree = tree;
            Parent = parent;
            Start = start;
            Children = new LazyChildrenCollection(ChildrenInitialisation());

            if (innerNode is PersistentSyntaxNodeWithTrivia)
            {
                var triviaNode = innerNode as PersistentSyntaxNodeWithTrivia;
                triviaBefore = new Lazy<SyntaxTriviaList>(() => GetTrivia(start - triviaNode.TriviaBefore.Length, triviaNode.TriviaBefore));
                triviaAfter = new Lazy<SyntaxTriviaList>(() => GetTrivia(End, triviaNode.TriviaAfter));
                SpanWithTrivia = new TextSpan(start - triviaNode.TriviaBefore.Length, triviaNode.TriviaBefore.Length + innerNode.Length + triviaNode.TriviaAfter.Length);
            }
            else
            {
                triviaBefore = new Lazy<SyntaxTriviaList>(() => new SyntaxTriviaList(start));
                triviaAfter = new Lazy<SyntaxTriviaList>(() => new SyntaxTriviaList(End));
                SpanWithTrivia = Span;
            }

            //SpanWithTrivia = new TextSpan(TriviaBefore.Start, TriviaAfter.Start + TriviaAfter.LengthInCharacters - TriviaBefore.Start);
        }

        private SyntaxTriviaList GetTrivia(int start, PersistentSyntaxTriviaList list)
        {
            return new SyntaxTriviaList(start, GetSyntaxTrivia(start, list));
        }

        private IEnumerable<FpSyntaxNode> GetSyntaxTrivia(int start, IEnumerable<PersistentSyntaxTrivia> childrenTrivia)
        {
            foreach (var item in childrenTrivia)
            {
                var result = (FpSyntaxNode)FpSyntaxNodeFactory.GetChild(Tree, this, item, start);
                start += result.Length;
                yield return result;
            }
        }

        protected virtual IEnumerable<Lazy<SyntaxNode>> ChildrenInitialisation()
        {
            int offset = 0;
            foreach (var child in InnerNode.Children)
            {
                int start = Start + offset;
                yield return new Lazy<SyntaxNode>(() => FpSyntaxNodeFactory.GetChild(Tree, this, child, start));
                offset += child.Length;
                if (child is PersistentSyntaxNodeWithTrivia)
                    offset += (child as PersistentSyntaxNodeWithTrivia).TriviaAfter.Length;
            }
        }

        public override SyntaxTriviaList GetTriviaBefore()
        {
            return TriviaBefore;
        }

        public override SyntaxTriviaList GetTriviaAfter()
        {
            return TriviaAfter;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is FpSyntaxNode))
                return false;

            return ((FpSyntaxNode)obj).InnerNode == InnerNode;
        }

        public override int GetHashCode()
        {
            return InnerNode.GetHashCode();
        }

        public override string ToString()
        {
            return Kind.ToString();
        }

        protected IEnumerable<T> GetChildrenOfType<T>(int start = 0, int step = 1) where T : FpSyntaxNode
        {
            for (int i = start; i < Children.Count; i += step)
            {
                T child = Children[i] as T;
                if (child != null) yield return child;
            }
        }
    }
}