﻿using CodeAnalysis.SyntaxTree.PersistentTree;

namespace CodeAnalysis.SyntaxTree.OnDemandTree
{
    public class FpSyntaxTerminal : FpSyntaxNode
    {
        public override bool IsMissing => InnerNode.IsMissing;

        public string Text => InnerNode.GetText();

        internal FpSyntaxTerminal(ISyntaxTree tree, SyntaxNode parent, PersistentSyntaxNode innerNode, int start) :
            base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpSyntaxPunctuation : FpSyntaxTerminal
    {
        internal FpSyntaxPunctuation(ISyntaxTree tree, SyntaxNode parent, PersistentPunctuation innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpSyntaxKeyword : FpSyntaxTerminal
    {
        internal FpSyntaxKeyword(ISyntaxTree tree, SyntaxNode parent, PersistentKeyword innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpSyntaxIdentifier : FpSyntaxTerminal
    {
        public string Identifier => Text;

        internal FpSyntaxIdentifier(ISyntaxTree tree, SyntaxNode parent, PersistentIdentifier innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }
}