﻿using CodeAnalysis.SyntaxTree.PersistentTree;
using System.Collections.Generic;
using System.Linq;

namespace CodeAnalysis.SyntaxTree.OnDemandTree
{
    public class FpProgramRootNode : FpSyntaxNode
    {
        public FpSyntaxTerminal ProgramKeyword => Children[0] as FpSyntaxTerminal;
        public FpSyntaxTerminal ProgramName => Children[1] as FpSyntaxTerminal;
        public FpSyntaxTerminal Semicolon => Children[2] as FpSyntaxTerminal;
        public FpUsesBlock UsesBlock => Children[3] as FpUsesBlock;
        public FpBlock Block => Children[4] as FpBlock;
        public FpSyntaxTerminal Dot => Children[5] as FpSyntaxTerminal;

        internal FpProgramRootNode(ISyntaxTree tree, SyntaxNode parent, PersistentProgramRootNode innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpVarBlock : FpSyntaxNode
    {
        public FpSyntaxKeyword VarToken => Children[0] as FpSyntaxKeyword;
        public FpVarDeclarationLines DeclarationLines => Children[1] as FpVarDeclarationLines;

        internal FpVarBlock(ISyntaxTree tree, SyntaxNode parent, PersistentVarSyntax innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpVarDeclarationLine : FpSyntaxNode
    {
        public FpVariableIdentifierList IdentifierList => Children[0] as FpVariableIdentifierList;
        public FpSyntaxPunctuation Colon => Children[1] as FpSyntaxPunctuation;
        public FpSyntaxNode Type => Children[2] as FpSyntaxNode;

        internal FpVarDeclarationLine(ISyntaxTree tree, SyntaxNode parent, PersistentVarDeclarationLineSyntax innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpBlock : FpSyntaxNode
    {
        public FpDeclarationBlock Declarations => Children[0] as FpDeclarationBlock;
        public FpCompoundStatement Body => Children[1] as FpCompoundStatement;

        internal FpBlock(ISyntaxTree tree, SyntaxNode parent, PersistentBlockSyntax innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpAssignmentStatement : FpSyntaxNode
    {
        internal FpAssignmentStatement(ISyntaxTree tree, SyntaxNode parent, PersistentSyntaxNodeWithTrivia innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpStatementSequence : FpSyntaxNode
    {
        internal FpStatementSequence(ISyntaxTree tree, SyntaxNode parent, PersistentSyntaxNodeWithTrivia innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpExpression : FpSyntaxNode
    {
        public FpSimpleExpression LeftExpression => Children[0] as FpSimpleExpression;
        public bool HasRightSide => Children.Count > 1;
        public FpSyntaxTerminal Operator => Children[1] as FpSyntaxTerminal;
        public FpSimpleExpression RightExpression => Children[2] as FpSimpleExpression;

        internal FpExpression(ISyntaxTree tree, SyntaxNode parent, PersistentSyntaxNodeWithTrivia innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpNoStatement : FpSyntaxNode
    {
        internal FpNoStatement(ISyntaxTree tree, SyntaxNode parent, PersistentNoStatementSyntax innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpCallStatement : FpSyntaxNode
    {
        internal FpCallStatement(ISyntaxTree tree, SyntaxNode parent, PersistentCallSyntax innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpActualParameterList : FpSyntaxNode
    {
        internal FpActualParameterList(ISyntaxTree tree, SyntaxNode parent, PersistentActualParameterList innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpActualParameter : FpSyntaxNode
    {
        internal FpActualParameter(ISyntaxTree tree, SyntaxNode parent, PersistentActualParameter innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpCompoundStatement : FpSyntaxNode
    {
        internal FpCompoundStatement(ISyntaxTree tree, SyntaxNode parent, PersistentCompoundStatement innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpIfStatement : FpSyntaxNode
    {
        internal FpIfStatement(ISyntaxTree tree, SyntaxNode parent, PersistentIfStatement innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpVariable : FpSyntaxNode
    {
        internal FpVariable(ISyntaxTree tree, SyntaxNode parent, PersistentVariableSyntax innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpDereferencedPointerVariable : FpSyntaxNode
    {
        public FpSyntaxNode Variable => Children[0] as FpSyntaxNode;
        public FpSyntaxTerminal RoofToken => Children[1] as FpSyntaxTerminal;

        internal FpDereferencedPointerVariable(ISyntaxTree tree, SyntaxNode parent, PersistentDereferencedPointerVariableSyntax innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpRecordFieldVariable : FpSyntaxNode
    {
        public FpSyntaxNode Variable => Children[0] as FpSyntaxNode;
        public FpSyntaxTerminal DotToken => Children[1] as FpSyntaxTerminal;
        public FpSyntaxIdentifier FieldName => Children[2] as FpSyntaxIdentifier;

        internal FpRecordFieldVariable(ISyntaxTree tree, SyntaxNode parent, PersistentRecordFieldVariableSyntax innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpArrayVariable : FpSyntaxNode
    {
        public FpSyntaxNode Variable => Children[0] as FpSyntaxNode;
        public FpSyntaxTerminal OpeningBracket => Children[1] as FpSyntaxTerminal;
        public FpIndexList IndexList => Children[2] as FpIndexList;
        public FpSyntaxTerminal ClosingBracket => Children[2] as FpSyntaxTerminal;

        internal FpArrayVariable(ISyntaxTree tree, SyntaxNode parent, PersistentArrayVariableSyntax innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpForLoopStatement : FpSyntaxNode
    {
        internal FpForLoopStatement(ISyntaxTree tree, SyntaxNode parent, PersistentForLoopStatement innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpRepeatLoopStatement : FpSyntaxNode
    {
        internal FpRepeatLoopStatement(ISyntaxTree tree, SyntaxNode parent, PersistentRepeatLoopStatement innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpWhileLoopStatement : FpSyntaxNode
    {
        internal FpWhileLoopStatement(ISyntaxTree tree, SyntaxNode parent, PersistentWhileLoopStatement innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpAssemblerStatement : FpSyntaxNode
    {
        internal FpAssemblerStatement(ISyntaxTree tree, SyntaxNode parent, PersistentAssemblerStatement innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpWithStatement : FpSyntaxNode
    {
        internal FpWithStatement(ISyntaxTree tree, SyntaxNode parent, PersistentWithStatement innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpVariableList : FpSyntaxNode
    {
        internal FpVariableList(ISyntaxTree tree, SyntaxNode parent, PersistentVariableListSyntax innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpGotoStatement : FpSyntaxNode
    {
        internal FpGotoStatement(ISyntaxTree tree, SyntaxNode parent, PersistentGotoStatement innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpCaseStatement : FpSyntaxNode
    {
        internal FpCaseStatement(ISyntaxTree tree, SyntaxNode parent, PersistentCaseStatement innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpCase : FpSyntaxNode
    {
        internal FpCase(ISyntaxTree tree, SyntaxNode parent, PersistentCaseSyntax innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpOrdinalConstantList : FpSyntaxNode
    {
        internal FpOrdinalConstantList(ISyntaxTree tree, SyntaxNode parent, PersistentOrdinalConstantList innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpOrdinalConstant : FpSyntaxNode
    {
        internal FpOrdinalConstant(ISyntaxTree tree, SyntaxNode parent, PersistentOrdinalConstant innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpCaseList : FpSyntaxNode
    {
        internal FpCaseList(ISyntaxTree tree, SyntaxNode parent, PersistentCaseList innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpSimpleExpression : FpSyntaxNode
    {
        public IEnumerable<FpTerm> Terms => GetChildrenOfType<FpTerm>(0, 2);
        public IEnumerable<FpSyntaxTerminal> Operators => GetChildrenOfType<FpSyntaxTerminal>(1, 2);

        internal FpSimpleExpression(ISyntaxTree tree, SyntaxNode parent, PersistentSimpleExpressionSyntax innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpTerm : FpSyntaxNode
    {
        public IEnumerable<FpFactor> Factors => GetChildrenOfType<FpFactor>(0, 2);

        public IEnumerable<FpSyntaxTerminal> Operators => GetChildrenOfType<FpSyntaxTerminal>(1, 2);

        internal FpTerm(ISyntaxTree tree, SyntaxNode parent, PersistentTermSyntax innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpFactor : FpSyntaxNode
    {
        public bool ContainsUnaryOperator => Children.Count == 2;
        public FpSyntaxTerminal UnaryOperator => Children[0] as FpSyntaxTerminal;

        /// <summary>
        /// Only applicable when ContainsUnaryOperator is true
        /// </summary>
        public FpFactor Operand => ContainsUnaryOperator ? Children[1] as FpFactor : null;

        public FpSyntaxTerminal Terminal => Children[0] as FpSyntaxTerminal;

        public bool ContainsParanthesis => Children.Count == 3;
        public FpExpression Expression => ContainsParanthesis ? Children[1] as FpExpression : null;

        internal FpFactor(ISyntaxTree tree, SyntaxNode parent, PersistentFactorSyntax innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpDeclarationBlock : FpSyntaxNode
    {
        internal FpDeclarationBlock(ISyntaxTree tree, SyntaxNode parent, PersistentDeclarationBlock innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpLabelDeclaration : FpSyntaxNode
    {
        internal FpLabelDeclaration(ISyntaxTree tree, SyntaxNode parent, PersistentLabelDeclaration innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpLabelList : FpSyntaxNode
    {
        internal FpLabelList(ISyntaxTree tree, SyntaxNode parent, PersistentLabelList innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpConstantDeclaration : FpSyntaxNode
    {
        public FpSyntaxKeyword ConstToken => Children[0] as FpSyntaxKeyword;
        public FpConstantDeclarationLines Declarations => Children[1] as FpConstantDeclarationLines;

        internal FpConstantDeclaration(ISyntaxTree tree, SyntaxNode parent, PersistentConstDeclaration innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpConstantDeclarationLines : FpSyntaxNode
    {
        public IEnumerable<FpConstantUntypedDeclarationLine> UntypedLines => Children.OfType<FpConstantUntypedDeclarationLine>();

        internal FpConstantDeclarationLines(ISyntaxTree tree, SyntaxNode parent, PersistentConstDeclarationLines innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpConstantUntypedDeclarationLine : FpSyntaxNode
    {
        public FpSyntaxIdentifier ConstantName => Children[0] as FpSyntaxIdentifier;
        public FpSyntaxPunctuation EqualsToken => Children[1] as FpSyntaxPunctuation;
        public FpExpression Value => Children[2] as FpExpression;
        public FpHintDirective Hints => Children[3] as FpHintDirective;
        public FpSyntaxPunctuation SemicolonToken => Children[4] as FpSyntaxPunctuation;

        internal FpConstantUntypedDeclarationLine(ISyntaxTree tree, SyntaxNode parent, PersistentConstUntypedDeclarationLine innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpHintDirective : FpSyntaxNode
    {
        internal FpHintDirective(ISyntaxTree tree, SyntaxNode parent, PersistentHintDirective innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpVarDeclarationLines : FpSyntaxNode
    {
        public IEnumerable<FpVarDeclarationLine> Lines => Children.OfType<FpVarDeclarationLine>();

        internal FpVarDeclarationLines(ISyntaxTree tree, SyntaxNode parent, PersistentVarDeclarationLinesSyntax innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpVariableModifiers : FpSyntaxNode
    {
        internal FpVariableModifiers(ISyntaxTree tree, SyntaxNode parent, PersistentVariableModifiers innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpExternalModifier : FpSyntaxNode
    {
        internal FpExternalModifier(ISyntaxTree tree, SyntaxNode parent, PersistentExternalModifier innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpCvarModifier : FpSyntaxNode
    {
        internal FpCvarModifier(ISyntaxTree tree, SyntaxNode parent, PersistentCvarModifier innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpExportModifier : FpSyntaxNode
    {
        internal FpExportModifier(ISyntaxTree tree, SyntaxNode parent, PersistentExportModifier innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpAbsoluteModifier : FpSyntaxNode
    {
        internal FpAbsoluteModifier(ISyntaxTree tree, SyntaxNode parent, PersistentAbsoluteModifier innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpVariableInitialisation : FpSyntaxNode
    {
        internal FpVariableInitialisation(ISyntaxTree tree, SyntaxNode parent, PersistentVariableInitialization innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpVariableIdentifierList : FpSyntaxNode
    {
        public IEnumerable<FpSyntaxIdentifier> Identifiers => GetChildrenOfType<FpSyntaxIdentifier>(0, 2);

        public IEnumerable<FpSyntaxPunctuation> Commas => GetChildrenOfType<FpSyntaxPunctuation>(1, 2);

        internal FpVariableIdentifierList(ISyntaxTree tree, SyntaxNode parent, PersistentVariableIdentifierList innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpSubrange : FpSyntaxNode
    {
        public FpSyntaxTerminal LowerBound => Children[0] as FpSyntaxTerminal;
        public FpSyntaxPunctuation DotDotToken => Children[1] as FpSyntaxPunctuation;
        public FpSyntaxTerminal UpperBound => Children[2] as FpSyntaxTerminal;

        internal FpSubrange(ISyntaxTree tree, SyntaxNode parent, PersistentSubrange innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpSubrangeList : FpSyntaxNode
    {
        public IEnumerable<FpSubrange> Subranges => GetChildrenOfType<FpSubrange>(0, 2);

        public IEnumerable<FpSyntaxPunctuation> Commas => GetChildrenOfType<FpSyntaxPunctuation>(1, 2);

        internal FpSubrangeList(ISyntaxTree tree, SyntaxNode parent, PersistentSubrangeList innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpArrayType : FpSyntaxNode
    {
        public FpSyntaxTerminal ArrayToken => Children[0] as FpSyntaxTerminal;
        public bool ContainsRange => Children.Count == 6;

        public FpSyntaxTerminal OpenBracketToken => ContainsRange ? Children[1] as FpSyntaxTerminal : null;
        public FpSubrangeList Ranges => ContainsRange ? Children[2] as FpSubrangeList : null;
        public FpSyntaxTerminal CloseBracketToken => ContainsRange ? Children[3] as FpSyntaxTerminal : null;
        public FpSyntaxTerminal OfToken => (ContainsRange ? Children[4] : Children[1]) as FpSyntaxTerminal;
        public FpSyntaxNode Type => (ContainsRange ? Children[5] : Children[2]) as FpSyntaxNode;

        internal FpArrayType(ISyntaxTree tree, SyntaxNode parent, PersistentArrayType innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpRecordType : FpSyntaxNode
    {
        public FpSyntaxTerminal RecordToken => Children[0] as FpSyntaxTerminal;
        public FpFieldList FieldList => Children[1] as FpFieldList;
        public FpSyntaxTerminal Semicolon => Children.Count == 3 ? null : Children[2] as FpSyntaxTerminal;
        public FpSyntaxTerminal EndToken => (Children.Count == 3 ? Children[2] : Children[3]) as FpSyntaxTerminal;

        internal FpRecordType(ISyntaxTree tree, SyntaxNode parent, PersistentRecordType innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpFieldList : FpSyntaxNode
    {
        public IEnumerable<FpFieldLine> FieldLines => GetChildrenOfType<FpFieldLine>(0, 2);

        public IEnumerable<FpSyntaxPunctuation> SemicolonTokens => GetChildrenOfType<FpSyntaxPunctuation>(1, 2);

        internal FpFieldList(ISyntaxTree tree, SyntaxNode parent, PersistentFieldList innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpFieldLine : FpSyntaxNode
    {
        public FpVariableIdentifierList FieldNameList => Children[0] as FpVariableIdentifierList;
        public FpSyntaxPunctuation ColonToken => Children[1] as FpSyntaxPunctuation;
        public FpSyntaxNode Type => Children[2] as FpSyntaxNode;

        internal FpFieldLine(ISyntaxTree tree, SyntaxNode parent, PersistentFieldLine innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpPointerType : FpSyntaxNode
    {
        public FpSyntaxPunctuation RoofToken => Children[0] as FpSyntaxPunctuation;
        public FpSyntaxIdentifier TargetTypeIdentifier => Children[1] as FpSyntaxIdentifier;

        internal FpPointerType(ISyntaxTree tree, SyntaxNode parent, PersistentPointerType innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpEnumType : FpSyntaxNode
    {
        public FpSyntaxPunctuation OpenParanthesisToken => Children[0] as FpSyntaxPunctuation;
        public FpEnumMembers EnumMembers => Children[1] as FpEnumMembers;
        public FpSyntaxPunctuation CloseParanthesisToken => Children[2] as FpSyntaxPunctuation;

        internal FpEnumType(ISyntaxTree tree, SyntaxNode parent, PersistentEnumType innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpEnumMembers : FpSyntaxNode
    {
        public IEnumerable<FpSyntaxIdentifier> EnumValueNames
        {
            get
            {
                for (int i = 0; i < Children.Count; i += 2)
                {
                    if (Children[i] is FpSyntaxIdentifier)
                    {
                        yield return Children[i] as FpSyntaxIdentifier;
                    }
                    else
                    {
                        if (Children[i].Children.Count > 0 && Children[i].Children[0] is FpSyntaxIdentifier)
                            yield return Children[i].Children[0] as FpSyntaxIdentifier;
                    }
                }
            }
        }

        public IEnumerable<FpInitializedEnumMember> InitializedEnumValues => Children.OfType<FpInitializedEnumMember>();

        public IEnumerable<FpSyntaxPunctuation> Commas => GetChildrenOfType<FpSyntaxPunctuation>(1, 2);

        internal FpEnumMembers(ISyntaxTree tree, SyntaxNode parent, PersistentEnumMembers innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpInitializedEnumMember : FpSyntaxNode
    {
        internal FpInitializedEnumMember(ISyntaxTree tree, SyntaxNode parent, PersistentInitializedEnumMember innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpTypeDeclaration : FpSyntaxNode
    {
        public FpSyntaxKeyword TypeToken => Children[0] as FpSyntaxKeyword;
        public FpTypeDeclarationLines DeclarationLines => Children[1] as FpTypeDeclarationLines;

        internal FpTypeDeclaration(ISyntaxTree tree, SyntaxNode parent, PersistentTypeDeclaration innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpTypeDeclarationLines : FpSyntaxNode
    {
        public IEnumerable<FpTypeDeclarationLine> Lines => Children.OfType<FpTypeDeclarationLine>();

        internal FpTypeDeclarationLines(ISyntaxTree tree, SyntaxNode parent, PersistentTypeDeclarationLines innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpAssemblerBlock : FpSyntaxNode
    {
        public FpSyntaxKeyword AssemblerKeyword => Children[0] as FpSyntaxKeyword;
        public FpSyntaxPunctuation SemicolonToken => Children[1] as FpSyntaxPunctuation;
        public FpDeclarationBlock Declarations => Children[2] as FpDeclarationBlock;
        public FpAssemblerStatement AssemblerCode => Children[3] as FpAssemblerStatement;

        internal FpAssemblerBlock(ISyntaxTree tree, SyntaxNode parent, PersistentAssemblerBlock innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpExternalDirective : FpSyntaxNode
    {
        internal FpExternalDirective(ISyntaxTree tree, SyntaxNode parent, PersistentExternalDirective innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpProcedureDeclaration : FpSyntaxNode
    {
        public FpProcedureHeader Header => Children[0] as FpProcedureHeader;
        public FpSyntaxPunctuation SemicolonAfterHeader => Children[1] as FpSyntaxPunctuation;
        public bool ContainsForward => (Children[2] as FpSyntaxNode).Kind == FpLexer.SyntaxKind.ForwardToken;
        public bool IsExternal => (Children[2] as FpSyntaxNode).Kind == FpLexer.SyntaxKind.ExternalDirective;
        public FpExternalDirective ExternalDirective => IsExternal ? Children[2] as FpExternalDirective : null;
        public bool IsAssemblerCode => (Children[2] as FpSyntaxNode).Kind == FpLexer.SyntaxKind.AssemblerBlock;
        public FpAssemblerBlock AssemblerBlock => Children[2] as FpAssemblerBlock;
        public bool ContainsBasicBlock => (Children[2] as FpSyntaxNode).Kind == FpLexer.SyntaxKind.Block;
        public FpBlock Block => Children[2] as FpBlock;

        public FpSyntaxPunctuation SemicolonAfterBlock => Children[3] as FpSyntaxPunctuation;

        internal FpProcedureDeclaration(ISyntaxTree tree, SyntaxNode parent, PersistentProcedureDeclaration innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpTypeDeclarationLine : FpSyntaxNode
    {
        public FpSyntaxIdentifier NewTypeName => Children[0] as FpSyntaxIdentifier;
        public FpSyntaxPunctuation EqualsToken => Children[1] as FpSyntaxPunctuation;
        public FpSyntaxNode Type => Children[2] as FpSyntaxNode;
        public FpHintDirective HintDirectives => Children[3] as FpHintDirective;
        public FpSyntaxPunctuation SemicolonToken => Children[4] as FpSyntaxPunctuation;

        internal FpTypeDeclarationLine(ISyntaxTree tree, SyntaxNode parent, PersistentTypeDeclarationLine innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public abstract class FpParameterDeclaration : FpSyntaxNode
    {
        public enum ParameterType { Const, Out, Var, Value }

        public abstract ParameterType ParamType { get; }
        public virtual FpVariableIdentifierList Identifiers => Children[1] as FpVariableIdentifierList;
        public virtual FpSyntaxPunctuation Colon => Children[2] as FpSyntaxPunctuation;
        public virtual FpSyntaxNode Type => Children[3] as FpSyntaxNode;

        internal FpParameterDeclaration(ISyntaxTree tree, SyntaxNode parent, PersistentSyntaxNode innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpConstParameter : FpParameterDeclaration
    {
        public FpSyntaxKeyword ConstKeyword => Children[0] as FpSyntaxKeyword;
        public override ParameterType ParamType => ParameterType.Const;

        internal FpConstParameter(ISyntaxTree tree, SyntaxNode parent, PersistentConstantParameter innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpOutParameter : FpParameterDeclaration
    {
        public FpSyntaxKeyword OutKeyword => Children[0] as FpSyntaxKeyword;
        public override ParameterType ParamType => ParameterType.Out;

        internal FpOutParameter(ISyntaxTree tree, SyntaxNode parent, PersistentOutParameter innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpVarParameter : FpParameterDeclaration
    {
        public FpSyntaxKeyword VarKeyword => Children[0] as FpSyntaxKeyword;
        public override ParameterType ParamType => ParameterType.Var;

        internal FpVarParameter(ISyntaxTree tree, SyntaxNode parent, PersistentVariableParameter innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpValueParameter : FpParameterDeclaration
    {
        public override ParameterType ParamType => ParameterType.Value;
        public override FpVariableIdentifierList Identifiers => Children[0] as FpVariableIdentifierList;
        public override FpSyntaxPunctuation Colon => Children[1] as FpSyntaxPunctuation;
        public override FpSyntaxNode Type => Children[2] as FpSyntaxNode;

        internal FpValueParameter(ISyntaxTree tree, SyntaxNode parent, PersistentValueParameter innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpFormalParameterList : FpSyntaxNode
    {
        public FpSyntaxPunctuation OpenParanthesis => Children[0] as FpSyntaxPunctuation;
        public FpParameterDeclarations ParameterDeclarations => Children[1] as FpParameterDeclarations;
        public FpSyntaxPunctuation CloseParanthesis => Children[2] as FpSyntaxPunctuation;

        internal FpFormalParameterList(ISyntaxTree tree, SyntaxNode parent, PersistentFormalParameterList innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpParameterDeclarations : FpSyntaxNode
    {
        public IEnumerable<FpParameterDeclaration> Parameters => GetChildrenOfType<FpParameterDeclaration>(0, 2);

        public IEnumerable<FpSyntaxPunctuation> Semicolons => GetChildrenOfType<FpSyntaxPunctuation>(1, 2);

        internal FpParameterDeclarations(ISyntaxTree tree, SyntaxNode parent, PersistentParameterDeclarations innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpProcedureHeader : FpSyntaxNode
    {
        public FpSyntaxKeyword ProcedureKeyword => Children[0] as FpSyntaxKeyword;
        public FpSyntaxIdentifier Identifier => Children[1] as FpSyntaxIdentifier;
        public FpFormalParameterList Parameters => Children[2] as FpFormalParameterList;
        public FpHintDirective Hints => Children[3] as FpHintDirective;

        internal FpProcedureHeader(ISyntaxTree tree, SyntaxNode parent, PersistentProcedureHeader innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpFunctionDeclaration : FpSyntaxNode
    {
        public FpFunctionHeader Header => Children[0] as FpFunctionHeader;
        public FpSyntaxPunctuation SemicolonAfterHeader => Children[1] as FpSyntaxPunctuation;
        public bool ContainsForward => (Children[2] as FpSyntaxNode).Kind == FpLexer.SyntaxKind.ForwardToken;
        public bool IsExternal => (Children[2] as FpSyntaxNode).Kind == FpLexer.SyntaxKind.ExternalDirective;
        public FpExternalDirective ExternalDirective => IsExternal ? Children[2] as FpExternalDirective : null;
        public bool IsAssemblerCode => (Children[2] as FpSyntaxNode).Kind == FpLexer.SyntaxKind.AssemblerBlock;
        public FpAssemblerBlock AssemblerBlock => Children[2] as FpAssemblerBlock;
        public bool ContainsBasicBlock => (Children[2] as FpSyntaxNode).Kind == FpLexer.SyntaxKind.Block;
        public FpBlock Block => Children[2] as FpBlock;

        internal FpFunctionDeclaration(ISyntaxTree tree, SyntaxNode parent, PersistentFunctionDeclaration innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpFunctionHeader : FpSyntaxNode
    {
        public FpSyntaxKeyword ProcedureKeyword => Children[0] as FpSyntaxKeyword;
        public FpSyntaxIdentifier Identifier => Children[1] as FpSyntaxIdentifier;
        public FpFormalParameterList Parameters => Children[2] as FpFormalParameterList;
        public FpSyntaxPunctuation Colon => Children[3] as FpSyntaxPunctuation;
        public FpSyntaxNode Type => Children[4] as FpSyntaxNode;
        public FpHintDirective Hints => Children[5] as FpHintDirective;

        internal FpFunctionHeader(ISyntaxTree tree, SyntaxNode parent, PersistentFunctionHeader innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpIndexList : FpSyntaxNode
    {
        internal FpIndexList(ISyntaxTree tree, SyntaxNode parent, PersistentIndexList innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpUsesBlock : FpSyntaxNode
    {
        public FpSyntaxKeyword UsesToken => Children[0] as FpSyntaxKeyword;
        public FpUnitList UnitList => Children[1] as FpUnitList;
        public FpSyntaxPunctuation SemicolonToken => Children[2] as FpSyntaxPunctuation;

        internal FpUsesBlock(ISyntaxTree tree, SyntaxNode parent, PersistentUsesBlock innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpUnitList : FpSyntaxNode
    {
        public IEnumerable<FpUnitDeclaration> UnitDeclarations => GetChildrenOfType<FpUnitDeclaration>(0, 2);

        internal FpUnitList(ISyntaxTree tree, SyntaxNode parent, PersistentUnitList innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpUnitDeclaration : FpSyntaxNode
    {
        public bool ContainsFileName => Children.Count > 1;
        public FpSyntaxIdentifier UnitName => Children[0] as FpSyntaxIdentifier;
        public FpSyntaxKeyword InKeyword => Children[1] as FpSyntaxKeyword;
        public FpConstant FileName => Children[2] as FpConstant;

        internal FpUnitDeclaration(ISyntaxTree tree, SyntaxNode parent, PersistentUnitDeclaration innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpFileType : FpSyntaxNode
    {
        public FpSyntaxKeyword FileToken => Children[0] as FpSyntaxKeyword;
        public FpSyntaxKeyword OfToken => Children[1] as FpSyntaxKeyword;
        public FpSyntaxNode Type => Children[2] as FpSyntaxNode;

        internal FpFileType(ISyntaxTree tree, SyntaxNode parent, PersistentFileType innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpSetType : FpSyntaxNode
    {
        public FpSyntaxKeyword SetToken => Children[0] as FpSyntaxKeyword;
        public FpSyntaxKeyword OfToken => Children[1] as FpSyntaxKeyword;
        public FpSyntaxNode Type => Children[2] as FpSyntaxNode;

        internal FpSetType(ISyntaxTree tree, SyntaxNode parent, PersistentSetType innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpStringWithFixedSize : FpSyntaxNode
    {
        public FpSyntaxKeyword StringToken => Children[0] as FpSyntaxKeyword;
        public bool HasSpecifiedLength => Children.Count > 1;
        public FpSyntaxPunctuation OpenParanthesisToken => HasSpecifiedLength ? Children[1] as FpSyntaxPunctuation : null;
        public FpExpression Expression => HasSpecifiedLength ? Children[2] as FpExpression : null;
        public FpSyntaxPunctuation CloseParanthesisToken => HasSpecifiedLength ? Children[3] as FpSyntaxPunctuation : null;

        internal FpStringWithFixedSize(ISyntaxTree tree, SyntaxNode parent, PersistentStringWithFixedSizeType innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }

    public class FpStringWithCodePageType : FpSyntaxNode
    {
        public FpSyntaxKeyword TypeToken => Children[0] as FpSyntaxKeyword;
        public bool IsAnsiString => StringTypeToken != null && StringTypeToken.Kind == FpLexer.SyntaxKind.AnsistringToken;
        public FpSyntaxKeyword StringTypeToken => Children[1] as FpSyntaxKeyword;
        public FpSyntaxPunctuation OpenParanthesisToken => Children[2] as FpSyntaxPunctuation;
        public FpExpression Expression => Children[3] as FpExpression;
        public FpSyntaxPunctuation CloseParanthesisToken => Children[4] as FpSyntaxPunctuation;

        internal FpStringWithCodePageType(ISyntaxTree tree, SyntaxNode parent, PersistentStringWithCodePageType innerNode, int start)
            : base(tree, parent, innerNode, start)
        {
        }
    }
}