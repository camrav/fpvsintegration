﻿using CodeAnalysis.SyntaxTree.PersistentTree;
using FpLexer;

namespace CodeAnalysis.SyntaxTree.OnDemandTree
{
    public abstract class FpSyntaxTrivia : FpSyntaxNode
    {
        public override bool IsMissing => false;

        internal FpSyntaxTrivia(ISyntaxTree tree, SyntaxNode parent, PersistentSyntaxTrivia underlyingTrivia, int start)
            : base(tree, parent, underlyingTrivia, start)
        {
        }
    }

    public class WhitespaceTrivia : FpSyntaxTrivia
    {
        internal WhitespaceTrivia(ISyntaxTree tree, SyntaxNode parent, PersistentWhitespaceSyntaxTrivia underlyingTrivia, int start)
            : base(tree, parent, underlyingTrivia, start)
        {
        }
    }

    public class CommentTrivia : FpSyntaxTrivia
    {
        internal CommentTrivia(ISyntaxTree tree, SyntaxNode parent, PersistentCommentSyntaxTrivia underlyingTrivia, int start)
            : base(tree, parent, underlyingTrivia, start)
        {
        }
    }

    public class UnknownTrivia : FpSyntaxTrivia
    {
        internal UnknownTrivia(ISyntaxTree tree, SyntaxNode parent, PersistentUnknownSyntaxTrivia underlyingTrivia, int start)
            : base(tree, parent, underlyingTrivia, start)
        {
        }
    }

    public class UnexpectedTrivia : FpSyntaxTrivia
    {
        public SyntaxKind[] Expected => (InnerNode as PersistentUnexpectedSyntaxTrivia).Expected;

        internal UnexpectedTrivia(ISyntaxTree tree, SyntaxNode parent, PersistentUnexpectedSyntaxTrivia underlyingTrivia, int start)
            : base(tree, parent, underlyingTrivia, start)
        {
        }

        public override string ToString()
        {
            return $"Expected {string.Join(", ", Expected)}; Got {Kind.ToString()}";//  string.Format("Expected {0}, Got Unexpected {0}", Kind.ToString());
        }
    }

    public class AssemblerTrivia : FpSyntaxTrivia
    {
        internal AssemblerTrivia(ISyntaxTree tree, SyntaxNode parent, PersistentAssemblerTrivia underlyingTrivia, int start)
            : base(tree, parent, underlyingTrivia, start)
        {
        }
    }
}