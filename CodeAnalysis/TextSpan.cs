﻿namespace CodeAnalysis
{
    public struct TextSpan
    {
        public int Start { get; private set; }
        public int Length { get; private set; }
        public int End => Start + Length;

        public TextSpan(int start, int length)
        {
            Start = start;
            Length = length;
        }

        public bool OverlapsWith(TextSpan span)
        {
            if (Start + Length <= span.Start)
                return false;
            if (span.Start + span.Length <= Start)
                return false;

            return true;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="toExclude"></param>
        /// <returns>TextSpan starting AFTER the given span.</returns>
        public TextSpan Exluding(TextSpan toExclude)
        {
            int newStart = toExclude.Start + toExclude.Length;
            if (newStart > Start + Length)
            {
                newStart = Start + Length;
            }
            else if (newStart < Start)
                newStart = Start;

            int originalEnd = Start + Length;

            return new TextSpan(newStart, originalEnd - newStart);
        }

        public bool Contains(TextSpan other)
        {
            return Start <= other.Start && End >= other.End;
        }

        public override string ToString()
        {
            return string.Format("Start: {0}, Length: {1}", Start, Length);
        }
    }
}