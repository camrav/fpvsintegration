﻿using CodeAnalysis.SyntaxTree.OnDemandTree;
using FpLexer;
using System.Collections.Generic;
using System.Linq;

namespace CodeAnalysis.Semantics
{
    internal class ExpressionEvaluator
    {
        private SemanticModel model;

        public ExpressionEvaluator(SemanticModel model)
        {
            this.model = model;
        }

        public string CalculateExpression(FpExpression expression, out ValueType valueType)
        {
            string left = CalculateSimpleExpression(expression.LeftExpression, out valueType);

            if (expression.HasRightSide)
            {
                ValueType rightType;
                string right = CalculateSimpleExpression(expression.RightExpression, out rightType);

                if (expression.Operator.Kind.IsRelationalOperator())
                {
                    if (!OperandsAreOfType(valueType, rightType, ValueType.Int, ValueType.Float))
                    {
                        valueType = ValueType.Undefined;
                        return null;
                    }

                    double leftValue = double.Parse(left);
                    double rightValue = double.Parse(right);
                    bool result = false;

                    switch (expression.Operator.Kind)
                    {
                        case SyntaxKind.LessOrEqualThanToken:
                            result = leftValue <= rightValue;
                            break;

                        case SyntaxKind.GreaterOrEqualThanToken:
                            result = leftValue >= rightValue;
                            break;

                        case SyntaxKind.LessThanToken:
                            result = leftValue < rightValue;
                            break;

                        case SyntaxKind.GreaterThanToken:
                            result = leftValue > rightValue;
                            break;

                        case SyntaxKind.EqualsToken:
                            result = leftValue == rightValue;
                            break;

                        case SyntaxKind.DoesNotEqualToken:
                            result = leftValue != rightValue;
                            break;
                    }
                    valueType = ValueType.Bool;
                    return result.ToString();
                }
                else
                {
                    valueType = ValueType.Undefined;
                    return null;
                }
            }

            return left;
        }

        private string CalculateSimpleExpression(FpSimpleExpression leftExpression, out ValueType leftType)
        {
            List<FpTerm> terms = new List<FpTerm>(leftExpression.Terms);
            List<FpSyntaxTerminal> operators = new List<FpSyntaxTerminal>(leftExpression.Operators);

            string value = CalculateTerm(terms[0], out leftType);

            for (int i = 0; i < operators.Count; i++)
            {
                ValueType rightType;
                string rightValue = CalculateTerm(terms[i + 1], out rightType);
                if (rightType == ValueType.Undefined)
                    return null;

                switch (operators[i].Kind)
                {
                    case SyntaxKind.PlusToken:
                        if (!OperandsAreOfType(leftType, rightType, ValueType.Int, ValueType.Float, ValueType.String))
                        {
                            leftType = ValueType.Undefined;
                            return null;
                        }
                        if (leftType == ValueType.String && rightType == ValueType.String)
                            value = value + rightValue;
                        else if (leftType == ValueType.String || rightType == ValueType.String)
                        {
                            leftType = ValueType.Undefined;
                            return null;
                        }
                        else if (leftType == ValueType.Int && rightType == ValueType.Int)
                            value = (int.Parse(value) + int.Parse(rightValue)).ToString();
                        else
                        {
                            leftType = ValueType.Float;
                            value = (double.Parse(value) + double.Parse(rightValue)).ToString();
                        }
                        break;

                    case SyntaxKind.MinusToken:
                        if (!OperandsAreOfType(leftType, rightType, ValueType.Int, ValueType.Float))
                        {
                            leftType = ValueType.Undefined;
                            return null;
                        }
                        if (leftType == ValueType.Int && rightType == ValueType.Int)
                            value = (int.Parse(value) - int.Parse(rightValue)).ToString();
                        else
                        {
                            leftType = ValueType.Float;
                            value = (double.Parse(value) - double.Parse(rightValue)).ToString();
                        }
                        break;

                    case SyntaxKind.OrToken:
                        if (leftType == ValueType.Bool && rightType == ValueType.Bool)
                            value = (bool.Parse(value) | bool.Parse(rightValue)).ToString();
                        else if (leftType == ValueType.Int && rightType == ValueType.Int)
                            value = (int.Parse(value) | int.Parse(rightValue)).ToString();
                        break;

                    case SyntaxKind.XorToken:
                        if (leftType == ValueType.Bool && rightType == ValueType.Bool)
                            value = (bool.Parse(value) ^ bool.Parse(rightValue)).ToString();
                        else if (leftType == ValueType.Int && rightType == ValueType.Int)
                            value = (int.Parse(value) ^ int.Parse(rightValue)).ToString();
                        break;
                }
            }

            return value;
        }

        private string CalculateTerm(FpTerm fpTerm, out ValueType leftType)
        {
            List<FpFactor> factors = new List<FpFactor>(fpTerm.Factors);
            List<FpSyntaxTerminal> operators = new List<FpSyntaxTerminal>(fpTerm.Operators);

            string value = CalculateFactor(factors[0], out leftType);

            for (int i = 0; i < operators.Count; i++)
            {
                ValueType rightType;
                string rightValue = CalculateFactor(factors[i + 1], out rightType);
                if (rightType == ValueType.Undefined)
                    return null;

                switch (operators[i].Kind)
                {
                    case SyntaxKind.AsteriskToken:
                        if (!OperandsAreOfType(leftType, rightType, ValueType.Int, ValueType.Float))
                        {
                            leftType = ValueType.Undefined;
                            return null;
                        }
                        if (leftType == ValueType.Int && rightType == ValueType.Int)
                            value = (int.Parse(value) * int.Parse(rightValue)).ToString();
                        else
                        {
                            leftType = ValueType.Float;
                            value = (double.Parse(value) * double.Parse(rightValue)).ToString();
                        }
                        break;

                    case SyntaxKind.SlashToken:
                        if (!OperandsAreOfType(leftType, rightType, ValueType.Int, ValueType.Float))
                        {
                            leftType = ValueType.Undefined;
                            return null;
                        }
                        leftType = ValueType.Float;
                        value = (double.Parse(value) / double.Parse(rightValue)).ToString();
                        break;

                    case SyntaxKind.DivToken:
                        if (leftType == ValueType.Int && rightType == ValueType.Int)
                            value = (int.Parse(value) / int.Parse(rightValue)).ToString();
                        else
                        {
                            leftType = ValueType.Undefined;
                            return null;
                        }
                        break;

                    case SyntaxKind.ModToken:
                        if (!OperandsAreOfType(leftType, rightType, ValueType.Int, ValueType.Float))
                        {
                            leftType = ValueType.Undefined;
                            return null;
                        }
                        if (leftType == ValueType.Int && rightType == ValueType.Int)
                            value = (int.Parse(value) % int.Parse(rightValue)).ToString();
                        else
                        {
                            leftType = ValueType.Float;
                            value = (double.Parse(value) % double.Parse(rightValue)).ToString();
                        }
                        break;

                    case SyntaxKind.AndToken:
                        if (leftType == ValueType.Bool && rightType == ValueType.Bool)
                            value = (bool.Parse(value) & bool.Parse(rightValue)).ToString();
                        else if (leftType == ValueType.Int && rightType == ValueType.Int)
                            value = (int.Parse(value) & int.Parse(rightValue)).ToString();
                        else
                        {
                            leftType = ValueType.Undefined;
                            return null;
                        }
                        break;

                    case SyntaxKind.ShlToken:
                    case SyntaxKind.LeftShiftToken:
                        if (leftType == ValueType.Int && rightType == ValueType.Int)
                            value = (int.Parse(value) << int.Parse(rightValue)).ToString();
                        else
                        {
                            leftType = ValueType.Undefined;
                            return null;
                        }
                        break;

                    case SyntaxKind.ShrToken:
                    case SyntaxKind.RightShiftToken:
                        if (leftType == ValueType.Int && rightType == ValueType.Int)
                            value = (int.Parse(value) >> int.Parse(rightValue)).ToString();
                        else
                        {
                            leftType = ValueType.Undefined;
                            return null;
                        }
                        break;
                }
            }

            return value;
        }

        private bool OperandsAreOfType(ValueType left, ValueType right, params ValueType[] types)
        {
            return types.Contains(left) && types.Contains(right);
        }

        private string CalculateFactor(FpFactor fpFactor, out ValueType type)
        {
            if (fpFactor.ContainsUnaryOperator)
            {
                return CalculateFactorWithUnaryOperator(fpFactor, out type);
            }
            else if (fpFactor.ContainsParanthesis)
            {
                return CalculateExpression(fpFactor.Expression, out type);
            }
            else
            {
                return CalculateConstant(fpFactor.Terminal, out type);
            }
        }

        private string CalculateConstant(FpSyntaxTerminal constantNode, out ValueType type)
        {
            if (constantNode.Kind == SyntaxKind.StringConstant)
            {
                type = ValueType.String;
                return constantNode.Text;
            }
            else if (constantNode.Kind == SyntaxKind.UintConstant)
            {
                type = ValueType.Int; //TODO: parse differently if its binary/octal/hex
                return constantNode.Text;
            }
            else if (constantNode.Kind == SyntaxKind.RealConstant)
            {
                type = ValueType.Float;
                return constantNode.Text;
            }
            else if (constantNode.Kind == SyntaxKind.NilConstant)
            {
                type = ValueType.Pointer;
                return constantNode.Text;
            }
            else if (constantNode.Kind == SyntaxKind.IdentifierToken)
            {
                var symbol = model.GetSymbol(constantNode as FpSyntaxIdentifier);
                if (symbol == null || symbol.SymbolKind != SymbolKind.Constant)
                {
                    type = ValueType.Undefined;
                    return null;
                }
                var constant = symbol as ConstantSymbol;
                type = constant.Type;
                return constant.Value;
            }

            type = ValueType.Undefined;
            return null;
        }

        private string CalculateFactorWithUnaryOperator(FpFactor factor, out ValueType type)
        {
            FpSyntaxTerminal operatorNode = factor.UnaryOperator;
            string value = CalculateFactor(factor.Operand, out type);
            if (operatorNode.Kind == SyntaxKind.PlusToken)
            {
                switch (type)
                {
                    case ValueType.Int:
                    case ValueType.Float:
                        return value;

                    default:
                        type = ValueType.Undefined;
                        return null;
                }
            }
            else if (operatorNode.Kind == SyntaxKind.MinusToken)
            {
                switch (type)
                {
                    case ValueType.Int:
                    case ValueType.Float:
                        if (value.StartsWith("-"))
                            return value.Substring(1);
                        else
                            return '-' + value;

                    default:
                        type = ValueType.Undefined;
                        return null;
                }
            }
            else if (operatorNode.Kind == SyntaxKind.NotToken)
            {
                switch (type)
                {
                    case ValueType.Int:
                        return (~int.Parse(value)).ToString();

                    case ValueType.Bool:
                        return (!bool.Parse(value)).ToString();

                    default:
                        type = ValueType.Undefined;
                        return null;
                }
            }
            type = ValueType.Undefined;
            return null;
        }
    }
}