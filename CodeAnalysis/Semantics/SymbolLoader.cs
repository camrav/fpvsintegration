﻿using CodeAnalysis.SyntaxTree.OnDemandTree;
using FpLexer;
using ProjectSystem;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CodeAnalysis.Semantics
{
    internal class SymbolLoader
    {
        private SemanticModel model;
        private UnitManager unitManager;
        private Dictionary<FpSyntaxNode, ISymbolWithChildren> subprograms;

        private TypeSymbol unknownType;

        public SymbolLoader(SemanticModel model, UnitManager manager, Dictionary<FpSyntaxNode, ISymbolWithChildren> subprograms)
        {
            this.model = model;
            unitManager = manager;
            this.subprograms = subprograms;
        }

        public void InitProgramRoot(ProgramSymbol result, FpProgramRootNode fpProgramRootNode)
        {
            foreach (var symbol in InitDefaultTypes(result))
                result.Children.Add(symbol);

            var unit = unitManager.LoadUnit("system");
            if (unit != null)
                result.Children.Add(unit);
            InitUnknownType(result);

            AddExceptions(result);

            ProcessUses(result, fpProgramRootNode.UsesBlock);
            ProcessDeclarationBlock(fpProgramRootNode.Block.Declarations, result);
        }

        private void InitUnknownType(ProgramSymbol result)
        {
            unknownType = new UnknownTypeSymbol(result, 0, "??");
            result.Children.Add(unknownType);
        }

        private void AddExceptions(ProgramSymbol program)
        {
            var anyType = new OrdinalTypeSymbol(program, 0, "any", 0, "any", "any", ProjectSystem.OrdinalType.Void);
            AddExtraProcedure(program, anyType, "Writeln");
            AddExtraProcedure(program, anyType, "Write");
            AddExtraProcedure(program, anyType, "Readln");
            AddExtraProcedure(program, anyType, "Read");

            var ordinalType = new OrdinalTypeSymbol(program, 0, "ordinal", 0, string.Empty, string.Empty, ProjectSystem.OrdinalType.Void);
            AddExtraProcedure(program, ordinalType, "Inc");
            AddExtraProcedure(program, ordinalType, "Dec");

            var pointerType = new PointerTypeSymbol(program, 0, "Pointer", null);
            AddExtraProcedure(program, pointerType, "New");
            AddExtraProcedure(program, pointerType, "Dispose");
        }

        private void AddExtraProcedure(ProgramSymbol program, TypeSymbol anyType, string name)
        {
            var symbol = new ProcedureSymbol(name, program, 0);
            symbol.Children.Add(new ParameterSymbol(name, symbol, anyType, 0, ParameterType.Value));
            program.Children.Add(symbol);
        }

        private void ProcessUses(ProgramSymbol result, FpUsesBlock uses)
        {
            if (uses.IsMissing) return;
            foreach (var declaration in uses.UnitList.UnitDeclarations)
            {
                UnitSymbol unit = null;
                if (declaration.ContainsFileName)
                {
                    unit = unitManager.LoadUnitIn(declaration.UnitName.Identifier, declaration.FileName.Text);
                }
                else
                {
                    unit = unitManager.LoadUnit(declaration.UnitName.Identifier);
                }
                if (unit != null)
                    result.Children.Add(unit);
            }
        }

        private void ProcessDeclarationBlock(FpDeclarationBlock block, ISymbolWithChildren parentSymbol)
        {
            for (int i = 0; i < block.Children.Count; i++)
            {
                switch ((block.Children[i] as FpSyntaxNode).Kind)
                {
                    case SyntaxKind.VarDeclaration:
                        AddVariables(parentSymbol, block.Children[i] as FpVarBlock);
                        break;

                    case SyntaxKind.TypeDeclaration:
                        AddTypes(parentSymbol, block.Children[i] as FpTypeDeclaration);
                        break;

                    case SyntaxKind.ConstantDeclaration:
                        AddConstants(parentSymbol, block.Children[i] as FpConstantDeclaration);
                        break;

                    case SyntaxKind.ProcedureDeclaration:
                        AddProcedure(parentSymbol, block.Children[i] as FpProcedureDeclaration);
                        break;

                    case SyntaxKind.FunctionDeclaration:
                        AddFunction(parentSymbol, block.Children[i] as FpFunctionDeclaration);
                        break;

                    default:
                        break;
                }
            }

            CheckPointerTypes(parentSymbol);
        }

        private void CheckPointerTypes(ISymbolWithChildren parentSymbol)
        {
            var pointerTypes = parentSymbol.Children.Where(c => c.SymbolKind == SymbolKind.Type)
                .Cast<ITypeSymbol>().Where(c => c.Kind == TypeKind.Pointer);

            foreach (PointerTypeSymbol pointerType in pointerTypes)
            {
                var baseTypeName = pointerType.BaseType.Name;
                var baseType = parentSymbol
                    .Children
                    .Where(c => c.SymbolKind == SymbolKind.Type && string.Equals(c.Name, baseTypeName, StringComparison.InvariantCultureIgnoreCase))
                    .Cast<ITypeSymbol>()
                    .FirstOrDefault();

                pointerType.BaseType = baseType;
            }
        }

        private void AddFunction(ISymbolWithChildren parentSymbol, FpFunctionDeclaration fpFunctionDeclaration)
        {
            var function = ParseFunctionHeader(parentSymbol, fpFunctionDeclaration.Header);
            if (function == null) return;

            FunctionSymbol existingFunction;
            if (FunctionExists(fpFunctionDeclaration, function, out existingFunction))
            {
                function = existingFunction;
            }
            else
            {
                parentSymbol.Children.Add(function);
            }

            subprograms.Add(fpFunctionDeclaration, function);

            if (fpFunctionDeclaration.ContainsForward)
                return;

            if (fpFunctionDeclaration.IsAssemblerCode)
                ProcessDeclarationBlock(fpFunctionDeclaration.AssemblerBlock.Declarations, function);

            if (fpFunctionDeclaration.ContainsBasicBlock)
                ProcessDeclarationBlock(fpFunctionDeclaration.Block.Declarations, function);
        }

        private bool FunctionExists(FpFunctionDeclaration fpFunctionDeclaration, FunctionSymbol function, out FunctionSymbol existingFunction)
        {
            existingFunction = function;
            var symbol = model.GetSymbol(fpFunctionDeclaration.Header.Identifier);
            if (symbol == null)
                return false;
            if (symbol.SymbolKind == SymbolKind.Function)
            {
                existingFunction = symbol as FunctionSymbol;
                return true;
            }
            return symbol != null;
        }

        private FunctionSymbol ParseFunctionHeader(ISymbolWithChildren parentSymbol, FpFunctionHeader header)
        {
            var type = GetTypeSymbol(header.Type);
            if (header.Identifier == null) return null;
            var result = new FunctionSymbol(header.Identifier.Text, parentSymbol, header.End, type);

            if (!ParseParameters(result, header.Parameters))
                return null;

            return result;
        }

        private void AddProcedure(ISymbolWithChildren parentSymbol, FpProcedureDeclaration fpProcedureDeclaration)
        {
            var procedure = ParseProcedureHeader(parentSymbol, fpProcedureDeclaration.Header);
            if (procedure == null) return;

            ProcedureSymbol existingProcedure;
            if (ProcedureExists(fpProcedureDeclaration, procedure, out existingProcedure))
                procedure = existingProcedure;
            else
                parentSymbol.Children.Add(procedure);

            subprograms.Add(fpProcedureDeclaration, procedure);

            if (fpProcedureDeclaration.ContainsForward)
                return;

            if (fpProcedureDeclaration.IsAssemblerCode)
                ProcessDeclarationBlock(fpProcedureDeclaration.AssemblerBlock.Declarations, procedure);

            if (fpProcedureDeclaration.ContainsBasicBlock)
                ProcessDeclarationBlock(fpProcedureDeclaration.Block.Declarations, procedure);
        }

        private bool ProcedureExists(FpProcedureDeclaration fpProcedureDeclaration, ProcedureSymbol procedure, out ProcedureSymbol existingProcedure)
        {
            existingProcedure = procedure;
            var symbol = model.GetSymbol(fpProcedureDeclaration.Header.Identifier);
            if (symbol == null)
                return false;
            if (symbol.SymbolKind == SymbolKind.Procedure)
            {
                existingProcedure = symbol as ProcedureSymbol;
                return true;
            }
            return symbol != null;
        }

        private ProcedureSymbol ParseProcedureHeader(ISymbolWithChildren parentSymbol, FpProcedureHeader header)
        {
            if (header.Identifier == null) return null;
            var result = new ProcedureSymbol(header.Identifier.Text, parentSymbol, header.End);

            if (!ParseParameters(result, header.Parameters))
                return null;

            return result;
        }

        private bool ParseParameters(ProcedureSymbol result, FpFormalParameterList parameters)
        {
            foreach (var declaration in parameters.ParameterDeclarations.Parameters)
            {
                ITypeSymbol type = GetTypeSymbol(declaration.Type);
                if (type == null)
                    return false;
                ParameterType paramType = (ParameterType)Enum.Parse(typeof(ParameterType), declaration.ParamType.ToString());

                foreach (var identifier in declaration.Identifiers.Identifiers)
                {
                    var symbol = new ParameterSymbol(identifier.Text, result, type, declaration.Type.End, paramType);
                    result.Children.Add(symbol);
                }
            }

            return true;
        }

        private void AddConstants(ISymbolWithChildren parent, FpConstantDeclaration fpConstantDeclaration)
        {
            foreach (var line in fpConstantDeclaration.Declarations.UntypedLines)
            {
                ValueType valueType;

                string value = new ExpressionEvaluator(model).CalculateExpression(line.Value, out valueType);
                if (valueType == ValueType.Undefined)
                    value = "Invalid expression";
                if (line.ConstantName == null) continue;
                var constant = new ConstantSymbol(parent, line.End, line.ConstantName.Text, valueType, value, null);
                parent.Children.Add(constant);
            }
        }

        private void AddTypes(ISymbolWithChildren parent, FpTypeDeclaration fpTypeDeclaration)
        {
            foreach (var line in fpTypeDeclaration.DeclarationLines.Lines)
            {
                ITypeSymbol type = GetTypeSymbol(line.Type) ?? unknownType;
                string name = line.NewTypeName.Text;
                if (name == null) continue;

                parent.Children.Add(type.Clone(name, parent, line.End));
            }
        }

        private void AddVariables(ISymbolWithChildren parent, FpVarBlock varBlock)
        {
            foreach (var line in varBlock.DeclarationLines.Lines)
            {
                ITypeSymbol type = GetTypeSymbol(line.Type);
                if (type == null)
                    continue;
                foreach (FpSyntaxTerminal identifier in line.IdentifierList.Identifiers)
                {
                    if (!identifier.IsMissing)
                        parent.Children.Add(new VariableSymbol(identifier.Text, parent, type, identifier.End));
                }
            }
        }

        private ITypeSymbol GetTypeSymbol(FpSyntaxNode fpType)
        {
            if (fpType.IsMissing) return null;
            switch (fpType.Kind)
            {
                case SyntaxKind.TypeIdentifier:
                    return GetBasicTypeSymbol(fpType as FpSyntaxIdentifier);

                case SyntaxKind.ArrayType:
                    return GetArrayTypeSymbol(fpType as FpArrayType);

                case SyntaxKind.RecordType:
                    return GetRecordTypeSymbol(fpType as FpRecordType);

                case SyntaxKind.SetType:
                    return GetSetTypeSymbol(fpType as FpSetType);

                case SyntaxKind.FileType:
                    return GetFileTypeSymbol(fpType as FpFileType);

                case SyntaxKind.EnumType:
                    return GetEnumTypeSymbol(fpType as FpEnumType);

                case SyntaxKind.StringWithFixedSizeType:
                    return GetStringWithFixedSizeType(fpType as FpStringWithFixedSize);

                case SyntaxKind.StringWithCodePageType:
                    return GetStringWithCodePage(fpType as FpStringWithCodePageType);

                case SyntaxKind.PointerType:
                    return GetPointerTypeSymbol(fpType as FpPointerType);
            }
            return null;
        }

        private ITypeSymbol GetPointerTypeSymbol(FpPointerType fpPointerType)
        {
            PointerTypeSymbol fakeInnerType = new PointerTypeSymbol(null, 0, fpPointerType?.TargetTypeIdentifier?.Identifier, null);

            PointerTypeSymbol type = new PointerTypeSymbol(null, fpPointerType.End, string.Empty, fakeInnerType);

            return type;
        }

        private ITypeSymbol GetStringWithCodePage(FpStringWithCodePageType fpStringWithCodePageType)
        {
            StringType type = StringType.Short;
            if (fpStringWithCodePageType.IsAnsiString)
                type = StringType.Ansi;

            return new StringTypeSymbol(null, fpStringWithCodePageType.End, string.Empty, 0xff, type);
        }

        private ITypeSymbol GetStringWithFixedSizeType(FpStringWithFixedSize fpStringWithFixedSize)
        {
            const int defaultLength = 0xFF;
            int length = defaultLength;
            if (fpStringWithFixedSize.HasSpecifiedLength)
            {
                ValueType valType;
                var result = new ExpressionEvaluator(model).CalculateExpression(fpStringWithFixedSize.Expression, out valType);
                if (valType == ValueType.Int)
                {
                    length = int.Parse(result);
                }
            }

            return new StringTypeSymbol(null, fpStringWithFixedSize.End, string.Empty, length, StringType.Short);
        }

        private ITypeSymbol GetEnumTypeSymbol(FpEnumType fpEnumType)
        {
            var result = new EnumTypeSymbol(null, fpEnumType.End, string.Empty, 0, 0, 0);

            foreach (var member in fpEnumType.EnumMembers.EnumValueNames)
            {
                result.Children.Add(new ConstantSymbol(result, member.End, member.Text, ValueType.Enum, member.Text, result));
            }

            return result;
        }

        private ITypeSymbol GetFileTypeSymbol(FpFileType fpFileType)
        {
            ITypeSymbol baseSymbol = GetTypeSymbol(fpFileType.Type);
            if (baseSymbol == null)
                return null;

            return new FileTypeSymbol(null, fpFileType.End, string.Empty, FileType.Typed, baseSymbol);
        }

        private ITypeSymbol GetSetTypeSymbol(FpSetType fpSetType)
        {
            ITypeSymbol baseSymbol = GetTypeSymbol(fpSetType.Type);
            if (baseSymbol == null)
                return null;

            return new SetTypeSymbol(null, fpSetType.End, string.Empty, baseSymbol);
        }

        private ITypeSymbol GetRecordTypeSymbol(FpRecordType fpRecordType)
        {
            RecordTypeSymbol result = new RecordTypeSymbol(null, fpRecordType.End, string.Empty, -1);

            foreach (var line in fpRecordType.FieldList.FieldLines)
            {
                ITypeSymbol type = GetTypeSymbol(line.Type) ?? unknownType;

                foreach (var identifier in line.FieldNameList.Identifiers)
                {
                    result.Children.Add(new VariableSymbol(identifier.Text, result, type, fpRecordType.End));
                }
            }

            return result;
        }

        private ITypeSymbol GetArrayTypeSymbol(FpArrayType type)
        {
            var elementType = GetTypeSymbol(type.Type) ?? unknownType;

            Range[] ranges = type.ContainsRange ? GetRanges(type.Ranges) : new Range[0];

            return new ArrayTypeSymbol(null, type.End, string.Empty, elementType, null, ranges);
        }

        private Range[] GetRanges(FpSubrangeList ranges)
        {
            List<Range> result = new List<Range>();

            foreach (var range in ranges.Subranges)
            {
                result.Add(new Range { LowerBound = range.LowerBound.Text, UpperBound = range.UpperBound.Text });
            }

            return result.ToArray();
        }

        private ITypeSymbol GetBasicTypeSymbol(FpSyntaxIdentifier identifier)
        {
            var symbols = from symbol in model.GetAvailableSymbols(identifier)
                          where symbol.SymbolKind == SymbolKind.Type
                          where string.Equals(symbol.Name, identifier.Text, StringComparison.OrdinalIgnoreCase)
                          select symbol;

            return symbols.FirstOrDefault() as ITypeSymbol;
        }

        private ISymbol[] InitDefaultTypes(ISymbolWithChildren rootSymbol)
        {
            return new ISymbol[]
            {
            };
        }
    }
}