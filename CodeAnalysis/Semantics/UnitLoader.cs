﻿using ProjectSystem;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml;

namespace CodeAnalysis.Semantics
{
    internal class UnitLoader
    {
        string PpuDumpExecutable => manager.PpuDumpExecutable;

        private string path;
        private UnitManager manager;

        private Dictionary<int, ITypeSymbol> typeSymbols = new Dictionary<int, ITypeSymbol>();
        private List<Dictionary<int, ITypeSymbol>> dependencySymbols = new List<Dictionary<int, ITypeSymbol>>();

        private event Action<int> symbolAdded = delegate { };

        public UnitLoader(string path, UnitManager manager)
        {
            this.path = path;
            this.manager = manager;
        }

        internal void Load(UnitSymbol symbol)
        {
            XmlDocument dump = DumpUnitFile(path);
            LoadDependentUnits(dump);
            XmlElement interfaceNode = GetInterfaceNode(dump);

            symbolAdded = delegate { };

            HashSet<string> skipped = new HashSet<string>();
            foreach (XmlNode symbolNode in interfaceNode.ChildNodes)
            {
                switch (symbolNode.Name)
                {
                    case "ord":
                        ParseOrdSymbol(symbol, symbolNode as XmlElement);
                        break;

                    case "string":
                        ParseStringSymbol(symbol, symbolNode as XmlElement);
                        break;

                    case "float":
                        ParseFloatSymbol(symbol, symbolNode as XmlElement);
                        break;

                    case "array":
                        ParseArraySymbol(symbol, symbolNode as XmlElement);
                        break;

                    case "ptr":
                        ParsePtrSymbol(symbol, symbolNode as XmlElement);
                        break;

                    case "file":
                        ParseFileSymbol(symbol, symbolNode as XmlElement);
                        break;

                    case "rec":
                        ParseRecordSymbol(symbol, symbolNode as XmlElement);
                        break;

                    case "enum":
                        ParseEnumSymbol(symbol, symbolNode as XmlElement);
                        break;

                    case "const":
                        ParseConstSymbol(symbol, symbolNode as XmlElement);
                        break;

                    case "proc":
                        ParseProcSymbol(symbol, symbolNode as XmlElement);
                        break;

                    case "type":
                        ParseTypeSymbol(symbol, symbolNode as XmlElement);
                        break;

                    case "var":
                        ParseVarSymbol(symbol, symbolNode as XmlElement);
                        break;

                    case "undefined":
                    case "formal":
                    case "set": //TODO: maybe have to parse set after all
                    case "proctype":
                    case "variant":
                    case "obj":
                    case "classref":
                    case "prop":
                        //ignored
                        ParseNullSymbol(symbol, symbolNode as XmlElement);
                        break;

                    default:
                        skipped.Add(symbolNode.Name);
                        break;
                }
            }
            symbol.TypeSymbols = typeSymbols;
        }

        private void LoadDependentUnits(XmlDocument dump)
        {
            var units = dump.GetElementsByTagName("units");
            if (units.Count == 0) return;

            foreach (XmlElement unit in units[0].ChildNodes)
            {
                dependencySymbols.Add(manager.LoadUnit(unit.InnerText).TypeSymbols);
            }
        }

        private void AddSymbol(int id, ITypeSymbol symbol)
        {
            typeSymbols.Add(id, symbol);
            symbolAdded(id);
        }

        private XmlDocument DumpUnitFile(string path)
        {
            Process process = new Process();
            process.StartInfo = new ProcessStartInfo
            {
                FileName = PpuDumpExecutable,
                Arguments = string.Format("-Fx -VA \"{0}\"", path), // -Fx makes output xml
                RedirectStandardOutput = true,
                CreateNoWindow = true,
                WindowStyle = ProcessWindowStyle.Hidden,
                UseShellExecute = false
            };
            process.Start();
            XmlDocument doc = new XmlDocument();
            doc.Load(process.StandardOutput);
            process.WaitForExit();
            process.Dispose();

            return doc;
        }

        private XmlElement GetInterfaceNode(XmlDocument doc)
        {
            var unit = doc.DocumentElement;
            foreach (XmlNode child in unit.ChildNodes)
            {
                if (child.Name == "interface") return child as XmlElement;
            }
            return null;
        }

        private void ParseNullSymbol(UnitSymbol parent, XmlElement element)
        {
            int id = -1;
            for (var child = element.FirstChild; child != null; child = child.NextSibling)
            {
                if (child.Name == "id")
                    id = int.Parse(child.InnerText);
            }

            if (id >= 0)
                typeSymbols.Add(id, null);
        }

        private ITypeSymbol GetTypeSymbol(XmlElement typeNode)
        {
            int id, unit;
            GetIdAndUnit(typeNode, out id, out unit);

            if (unit != -1)
            {
                return dependencySymbols[unit][id];
            }

            if (typeSymbols.ContainsKey(id))
                return typeSymbols[id];
            return null;
        }

        private void GetIdAndUnit(XmlElement element, out int id, out int unit)
        {
            id = -1;
            unit = -1;

            foreach (XmlElement child in element.ChildNodes)
            {
                switch (child.Name)
                {
                    case "id":
                        id = int.Parse(child.InnerText);
                        break;

                    case "unit":
                        unit = int.Parse(child.InnerText);
                        break;
                }
            }
        }

        private void ParseTypeSymbol(UnitSymbol parent, XmlElement element)
        {
            ITypeSymbol baseType = null;
            string name = string.Empty;

            foreach (XmlElement child in element.ChildNodes)
            {
                string text = child.InnerText;
                switch (child.Name)
                {
                    case "name":
                        name = text;
                        break;

                    case "ref":
                        baseType = GetTypeSymbol(child);
                        break;
                }
            }

            AliasTypeSymbol symbol = new AliasTypeSymbol(parent, 0, name, baseType);
            parent.Children.Add(symbol);
        }

        private void ParseProcSymbol(UnitSymbol parent, XmlElement element)
        {
            string name = string.Empty;
            int id = -1;
            XmlElement options = null;
            ITypeSymbol returnValue = null;
            XmlElement parameters = null;

            foreach (XmlElement child in element.ChildNodes)
            {
                string text = child.InnerText;
                switch (child.Name)
                {
                    case "name":
                        name = text;
                        break;

                    case "id":
                        id = int.Parse(text);
                        break;

                    case "options":
                        options = child;
                        break;

                    case "rettype":
                        returnValue = GetTypeSymbol(child);
                        break;

                    case "params":
                        parameters = child;
                        break;
                }
            }

            ProcedureSymbol subprogram;
            if (returnValue == null)
                subprogram = new ProcedureSymbol(name, parent, 0);
            else
                subprogram = new FunctionSymbol(name, parent, 0, returnValue);
            ParseProcOptions(subprogram, options);
            if (parameters != null)
                ParseParameters(subprogram, parameters);
            parent.Children.Add(subprogram);
        }

        private void ParseParameters(ProcedureSymbol subprogram, XmlElement parameters)
        {
            foreach (XmlElement param in parameters)
            {
                if (param.Name == "param")
                {
                    ParseParam(subprogram, param);
                }
                else if (param.Name == "array")
                {
                    ParseArraySymbol(subprogram, param);
                }
                else if (param.Name == "set")
                {
                    //TODO: ParseSetSymbol
                }
                else
                    throw new Exception("unexpected param type");
            }
        }

        private void ParseParam(ProcedureSymbol subprogram, XmlElement param)
        {
            string name = string.Empty;
            ITypeSymbol type = null;
            ParameterType paramType = ParameterType.Value;

            foreach (XmlElement child in param.ChildNodes)
            {
                string text = child.InnerText;
                switch (child.Name)
                {
                    case "name":
                        name = text;
                        break;

                    case "vartype":
                        type = GetTypeSymbol(child);
                        break;

                    case "rettype":
                        paramType = (ParameterType)Enum.Parse(typeof(ParameterType), text, ignoreCase: true);
                        break;
                }
            }

            ParameterSymbol symbol = new ParameterSymbol(name, subprogram, type, 0, paramType);
            subprogram.Children.Add(symbol);
        }

        private void ParseProcOptions(ProcedureSymbol subprogram, XmlElement options)
        {
            foreach (XmlElement option in options)
            {
                subprogram.Options.Add((ProcedureOption)Enum.Parse(typeof(ProcedureOption), option.InnerText, ignoreCase: true));
            }
        }

        private void ParseEnumSymbol(UnitSymbol parent, XmlElement element)
        {
            string name = string.Empty;
            int id = -1;
            int size = 0;
            int minValue = 0;
            int maxValue = 0;
            XmlElement elements = null;

            foreach (XmlElement child in element.ChildNodes)
            {
                string text = child.InnerText;
                switch (child.Name)
                {
                    case "name":
                        name = text;
                        break;

                    case "id":
                        id = int.Parse(text);
                        break;

                    case "low":
                        minValue = int.Parse(text);
                        break;

                    case "high":
                        maxValue = int.Parse(text);
                        break;

                    case "size":
                        size = int.Parse(text);
                        break;

                    case "elements":
                        elements = child;
                        break;
                }
            }

            EnumTypeSymbol symbol = new EnumTypeSymbol(parent, 0, name, size, minValue, maxValue);
            ParseEnumElements(symbol, elements);
            parent.Children.Add(symbol);
            AddSymbol(id, symbol);
        }

        private void ParseEnumElements(EnumTypeSymbol symbol, XmlElement elements)
        {
            foreach (XmlElement element in elements)
            {
                ParseConstSymbol(symbol, element);
            }
        }

        private void ParseConstSymbol(ISymbolWithChildren parent, XmlElement element)
        {
            string name = string.Empty;
            ValueType valueType = ValueType.Int;
            string value = string.Empty;
            ITypeSymbol typeSymbol = null;

            foreach (XmlElement child in element.ChildNodes)
            {
                string text = child.InnerText;
                switch (child.Name)
                {
                    case "name":
                        name = text;
                        break;

                    case "valtype":
                        valueType = (ValueType)Enum.Parse(typeof(ValueType), text, ignoreCase: true);
                        break;

                    case "value":
                        value = text;
                        break;

                    case "typeref":
                        typeSymbol = typeSymbols[int.Parse(child.FirstChild.InnerText)] as ITypeSymbol;
                        break;
                }
            }

            ConstantSymbol symbol = new ConstantSymbol(parent, 0, name, valueType, value, typeSymbol);
            parent.Children.Add(symbol);
        }

        private void ParseRecordSymbol(UnitSymbol parent, XmlElement element)
        {
            string name = string.Empty;
            int id = -1;
            int size = 0;
            XmlElement fields = null;

            foreach (XmlElement child in element.ChildNodes)
            {
                string text = child.InnerText;
                switch (child.Name)
                {
                    case "name":
                        name = text;
                        break;

                    case "id":
                        id = int.Parse(text);
                        break;

                    case "size":
                        size = int.Parse(text);
                        break;

                    case "fields":
                        fields = child;
                        break;
                }
            }

            RecordTypeSymbol symbol = new RecordTypeSymbol(parent, 0, name, size);
            if (fields != null)
                ParseRecordMembers(symbol, fields);
            parent.Children.Add(symbol);
            AddSymbol(id, symbol);
        }

        private void ParseRecordMembers(RecordTypeSymbol parent, XmlElement fields)
        {
            foreach (XmlElement field in fields)
            {
                string name = string.Empty;
                int typeId = -1;
                int unit = -1;

                foreach (XmlElement child in field.ChildNodes)
                {
                    string text = child.InnerText;
                    switch (child.Name)
                    {
                        case "name":
                            name = text;
                            break;

                        case "vartype":
                            GetIdAndUnit(child, out typeId, out unit);
                            break;
                    }
                }

                if (unit != -1)
                {
                    ITypeSymbol type = dependencySymbols[unit][typeId];
                    VariableSymbol var = new VariableSymbol(name, parent, type, 0);
                    parent.Children.Add(var);
                    continue;
                }

                if (typeSymbols.ContainsKey(typeId))
                {
                    ITypeSymbol type = typeSymbols[typeId] as ITypeSymbol;
                    VariableSymbol var = new VariableSymbol(name, parent, type, 0);
                    parent.Children.Add(var);
                }
                else
                {
                    symbolAdded += id =>
                    {
                        if (id != typeId) return;
                        ITypeSymbol type = typeSymbols[typeId] as ITypeSymbol;
                        VariableSymbol var = new VariableSymbol(name, parent, type, 0);
                        parent.Children.Add(var);
                    };
                }
            }
        }

        private void ParseFileSymbol(UnitSymbol parent, XmlElement element)
        {
            string name = string.Empty;
            int id = -1;
            FileType type = FileType.Text;
            ITypeSymbol baseType = null;

            foreach (XmlElement child in element.ChildNodes)
            {
                string text = child.InnerText;
                switch (child.Name)
                {
                    case "name":
                        name = text;
                        break;

                    case "id":
                        id = int.Parse(text);
                        break;

                    case "filetype":
                        type = (FileType)Enum.Parse(typeof(FileType), text, ignoreCase: true);
                        break;

                    case "typeref":
                        baseType = typeSymbols[int.Parse(child.FirstChild.InnerText)] as ITypeSymbol;
                        break;
                }
            }

            FileTypeSymbol symbol = new FileTypeSymbol(parent, 0, name, type, baseType);
            parent.Children.Add(symbol);
            AddSymbol(id, symbol);
        }

        private void ParseArraySymbol(ISymbolWithChildren parent, XmlElement element)
        {
            string name = string.Empty;
            int id = -1;
            ITypeSymbol elementType = null;
            ITypeSymbol indexerType = null;
            string lowerBound = string.Empty;
            string upperBound = string.Empty;

            foreach (XmlElement child in element.ChildNodes)
            {
                string text = child.InnerText;
                switch (child.Name)
                {
                    case "name":
                        name = text;
                        break;

                    case "id":
                        id = int.Parse(text);
                        break;

                    case "types":
                        ParseOrdSymbol(parent, child.FirstChild as XmlElement);
                        break;

                    case "eltype":
                        elementType = GetTypeSymbol(child);
                        break;

                    case "rangetype":
                        indexerType = GetTypeSymbol(child);
                        break;

                    case "low":
                        lowerBound = text;
                        break;

                    case "high":
                        upperBound = text;
                        break;
                }
            }
            Range[] ranges = (lowerBound.Length == 0 && upperBound.Length == 0) ? new Range[0] : new[] { new Range { LowerBound = lowerBound, UpperBound = upperBound } };

            ArrayTypeSymbol symbol = new ArrayTypeSymbol(parent, 0, name, elementType, indexerType, ranges);
            parent.Children.Add(symbol);
            AddSymbol(id, symbol);
        }

        private void ParseFloatSymbol(UnitSymbol parent, XmlElement element)
        {
            string name = string.Empty;
            int id = -1;
            FloatType type = FloatType.Single;

            foreach (XmlElement child in element.ChildNodes)
            {
                string text = child.InnerText;
                switch (child.Name)
                {
                    case "name":
                        name = text;
                        break;

                    case "id":
                        id = int.Parse(text);
                        break;

                    case "floattype":
                        type = (FloatType)Enum.Parse(typeof(FloatType), text, ignoreCase: true);
                        break;
                }
            }

            FloatTypeSymbol symbol = new FloatTypeSymbol(parent, 0, name, type);
            parent.Children.Add(symbol);
            AddSymbol(id, symbol);
        }

        private void ParseOrdSymbol(ISymbolWithChildren parent, XmlElement element)
        {
            string name = string.Empty;
            int id = -1;
            OrdinalType type = OrdinalType.Void;
            int size = 0;
            string minValue = string.Empty;
            string maxValue = string.Empty;

            foreach (XmlElement child in element.ChildNodes)
            {
                string text = child.InnerText;
                switch (child.Name)
                {
                    case "name":
                        name = text;
                        break;

                    case "id":
                        id = int.Parse(text);
                        break;

                    case "ordtype":
                        type = (OrdinalType)Enum.Parse(typeof(OrdinalType), text, ignoreCase: true);
                        break;

                    case "size":
                        size = int.Parse(text);
                        break;

                    case "low":
                        minValue = text;
                        break;

                    case "high":
                        maxValue = text;
                        break;
                }
            }

            OrdinalTypeSymbol symbol = new OrdinalTypeSymbol(parent, 0, name, size, minValue, maxValue, type);
            parent.Children.Add(symbol);
            typeSymbols[id] = symbol;
        }

        private void ParsePtrSymbol(UnitSymbol parent, XmlElement element)
        {
            string name = string.Empty;
            int id = -1;
            int baseTypeId = -1;
            int unit = -1;

            foreach (XmlElement child in element.ChildNodes)
            {
                string text = child.InnerText;
                switch (child.Name)
                {
                    case "name":
                        name = text;
                        break;

                    case "id":
                        id = int.Parse(text);
                        break;

                    case "ptr":
                        GetIdAndUnit(child, out baseTypeId, out unit);
                        break;
                }
            }

            if (unit != -1)
            {
                ITypeSymbol baseType = dependencySymbols[unit][baseTypeId];
                PointerTypeSymbol symbol = new PointerTypeSymbol(parent, 0, name, baseType);
                parent.Children.Add(symbol);
                AddSymbol(id, symbol);
                return;
            }

            if (typeSymbols.ContainsKey(baseTypeId))
            {
                ITypeSymbol baseType = typeSymbols[baseTypeId];
                PointerTypeSymbol symbol = new PointerTypeSymbol(parent, 0, name, baseType);
                parent.Children.Add(symbol);
                AddSymbol(id, symbol);
            }
            else
            {
                symbolAdded += symId =>
                {
                    if (symId != baseTypeId) return;
                    ITypeSymbol baseType = typeSymbols[baseTypeId];
                    PointerTypeSymbol symbol = new PointerTypeSymbol(parent, 0, name, baseType);
                    parent.Children.Add(symbol);
                    AddSymbol(id, symbol);
                };
            }
        }

        private void ParseStringSymbol(UnitSymbol parent, XmlElement element)
        {
            string name = string.Empty;
            int id = -1;
            StringType type = StringType.Short;
            int length = 0;

            foreach (XmlElement child in element.ChildNodes)
            {
                string text = child.InnerText;
                switch (child.Name)
                {
                    case "name":
                        name = text;
                        break;

                    case "id":
                        id = int.Parse(text);
                        break;

                    case "strtype":
                        type = (StringType)Enum.Parse(typeof(StringType), text, ignoreCase: true);
                        break;

                    case "len":
                        length = int.Parse(text);
                        break;
                }
            }

            StringTypeSymbol symbol = new StringTypeSymbol(parent, 0, name, length, type);
            parent.Children.Add(symbol);
            typeSymbols[id] = symbol;
        }

        private string GetName(XmlElement element)
        {
            var name = element.GetElementsByTagName("name")[0];

            return name?.FirstChild?.Value;
        }

        private int GetId(XmlElement element)
        {
            XmlElement id = element.GetElementsByTagName("name")[0] as XmlElement;

            return int.Parse(id.Value);
        }

        private void ParseVarSymbol(UnitSymbol parent, XmlElement element)
        {
            string name = element.GetElementsByTagName("name")[0].InnerText;
            ITypeSymbol type = GetTypeSymbol(element.GetElementsByTagName("vartype")[0] as XmlElement);

            parent.Children.Add(new VariableSymbol(name, parent, type, 0));
        }
    }
}