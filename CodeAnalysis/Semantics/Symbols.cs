﻿using System.Collections.Generic;

namespace CodeAnalysis.Semantics
{
    public enum SymbolKind { Variable, Procedure, Function, Type, Program, Constant, Unit, Parameter }

    public interface ISymbol
    {
        SymbolKind SymbolKind { get; }
        string Name { get; }
        ISymbolWithChildren ContainingSymbol { get; }

        //Point after which this symbol can be used
        int DeclarationPosition { get; }
    }

    public interface ISymbolWithChildren : ISymbol
    {
        ICollection<ISymbol> Children { get; }
    }

    public interface ISymbolWithChildren<TChildren> : ISymbolWithChildren where TChildren : ISymbol
    {
        new ICollection<TChildren> Children { get; }
    }

    public enum TypeKind
    {
        Ordinal, Float, String, Pointer, Array, Record, File,
        Enum,
        Alias,
        Set,
        Unknown
    }

    public interface ITypeSymbol : ISymbol
    {
        TypeKind Kind { get; }

        ITypeSymbol Clone(string newName, ISymbolWithChildren newParent, int newDeclarationPosition);
    }

    public interface ITypedSymbol : ISymbol
    {
        ITypeSymbol Type { get; }
    }

    public abstract class BaseSymbol : ISymbol
    {
        public abstract ISymbolWithChildren ContainingSymbol { get; }

        public abstract int DeclarationPosition { get; }

        public abstract string Name { get; }

        public abstract SymbolKind SymbolKind { get; }

        public override string ToString()
        {
            return string.Format("{0}: {1}", Name, SymbolKind);
        }
    }

    public abstract class SubprogramSymbol : BaseSymbol, ISymbolWithChildren
    {
        public ICollection<ISymbol> Children { get; }

        public override ISymbolWithChildren ContainingSymbol { get; }

        public override string Name { get; }

        public SubprogramSymbol(string name, ISymbolWithChildren containingSymbol)
        {
            Children = new List<ISymbol>();
            Name = name;
            ContainingSymbol = containingSymbol;
        }
    }

    public class ProgramSymbol : SubprogramSymbol
    {
        public override SymbolKind SymbolKind => SymbolKind.Program;

        public override int DeclarationPosition => 0;

        public ProgramSymbol(string name)
            : base(name, null)
        {
        }
    }

    public class VariableSymbol : BaseSymbol, ITypedSymbol
    {
        public override ISymbolWithChildren ContainingSymbol { get; }

        public override string Name { get; }

        public override SymbolKind SymbolKind => SymbolKind.Variable;

        public ITypeSymbol Type { get; }

        public override int DeclarationPosition { get; }

        public VariableSymbol(string name, ISymbolWithChildren containingSymbol, ITypeSymbol type, int declarationPosition)
        {
            ContainingSymbol = containingSymbol;
            Name = name;
            Type = type;
            DeclarationPosition = declarationPosition;
        }
    }

    public enum ValueType
    {
        Int, String, Float, Set, Pointer, Array, Record, Undefined, Bool,
        Enum
    }

    public class ConstantSymbol : BaseSymbol
    {
        public override ISymbolWithChildren ContainingSymbol { get; }

        public override int DeclarationPosition { get; }

        public override string Name { get; }

        public override SymbolKind SymbolKind => SymbolKind.Constant;

        public ValueType Type { get; }

        public string Value { get; }

        public ITypeSymbol TypeSymbol { get; }

        public ConstantSymbol(ISymbolWithChildren containingSymbol, int declarationPosition, string name,
            ValueType type, string value, ITypeSymbol typeSymbol)
        {
            ContainingSymbol = containingSymbol;
            DeclarationPosition = declarationPosition;
            Name = name;
            Type = type;
            Value = value;
            TypeSymbol = typeSymbol;
        }
    }

    public class UnitSymbol : SubprogramSymbol
    {
        public UnitSymbol(string name) : base(name, null)
        {
        }

        public Dictionary<int, ITypeSymbol> TypeSymbols { get; set; }

        public override int DeclarationPosition => 0;

        public override SymbolKind SymbolKind => SymbolKind.Unit;
    }

    public enum ParameterType { Value, Const, Var, Out }

    public class ParameterSymbol : VariableSymbol
    {
        public ParameterType ParamType { get; }
        public override SymbolKind SymbolKind => SymbolKind.Parameter;

        public ParameterSymbol(string name, ISymbolWithChildren containingSymbol, ITypeSymbol type, int declarationPosition, ParameterType paramType)
            : base(name, containingSymbol, type, declarationPosition)
        {
            ParamType = paramType;
        }
    }

    public enum ProcedureOption { Procedure, Inline, Function, Operator, Overload }

    public class ProcedureSymbol : SubprogramSymbol
    {
        public override int DeclarationPosition { get; }

        public override SymbolKind SymbolKind => SymbolKind.Procedure;

        public ICollection<ProcedureOption> Options { get; } = new List<ProcedureOption>();

        public ProcedureSymbol(string name, ISymbolWithChildren containingSymbol, int declarationPosition) : base(name, containingSymbol)
        {
            DeclarationPosition = declarationPosition;
        }
    }

    public class FunctionSymbol : ProcedureSymbol
    {
        public override SymbolKind SymbolKind => SymbolKind.Function;
        public ITypeSymbol ReturnType { get; }

        public FunctionSymbol(string name, ISymbolWithChildren containingSymbol, int declarationPosition, ITypeSymbol returnType)
            : base(name, containingSymbol, declarationPosition)
        {
            ReturnType = returnType;
        }
    }
}