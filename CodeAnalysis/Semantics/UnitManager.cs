﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CodeAnalysis.Semantics
{
    public class UnitManager
    {
        private struct UnitDetails
        {
            public DateTime LastModified { get; set; }
            public UnitSymbol Symbol { get; set; }
        }

        public readonly string PpuDumpExecutable;
        private readonly string StandardLibraryFolder;

        /// <summary>
        /// Key - unit name, Value - unit path
        /// </summary>
        private Dictionary<string, string> unitPositions = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);

        /// <summary>
        /// file paths are the key
        /// </summary>
        private Dictionary<string, UnitDetails> cachedUnitSymbols = new Dictionary<string, UnitDetails>();

        public UnitManager(string ppuDumpExecutable, string standardLibraryFolder)
        {
            PpuDumpExecutable = ppuDumpExecutable;
            StandardLibraryFolder = standardLibraryFolder;

            if (!File.Exists(ppuDumpExecutable) || !Directory.Exists(standardLibraryFolder))
                return;

            InitStandardLibrary();
        }

        private void InitStandardLibrary()
        {
            var unitFiles = Directory
                .EnumerateDirectories(StandardLibraryFolder)
                .SelectMany(dir => Directory.EnumerateFiles(dir))
                .Where(file => Path.GetExtension(file) == ".ppu");

            foreach (var file in unitFiles)
            {
                unitPositions.Add(Path.GetFileNameWithoutExtension(file), file);
            }
        }

        public UnitSymbol LoadUnit(string unitName)
        {
            if (unitPositions.ContainsKey(unitName))
                return LoadUnitFromFile(unitName, unitPositions[unitName]);
            else
                return null;
        }

        public UnitSymbol LoadUnitIn(string unitName, string fileName)
        {
            return LoadUnitFromFile(unitName, fileName);
        }

        private UnitSymbol LoadUnitFromFile(string unitName, string path)
        {
            if (!File.Exists(path) || !File.Exists(PpuDumpExecutable))
                return null;

            DateTime lastModified = File.GetLastWriteTime(path);
            if (cachedUnitSymbols.ContainsKey(path))
            {
                if (cachedUnitSymbols[path].LastModified == lastModified)
                {
                    return cachedUnitSymbols[path].Symbol;
                }
                else
                    cachedUnitSymbols[path] = new UnitDetails { Symbol = new UnitSymbol(unitName), LastModified = lastModified };
            }
            else
            {
                cachedUnitSymbols.Add(path, new UnitDetails { Symbol = new UnitSymbol(unitName), LastModified = lastModified });
            }

            UnitSymbol symbol = cachedUnitSymbols[path].Symbol;
            UnitLoader loader = new UnitLoader(path, this);
            loader.Load(symbol);

            return symbol;
        }
    }
}