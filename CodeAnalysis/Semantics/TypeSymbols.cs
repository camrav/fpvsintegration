﻿using CodeAnalysis.Semantics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;

namespace ProjectSystem
{
    public abstract class TypeSymbol : BaseSymbol, ITypeSymbol
    {
        public override ISymbolWithChildren ContainingSymbol { get; }

        public override string Name { get; }

        public override SymbolKind SymbolKind => SymbolKind.Type;

        public override int DeclarationPosition { get; }

        public abstract TypeKind Kind { get; }

        public TypeSymbol(ISymbolWithChildren containingSymbol, int declarationPosition, string name)
        {
            ContainingSymbol = containingSymbol;
            Name = name;
            DeclarationPosition = declarationPosition;
        }

        public override string ToString()
        {
            return Name;
        }

        public abstract ITypeSymbol Clone(string newName, ISymbolWithChildren newParent, int newDeclarationPosition);
    }

    public class UnknownTypeSymbol : TypeSymbol
    {
        public UnknownTypeSymbol(ISymbolWithChildren containingSymbol, int declarationPosition, string name) 
            : base(containingSymbol, declarationPosition, name)
        {
        }

        public override TypeKind Kind => TypeKind.Unknown;

        public override ITypeSymbol Clone(string newName, ISymbolWithChildren newParent, int newDeclarationPosition)
        {
            return new UnknownTypeSymbol(newParent, newDeclarationPosition, newName);
        }
    }

    public enum OrdinalType { Void, Uint, Sint, Pasbool, Bool, Char }

    public class OrdinalTypeSymbol : TypeSymbol
    {
        public int Size { get; }
        public string MinValue { get; }
        public string MaxValue { get; }
        public OrdinalType Type { get; }
        public override TypeKind Kind => TypeKind.Ordinal;

        public OrdinalTypeSymbol(ISymbolWithChildren containingSymbol, int declarationPosition, string name,
            int size, string minValue, string maxValue, OrdinalType type)
            : base(containingSymbol, declarationPosition, name)
        {
            Size = size;
            MinValue = minValue;
            MaxValue = maxValue;
            Type = type;
        }

        public override ITypeSymbol Clone(string newName, ISymbolWithChildren newParent, int newDeclarationPosition)
        {
            return new OrdinalTypeSymbol(newParent, newDeclarationPosition, newName, Size, MinValue, MaxValue, Type);
        }
    }

    public enum StringType { Short, Long, Ansi, Wide, Unicode }

    public class StringTypeSymbol : TypeSymbol
    {
        public int Length { get; }
        public StringType Type { get; }
        public override TypeKind Kind => TypeKind.String;

        public StringTypeSymbol(ISymbolWithChildren containingSymbol, int declarationPosition, string name,
            int length, StringType type)
            : base(containingSymbol, declarationPosition, name)
        {
            Length = length;
            Type = type;
        }

        public override ITypeSymbol Clone(string newName, ISymbolWithChildren newParent, int newDeclarationPosition)
        {
            return new StringTypeSymbol(newParent, newDeclarationPosition, newName, Length, Type);
        }

        public override string ToString()
        {
            if (!string.IsNullOrEmpty(Name))
                return base.ToString();

            if (Type == StringType.Short)
            {
                if (Length == 0xff)
                    return "string";
                else
                    return string.Format("string[{0}]", Length);
            }
            else
                return string.Format("{0}string", Type);
        }
    }

    public enum FloatType { Single, Double, Extended, Comp, Float128, Currency }

    public class FloatTypeSymbol : TypeSymbol
    {
        public FloatType Type { get; }
        public override TypeKind Kind => TypeKind.Float;

        public FloatTypeSymbol(ISymbolWithChildren containingSymbol, int declarationPosition, string name, FloatType type)
            : base(containingSymbol, declarationPosition, name)
        {
            Type = type;
        }

        public override ITypeSymbol Clone(string newName, ISymbolWithChildren newParent, int newDeclarationPosition)
        {
            return new FloatTypeSymbol(newParent, newDeclarationPosition, newName, Type);
        }
    }

    public struct Range
    {
        public string LowerBound { get; set; }
        public string UpperBound { get; set; }
    }

    public class ArrayTypeSymbol : TypeSymbol
    {
        public ITypeSymbol ElementType { get; }
        public ITypeSymbol IndexerType { get; }

        public Range[] Ranges { get; }
        public override TypeKind Kind => TypeKind.Array;

        public ArrayTypeSymbol(ISymbolWithChildren containingSymbol, int declarationPosition, string name,
            ITypeSymbol elementType, ITypeSymbol indexerType, params Range[] ranges)
            : base(containingSymbol, declarationPosition, name)
        {
            ElementType = elementType;
            IndexerType = indexerType;
            Ranges = ranges;
        }

        public override string ToString()
        {
            if (string.IsNullOrEmpty(Name))
            {
                string ranges;
                if (Ranges.Length == 0)
                    ranges = string.Empty;
                else
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("[");
                    sb.AppendFormat("{0}..{1}", Ranges[0].LowerBound, Ranges[0].UpperBound);
                    for (int i = 1; i < Ranges.Length; i++)
                    {
                        sb.AppendFormat(", {0}..{1}", Ranges[i].LowerBound, Ranges[i].UpperBound);
                    }
                    sb.Append("] ");
                    ranges = sb.ToString();
                }

                return string.Format("Array {0}of {1}", ranges, ElementType.ToString());
            }
            else
                return base.ToString();
        }

        public override ITypeSymbol Clone(string newName, ISymbolWithChildren newParent, int newDeclarationPosition)
        {
            return new ArrayTypeSymbol(newParent, newDeclarationPosition, newName, ElementType, IndexerType, Ranges);
        }
    }

    public class PointerTypeSymbol : TypeSymbol
    {
        public override TypeKind Kind => TypeKind.Pointer;
        public ITypeSymbol BaseType { get; set; }

        public PointerTypeSymbol(ISymbolWithChildren containingSymbol, int declarationPosition, string name, ITypeSymbol baseType)
            : base(containingSymbol, declarationPosition, name)
        {
            BaseType = baseType;
        }

        public override string ToString()
        {
            if (string.IsNullOrEmpty(Name))
                return string.Format("{0}^", BaseType.ToString());
            else
                return base.ToString();
        }

        public override ITypeSymbol Clone(string newName, ISymbolWithChildren newParent, int newDeclarationPosition)
        {
            return new PointerTypeSymbol(newParent, newDeclarationPosition, newName, BaseType);
        }
    }

    public enum FileType { Untyped, Text, Typed }

    public class FileTypeSymbol : TypeSymbol
    {
        public override TypeKind Kind => TypeKind.File;
        public FileType Type { get; }
        public ITypeSymbol BaseType { get; }

        public FileTypeSymbol(ISymbolWithChildren containingSymbol, int declarationPosition, string name,
            FileType type, ITypeSymbol baseType = null)
           : base(containingSymbol, declarationPosition, name)
        {
            Type = type;
            BaseType = baseType;
        }

        public override string ToString()
        {
            if (string.IsNullOrEmpty(Name))
                return string.Format("file of {0}", BaseType.ToString());
            else
                return base.ToString();
        }

        public override ITypeSymbol Clone(string newName, ISymbolWithChildren newParent, int newDeclarationPosition)
        {
            return new FileTypeSymbol(newParent, newDeclarationPosition, newName, Type, BaseType);
        }
    }

    public class RecordTypeSymbol : TypeSymbol, ISymbolWithChildren
    {
        public override TypeKind Kind => TypeKind.Record;
        public int Size { get; }

        public ICollection<VariableSymbol> Members => Children.Cast<VariableSymbol>().ToList();

        public ICollection<ISymbol> Children { get; } = new List<ISymbol>();

        public RecordTypeSymbol(ISymbolWithChildren containingSymbol, int declarationPosition, string name, int size)
            : base(containingSymbol, declarationPosition, name)
        {
            Size = size;
        }

        public override string ToString()
        {
            if (string.IsNullOrEmpty(Name))
                return "Record";
            else
                return base.ToString();
        }

        public override ITypeSymbol Clone(string newName, ISymbolWithChildren newParent, int newDeclarationPosition)
        {
            var result = new RecordTypeSymbol(newParent, newDeclarationPosition, newName, Size);
            foreach (var child in Children)
            {
                result.Children.Add(child);
            }
            return result;
        }
    }

    public class EnumTypeSymbol : TypeSymbol, ISymbolWithChildren<ConstantSymbol>
    {
        public override TypeKind Kind => TypeKind.Enum;
        public int Size { get; }
        public int LowestValue { get; }
        public int HighestValue { get; }

        ICollection<ISymbol> ISymbolWithChildren.Children { get; } = new List<ISymbol>();
        public ICollection<ConstantSymbol> Children { get; } = new List<ConstantSymbol>();

        public EnumTypeSymbol(ISymbolWithChildren containingSymbol, int declarationPosition, string name,
            int size, int lowestValue, int highestValue)
            : base(containingSymbol, declarationPosition, name)
        {
            Size = size;
            LowestValue = lowestValue;
            HighestValue = highestValue;
        }

        public override ITypeSymbol Clone(string newName, ISymbolWithChildren newParent, int newDeclarationPosition)
        {
            var result = new EnumTypeSymbol(newParent, newDeclarationPosition, newName, Size, LowestValue, HighestValue);
            foreach (var child in Children)
            {
                result.Children.Add(child);
            }
            return result;
        }
    }

    public class AliasTypeSymbol : TypeSymbol
    {
        public override TypeKind Kind => TypeKind.Alias;
        public ITypeSymbol BaseType { get; }

        public AliasTypeSymbol(ISymbolWithChildren containingSymbol, int declarationPosition, string name, ITypeSymbol baseSymbol)
            : base(containingSymbol, declarationPosition, name)
        {
            BaseType = baseSymbol;
        }

        public override ITypeSymbol Clone(string newName, ISymbolWithChildren newParent, int newDeclarationPosition)
        {
            return new AliasTypeSymbol(newParent, newDeclarationPosition, newName, BaseType);
        }
    }

    public class SetTypeSymbol : TypeSymbol
    {
        public override TypeKind Kind => TypeKind.Set;
        public ITypeSymbol BaseType { get; }

        public SetTypeSymbol(ISymbolWithChildren containingSymbol, int declarationPosition, string name, ITypeSymbol baseSymbol)
            : base(containingSymbol, declarationPosition, name)
        {
            BaseType = baseSymbol;
        }

        public override ITypeSymbol Clone(string newName, ISymbolWithChildren newParent, int newDeclarationPosition)
        {
            return new SetTypeSymbol(newParent, newDeclarationPosition, newName, BaseType);
        }
    }
}