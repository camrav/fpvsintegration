﻿using CodeAnalysis.SyntaxTree.OnDemandTree;
using FpLexer;
using ProjectSystem;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CodeAnalysis.Semantics
{
    public class SemanticModel
    {
        public FpSyntaxTree SourceTree { get; }
        private Dictionary<FpSyntaxNode, ISymbolWithChildren> allSubprograms = new Dictionary<FpSyntaxNode, ISymbolWithChildren>();
        private UnitManager unitManager;

        public SemanticModel(FpSyntaxTree tree, UnitManager manager)
        {
            SourceTree = tree;
            unitManager = manager;

            InitSymbols();
        }

        private void InitSymbols()
        {
            FpSyntaxNode root = (SourceTree.Root as FpSyntaxNode);
            if (root.Kind == SyntaxKind.ProgramRoot)
            {
                ProgramSymbol symbol = new ProgramSymbol((root as FpProgramRootNode).ProgramName.Text);
                allSubprograms.Add(root, symbol);
                new SymbolLoader(this, unitManager, allSubprograms).InitProgramRoot(symbol, root as FpProgramRootNode);
            }
        }

        public IEnumerable<ISymbol> GetAvailableSymbols(FpSyntaxNode position)
        {
            FpSyntaxNode originalNode = position;

            while (!allSubprograms.ContainsKey(position))
            {
                position = position.Parent as FpSyntaxNode;
            }

            var subprogram = allSubprograms[position];

            List<ISymbol> result = new List<ISymbol>();
            //Dictionary<string, ISymbol> result = new Dictionary<string, ISymbol>(StringComparer.OrdinalIgnoreCase);
            while (subprogram != null)
            {
                foreach (var symbol in GetChildrenSymbols(subprogram, originalNode.Start))
                {
                    result.Add(symbol);
                }
                subprogram = subprogram.ContainingSymbol;
            }

            return result;
        }

        private IEnumerable<ISymbol> GetChildrenSymbols(ISymbolWithChildren subprogram, int maxPosition)
        {
            return
                subprogram
                .Children
                .Where(symbol => symbol.DeclarationPosition < maxPosition)
                .SelectMany(symbol =>
                {
                    if (symbol.SymbolKind == SymbolKind.Unit)
                        return (symbol as ISymbolWithChildren).Children;
                    else if (symbol is EnumTypeSymbol)
                    {
                        var sym = symbol as EnumTypeSymbol;
                        List<ISymbol> result = new List<ISymbol>(sym.Children);
                        result.Add(sym);
                        return result;
                    }
                    else
                        return new[] { symbol };
                });
        }

        public ISymbol GetSymbol(FpSyntaxIdentifier identifier)
        {
            return GetAvailableSymbols(identifier).Where(sym => string.Equals(sym.Name, identifier.Text, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
        }

        private IEnumerable<ISymbol> GetAvailableSymbols(ISymbolWithChildren symbol, ISymbol stopSymbol)
        {
            var availableSymbols = symbol.Children.TakeWhile(c => c != stopSymbol);

            return symbol.ContainingSymbol == null ? availableSymbols : availableSymbols.Concat(GetAvailableSymbols(symbol.ContainingSymbol, symbol));
        }
    }
}