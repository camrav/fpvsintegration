﻿using FpLexer;

namespace CodeAnalysis
{
    public class StringSourceText : ISourceText
    {
        private string text;

        public StringSourceText(string text)
        {
            this.text = text;
        }

        public string GetText()
        {
            return text;
        }
    }
}