﻿using FpLexer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CodeAnalysis.Grammar
{
    [AttributeUsage(AttributeTargets.Method, Inherited = false, AllowMultiple = true)]
    internal sealed class ProductionAttribute : Attribute
    {
        public SyntaxKind LeftSide { get; }
        public SyntaxKind[] RightSides { get; }

        public ProductionAttribute(SyntaxKind leftSide, params SyntaxKind[] rightSides)
        {
            LeftSide = leftSide;
            RightSides = rightSides;
        }
    }

    internal class FpProductionProvider
    {
        private Type attributedClass = typeof(FpParser);

        public IEnumerable<Production> GetProductions()
        {
            return attributedClass
                .GetMethods(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static)
                .SelectMany(method => method.GetCustomAttributes(typeof(ProductionAttribute), false))
                .Select(attribute => attribute as ProductionAttribute)
                .Select(attribute => new Production(attribute.LeftSide, attribute.RightSides));
        }
    }
}