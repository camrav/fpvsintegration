﻿using FpLexer;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CodeAnalysis.Grammar
{
    public class FpGrammar
    {
        private const SyntaxKind StartSymbol = SyntaxKind.ProgramRoot; //TODO: replace the start symbol, to allow for units, libraries
        private Dictionary<SyntaxKind, List<Production>> productions = new Dictionary<SyntaxKind, List<Production>>();
        private Dictionary<SyntaxKind, string> stringAliases;

        private Dictionary<SyntaxKind, HashSet<SyntaxKind>> firsts = new Dictionary<SyntaxKind, HashSet<SyntaxKind>>();
        private Dictionary<SyntaxKind, HashSet<SyntaxKind>> follows = new Dictionary<SyntaxKind, HashSet<SyntaxKind>>();

        public FpGrammar()
        {
            InitGrammar();
            InitStringAliases();

            CalculateFirsts();
            CalculateFollows();
        }

        private void InitGrammar()
        {
            FpProductionProvider provider = new FpProductionProvider();

            foreach (var production in provider.GetProductions())
            {
                if (!productions.ContainsKey(production.LeftSide))
                    productions.Add(production.LeftSide, new List<Production>());

                productions[production.LeftSide].Add(production);
            }
        }

        private void InitStringAliases()
        {
            stringAliases = new Dictionary<SyntaxKind, string>
            {
                {SyntaxKind.BeginToken, "begin" },
                {SyntaxKind.EndToken, "end" },
                {SyntaxKind.ProgramToken, "program" },
                {SyntaxKind.VarToken, "var" },
                {SyntaxKind.IfToken, "if" },
                {SyntaxKind.DivToken, "div" },
                {SyntaxKind.ForToken, "for" },
                {SyntaxKind.RepeatToken, "repeat" },
                {SyntaxKind.ProcedureToken, "procedure" },
                {SyntaxKind.FunctionToken, "function" },
                {SyntaxKind.TypeToken, "type" },
                {SyntaxKind.UsesToken, "uses" },
                {SyntaxKind.InToken, "in" },
                {SyntaxKind.AsmToken, "asm" },
                {SyntaxKind.ThenToken, "then" },
                {SyntaxKind.ElseToken, "else" },
                {SyntaxKind.ToToken, "to" },
                {SyntaxKind.DowntoToken, "downto" },
                {SyntaxKind.DoToken, "do" },
                {SyntaxKind.UntilToken, "until" },
                {SyntaxKind.WhileToken, "while" },
                {SyntaxKind.WithToken, "with" },
                {SyntaxKind.GotoToken, "goto" },
                {SyntaxKind.CaseToken, "case" },
                {SyntaxKind.OfToken, "of" },
                {SyntaxKind.OtherwiseToken, "otherwise" },
                {SyntaxKind.IsToken, "is" },
                {SyntaxKind.OrToken, "or" },
                {SyntaxKind.XorToken, "xor" },
                {SyntaxKind.ModToken, "mod" },
                {SyntaxKind.AndToken, "and" },
                {SyntaxKind.ShlToken, "shl" },
                {SyntaxKind.ShrToken, "shr" },
                {SyntaxKind.AsToken, "as" },
                {SyntaxKind.NotToken, "not" },
                {SyntaxKind.LabelToken, "label" },
                {SyntaxKind.ConstToken, "const" },
                {SyntaxKind.ArrayToken, "array" },
                {SyntaxKind.RecordToken, "record" },
                {SyntaxKind.OutToken, "out" },
                {SyntaxKind.FileToken, "file" },
                {SyntaxKind.SetToken, "set" },
                {SyntaxKind.StringToken, "string" },

                // modifiers
                {SyntaxKind.DeprecatedToken, "deprecated" },
                {SyntaxKind.ExperimentalToken, "experimental" },
                {SyntaxKind.PlatformToken, "platform" },
                {SyntaxKind.UnimplementedToken, "unimplemented" },
                {SyntaxKind.AbsoluteToken, "absolute" },
                {SyntaxKind.ExportToken, "export" },
                {SyntaxKind.CvarToken, "cvar" },
                {SyntaxKind.ExternalToken, "external" },
                {SyntaxKind.NameToken, "name" },
                {SyntaxKind.ForwardToken, "forward" },
                {SyntaxKind.AssemblerToken, "assembler" },
                {SyntaxKind.IndexToken, "index" },
                {SyntaxKind.AnsistringToken, "ansistring" },

                {SyntaxKind.DotToken, "." },
                {SyntaxKind.ColonToken, ":" },
                {SyntaxKind.SemicolonToken, ";" },
                {SyntaxKind.CommaToken, "," },
                {SyntaxKind.OpenBracketToken, "[" },
                {SyntaxKind.CloseBracketToken, "]"},
                {SyntaxKind.OpenParanthesisToken, "(" },
                {SyntaxKind.CloseParanthesisToken, ")" },
                {SyntaxKind.RoofToken, "^" },
                {SyntaxKind.AtToken, "@" },
                {SyntaxKind.DotDotToken, ".." },

                {SyntaxKind.AssignToken, ":=" },
                {SyntaxKind.PlusToken, "+" },
                {SyntaxKind.MinusToken, "-" },
                {SyntaxKind.AsteriskToken, "*" },
                {SyntaxKind.SlashToken, "/" },
                {SyntaxKind.EqualsToken, "=" },
                {SyntaxKind.DoesNotEqualToken, "<>" },
                {SyntaxKind.PlusEqualsToken, "+=" },
                {SyntaxKind.MinusEqualsToken, "-=" },
                {SyntaxKind.AsteriskEqualsToken, "*=" },
                {SyntaxKind.SlashEqualsToken, "/=" },
                {SyntaxKind.LessThanToken, "<" },
                {SyntaxKind.GreaterThanToken, ">" },
                {SyntaxKind.LessOrEqualThanToken, "<=" },
                {SyntaxKind.GreaterOrEqualThanToken, ">=" },
                {SyntaxKind.LeftShiftToken, "<<" },
                {SyntaxKind.RightShiftToken, ">>" }
            };
        }

        private void CalculateFirsts()
        {
            foreach (var kind in productions.Keys) //adds all nonterminals
            {
                firsts.Add(kind, new HashSet<SyntaxKind>());
            }

            foreach (SyntaxKind kind in Enum.GetValues(typeof(SyntaxKind)))
            {
                if (kind.IsTerminal())
                {
                    firsts.Add(kind, new HashSet<SyntaxKind> { kind });
                }
            }

            bool wasChanged;
            do
            {
                wasChanged = false;

                foreach (var productionList in productions)
                {
                    foreach (var production in productionList.Value)
                    {
                        wasChanged |= CalculateFirstsFromProduction(production);
                    }
                }
            } while (wasChanged);
        }

        private bool CalculateFirstsFromProduction(Production production)
        {
            if (production.RightSide.Length == 0)
            {
                return firsts[production.LeftSide].Add(SyntaxKind.None); //add returns true if object is successfully added
            }

            bool added = false;
            int i = 0;
            for (; i < production.RightSide.Length; i++)
            {
                int originalCount = firsts[production.LeftSide].Count;

                firsts[production.LeftSide].UnionWith(firsts[production.RightSide[i]].Except(new[] { SyntaxKind.None }));
                added |= originalCount != firsts[production.LeftSide].Count;

                if (!firsts[production.RightSide[i]].Contains(SyntaxKind.None))
                    break;
            }

            if (i == production.RightSide.Length) //all nonterminals on right side reduce to lambda
            {
                firsts[production.LeftSide].Add(SyntaxKind.None);
            }

            return added;
        }

        private void CalculateFollows()
        {
            InitFollows();

            bool wasChanged;
            do
            {
                wasChanged = false;

                foreach (var productionList in productions)
                {
                    foreach (var production in productionList.Value)
                    {
                        wasChanged |= CalculateFollowsFromProduction(production);
                    }
                }
            } while (wasChanged);
        }

        private void InitFollows()
        {
            foreach (var kind in firsts.Keys)
            {
                follows.Add(kind, new HashSet<SyntaxKind>());
            }

            follows[StartSymbol].Add(SyntaxKind.EndOfFile);

            foreach (var production in productions.Values.SelectMany(list => list))
            {
                for (int i = 0; i < production.RightSide.Length - 1; i++)
                {
                    if (production.RightSide[i + 1].IsTerminal())
                        follows[production.RightSide[i]].Add(production.RightSide[i + 1]);
                }
            }
        }

        private bool CalculateFollowsFromProduction(Production production)
        {
            bool changed = false;
            for (int i = 0; i < production.RightSide.Length - 1; i++)
            {
                int originalCount = follows[production.RightSide[i]].Count;

                follows[production.RightSide[i]].UnionWith(firsts[production.RightSide[i + 1]].Except(new[] { SyntaxKind.None }));

                changed |= originalCount != follows[production.RightSide[i]].Count;
            }

            int index = production.RightSide.Length - 1;
            bool lastHadLambda = true;

            while (lastHadLambda)
            {
                if (index < 0) break;
                lastHadLambda = firsts[production.RightSide[index]].Contains(SyntaxKind.None);

                int originalCount = follows[production.RightSide[index]].Count;
                follows[production.RightSide[index]].UnionWith(follows[production.LeftSide]);
                changed |= originalCount != follows[production.RightSide[index]].Count;

                index--;
            }

            return changed;
        }

        public string GetStringAlias(SyntaxKind kind)
        {
            string result;
            if (stringAliases.TryGetValue(kind, out result))
                return result;
            return null;
        }

        public IEnumerable<SyntaxKind> GetPossibleKinds(SyntaxKind kind, int position, SyntaxKind[] alreadyPresent)
        {
            List<SyntaxKind> result = new List<SyntaxKind>();

            List<Production> productionList;
            if (!productions.TryGetValue(kind, out productionList))
                return result;

            foreach (var production in productionList)
            {
                if (!ProductionMatches(production, position, alreadyPresent))
                {
                    continue;
                }

                int curPosition = GetPosition(production, position);

                bool containsLambda;
                do
                {
                    containsLambda = false;
                    if (production.RightSide.Length <= curPosition)
                        break;

                    containsLambda = firsts[production.RightSide[curPosition]].Contains(SyntaxKind.None);

                    result.AddRange(firsts[production.RightSide[curPosition]].Except(new[] { SyntaxKind.None }));

                    curPosition++;
                } while (containsLambda);

                if (containsLambda && production.RightSide.Length > 0)
                {
                    result.AddRange(follows[kind]);
                }

                if (production.RightSide.Length == position)
                    result.Add(SyntaxKind.None);
            }

            return result;
        }

        private bool ProductionMatches(Production production, int position, SyntaxKind[] alreadyPresent)
        {
            if (ProductionIsRecursive(production))
            {
                if (production.RightSide[production.RightSide.Length - 1] == production.LeftSide)
                    return ProductionMatchesRightRecursive(production, position, alreadyPresent);
                else
                    return ProductionMatchesLeftRecursive(production, position, alreadyPresent);               
            }

            if (production.RightSide.Length < alreadyPresent.Length || position != alreadyPresent.Length)
                return false;

            for (int i = 0; i < position; i++)
            {
                if (!CanBeProductOf(production.RightSide[i], alreadyPresent[i]))
                    return false;
            }

            return true;
        }

        private bool ProductionMatchesLeftRecursive(Production production, int position, SyntaxKind[] alreadyPresent)
        {
            return true;
        }

        private bool ProductionMatchesRightRecursive(Production production, int position, SyntaxKind[] alreadyPresent)
        {
            for (int i = 0; i < position; i++)
            {
                if (!CanBeProductOf(production.RightSide[i % (production.RightSide.Length - 1)], alreadyPresent[i]))
                {
                    return false;
                }
            }
            return true;
        }

        private bool CanBeProductOf(SyntaxKind from, SyntaxKind to)
        {
            if (TokensAreEquivalent(from, to))
                return true;

            if (!productions.ContainsKey(from))
                return false;

            foreach (var prod in productions[from])
            {
                if (prod.RightSide.Length != 1) continue;

                if (CanBeProductOf(prod.RightSide[0], to))
                    return true;
            }

            return false;
        }

        private bool TokensAreEquivalent(SyntaxKind from, SyntaxKind to)
        {
            if (from == to)
                return true;

            if (from.IsIdentifier() && to.IsIdentifier())
                return true;

            return false;
        }

        private int GetPosition(Production production, int position)
        {
            if (!ProductionIsRecursive(production))
                return position;

            int loopLength = production.RightSide.Length - 1;
            return position % loopLength;
        }

        private bool ProductionIsRecursive(Production production)
        {
            return production.RightSide.Contains(production.LeftSide);
        }

        public IEnumerable<SyntaxKind> GetFollows(SyntaxKind kind)
        {
            if (follows.ContainsKey(kind))
                return follows[kind];
            else
                return Enumerable.Empty<SyntaxKind>();
        }
    }

    internal class Production
    {
        public SyntaxKind LeftSide { get; }
        public SyntaxKind[] RightSide { get; }

        public Production(SyntaxKind leftSide, params SyntaxKind[] rightSide)
        {
            LeftSide = leftSide;
            RightSide = rightSide;
        }

        public override string ToString()
        {
            return string.Format("{0} -> {1}", LeftSide.ToString(), string.Join(", ", RightSide));
        }
    }
}